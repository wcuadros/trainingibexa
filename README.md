# GEB CMS

[![License | PROPIEATARY](https://img.shields.io/badge/License-PROPIETARY-yellow)](https://www.grupoenergiabogota.com/terminos-y-condiciones) [![platform Linux | macOS | Windows](https://img.shields.io/badge/platform-Linux%20%7C%20macOS%20%7C%20Windows-lightgrey.svg)](https://www.docker.com/products/docker-desktop) [![docker-compose](https://img.shields.io/badge/%F0%9F%90%B3-docker--compose-blue.svg)](https://medium.com/rate-engineering/using-docker-containers-to-run-a-distributed-application-locally-eeabd360bca3) [![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://semantic-release.gitbook.io/semantic-release/) [![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/) [![pipeline status](https://gitlab.com/CamaraComercioBogota/CCBeZ/badges/master/pipeline.svg)](https://gitlab.com/CamaraComercioBogota/CCBeZ/commits/master) [![coverage report](https://gitlab.com/CineColombia/CMS/badges/master/coverage.svg)](https://gitlab.com/CineColombia/CMS/commits/master) [![</> | 1x engineers](https://img.shields.io/static/v1?label=%3C/%3E&message=1x%20engineers&color=blueviolet)](https://1x.engineer)

This is the Content Management System app to manage content for all portals network of the [GEB](https://www.grupoenergiabogota.com) project. This app is built using the [Symfony 5.3](https://symfony.com) Framework and [Ibexa 3.3.12](https://doc.ibexa.co/en/latest/)

This project uses [Docker](https://www.docker.com) containers to develop, distribuite, deploy, run and monitor the app.

# Prerequesites

-   GNU Make (preinstalled in Linux and macOS, [see for Windows](https://stackoverflow.com/questions/32127524/how-to-install-and-use-make-in-windows) or use [Docker Desktop for WSL 2](https://engineering.docker.com/2019/06/docker-hearts-wsl-2/))

# Recommendations

-   You will be using (and mistyping 😉) the `make` command so often that it is recommended to create an alias (e.g. `m`) for `make`.
-   `make` command has autocomplete! 😃, so use TAB key to see all available tasks (targets).

# Install requirements:

You will need to use this just once. If you have already installed a similar project before you might not need this step.

```bash
make requirements
```

# Override configurations

## For Docker Image Purposes Only

Create the file `.env` with the environment variables, use the `.env.dist` file for reference.

In order to download all required packages and conent you will need to configure the following variables in the `.env` file:

-   Composer authentication username and password for eZ Platform repository. Use the credentials `CCB eZ Enterprise Access Keys` shared via LastPass.

    -   `COMPOSER_AUTH_EZ_USERNAME`: use `Key ID` from secure note of LastPass
    -   `COMPOSER_AUTH_EZ_PASSWORD`: use `Secret Key` from secure note of LastPass
    
-   Aplyca Composer packages access token. Create a personal access token https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html
    -   `COMPOSER_AUTH_APLYCA_TOKEN`: Use a GitLab Deploy token (GitLab Enterprise) or Gitlab personal token

-   AWS Credentials to access to content. Use the credentials `CCB eZ Content Access Keys` shared via LastPass.
    -   `CONTENT_ACCESS_KEY`: use `Key ID` from secure note of LastPass
    -   `CONTENT_SIGNATURE`: use `Secret Key` from secure note of LastPass

# Installation

```bash
make install
```

# Start working

See [Symfony](https://symfony.com/doc/current/index.html) documentation.

## Initialize your environment

This command will build the Docker containers

```bash
make
```

## Execute tests

```bash
make test
```

## Open your environment in the browser

```bash
make open
```

## Pushing the changes to the repo

-   This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html) for releasing new versions
-   This projects adheres to [Conventional Commits](https://conventionalcommits.org) for commit guidelines
-   This project uses [Semantic Realease](https://semantic-release.gitbook.io/semantic-release) to automatically release a new version depending on commits messages
-   This project uses [Semantic Release Changelog](https://github.com/semantic-release/changelog) to automatically generate CHANGELOG.md file

Execute the following command to push your changes

```bash
make git
```

The command above will promnt some questions to help you create a good commit message in compliance to the semantic release.

This project uses semantic release to deploy to **ECS** depending on the commit messages in the `master` branch.

# Other useful commands

## Building the environment

```bash
make
```

## Update DataBase

```bash
make db.update
```

## See all commands available

```bash
make help
```

## Available commands

```
ℹ️  Usage: make <task> [option=value]
Default task: init

🛠️  Tasks:
  install i       Perform install tasks (build, updatedb, index, test, and open). This is the default task
  git g           Review, add, commit and push changes using commitizen. Usage: make push
  index           Index/reindex DB content in Solr
  db.update       ⬇️ Update DB from remote source
  test            Run all tests.
  h help          ℹ️  This help.
  requirements    📦 Installing local requirements
  init i          🏗️ Create the environment. This is the default task.
  pull            Reload one or all service containers
  config          Reload one or all service containers
  build b         Reload one or all service containers
  up reload       Reload one or all service containers
  remove          Remove service services
  remove.image    Remove Docker image from registy
  remove.uat      Destroy UAT
  down d          👎 Destroy environment
  stop            🛑 Stop environment
  start           👍 Start environment
  push            🐳  Login to Registry then push the image. Registry authentication required. Usage: make push.image.
  reboot          Recreate service containers
  rebuild         Rebuild service containers
  cli exec        Execute commands in service containers, use "command"  argument to send the command. By Default enter the shell.
  run             👟  Run commands in a new service container
  log l           Show logs. Usage: make logs [service=app]
  copy            Copy app files/directories from service container to host
  sync            Copy composer files generated inside service container to host
  sync.vendor     Copy vendor files generated inside app service container to the host
  sync.all        Copy files generated inside service container to host
  open o          Open app in the browser
  open.dbadmin    Open dbadmin in the browser
  open.mail       Open mail in the browser
  expose          Expose your local environment to the internet, thanks to Serveo (https://serveo.net)
  behat           Testing with Behat. Usage: make test.behat suite="suite" feature="path"
  codecept        Testing with Codeception. Usage: make test.codecept suite="suite" feature="path"
  db.download     Download DB
  login.ecr       Login to the AWS registry
  update.service  🚀  Update service in ECS, default FOLGERDAP-Server-Staging
  deploy.uat      Deploy to UAT
```

# Clear cache of application

```bash
make reboot
```

# Deploying App

Configrue the following variables:

## Buil-time parameters

- `COMPOSER_AUTH_EZ_USERNAME`: `Secret`. Usuario para descargar paquetes licenciados de Ibexa. Se debe obtener de el Support Portal de Ibexa. Usar uno exlusio para CI/CD
- `COMPOSER_AUTH_EZ_PASSWORD`: `Secret`. Clave para descargar paquetes liceniados de Ibexa. Se debe obtener de el Support Portal de Ibexa. Usar uno exlusio para CI/CD
- `COMPOSER_AUTH_APLYCA_TOKEN`: `Secret`. Token to download composer packages from the Aplyca package registry. Use a GitLab Deploy token (GitLab Enterprise) or Gitlab personal token

## Who do I talk to?

-   Repo owner or admin
-   Other community or team contact
-   Slack channel [#ccb-private](https://aplyca.slack.com/messages/G04FTURRD)
