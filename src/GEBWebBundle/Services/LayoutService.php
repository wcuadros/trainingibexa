<?php

/**
 * File containing the LayoutService class.
 *
 * (c) www.aplyca.com
 */

namespace App\GEBWebBundle\Services;

use App\GEBWebBundle\Services\ContentService;
use eZ\Bundle\EzPublishCoreBundle\Controller;
use eZ\Publish\API\Repository\Repository;
use eZ\Publish\Core\MVC\ConfigResolverInterface;
use Symfony\Component\HttpFoundation\Response;
use Psr\Log\LoggerInterface;

class LayoutService
{
    private $contentService;
    private $configResolver;
    private $logger;

    public function __construct(
        ContentService $contentService,
        ConfigResolverInterface $configResolver,
        LoggerInterface $logger
    ) {
        $this->logger = $logger;
        $this->configResolver = $configResolver;
        $this->contentService = $contentService;
    }

    /**
     * Get header content info
     * 
     */
    public function getHeaderContent()
    {
        $headerLocationId = $this->configResolver->getParameter('header', 'geb')["locationId"];
        return $this->contentService->getContentByLocationId($headerLocationId);
    }

    /**
     * Get Footer content info
     * 
     */
    public function getFooterContent()
    {
        $footerLocationId = $this->configResolver->getParameter('footer', 'geb')["locationId"];
        return $this->contentService->getContentByLocationId($footerLocationId);
    }

    /**
     * Get header socials info
     * 
     */
    public function getHeaderSocialsContents()
    {
        $headerContent = $this->getHeaderContent();
        $headerSocials = $this->contentService->getRelations($headerContent, 'social_networks', false);

        return $headerSocials;
    }

    /**
     * Get Footer socials info
     * 
     */
    public function getFooterSocialsContents()
    {
        $footerContent = $this->getFooterContent();
        $footerSocials = $this->contentService->getRelations($footerContent, 'social_networks', false);

        return $footerSocials;
    }

    /**
     * Get header main nav info
     * 
     */
    public function getHeaderMainNavContents()
    {
        $headerContent = $this->getHeaderContent();
        $headerMainNav = $this->contentService->getRelations($headerContent, 'main_nav', false);

        return $headerMainNav;
    }

    /**
     * Get footer main nav info
     * 
     */
    public function getFooterMainNavContents()
    {
        $footerContent = $this->getFooterContent();
        $footerMainNav = $this->contentService->getRelations($footerContent, 'main_nav', true);

        return $footerMainNav;
    }

    /**
     * Get header utility nav info
     * 
     */
    public function getHeaderUtilityNavContents()
    {
        $headerContent = $this->getHeaderContent();
        $headerUtilityNav = $this->contentService->getRelations($headerContent, 'utility_nav', false);

        return $headerUtilityNav;
    }

    /**
     * Get footer utility nav info
     * 
     */
    public function getFooterUtilityNavContents()
    {
        $footerContent = $this->getFooterContent();
        $footerUtilityNav = $this->contentService->getRelations($footerContent, 'utility_nav', false);

        return $footerUtilityNav;
    }

    /**
     * Get header main nav secondary links info
     * 
     */
    public function getMainNavSecondaryLinks($mainMenuContent)
    {
        return $this->contentService->getRelations($mainMenuContent, 'secondary_links', true);
    }

    /**
     * Get header main nav main link info
     * 
     */
    public function getMainNavMainLink($mainMenuContent)
    {
        return $this->contentService->getRelationContent($mainMenuContent, 'main_link');
    }

    /**
     * Get header main nav promo content info
     * 
     */
    public function getMainNavFeaturedContents($mainMenuContent)
    {
        return $this->contentService->getRelations($mainMenuContent, 'featured_content', false);
    }


}
