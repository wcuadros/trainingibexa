<?php
/**
 * File containing the ContentService class.
 *
 * (c) www.aplyca.com
 * (c) Developer sbustos@aplyca.com
 */

namespace App\GEBWebBundle\Services;

use Psr\Log\LoggerInterface;
use eZ\Publish\Core\MVC\ConfigResolverInterface;
use AXP\Services\SearchService as axpSearchService;
use AXP\Builder\CriteriaBuilder;
class AutomaticContentService
{
    protected $gebLogger;
    private $axpSearchService;
    private $configResolver;
    private $contentService;
    private $criteriaBuilder;

    public function __construct(
        LoggerInterface $gebLogger,
        axpSearchService $axpSearchService,
        ConfigResolverInterface $configResolver,
        ContentService $contentService,
        CriteriaBuilder $criteriaBuilder
    ){
        $this->gebLogger = $gebLogger;
        $this->axpSearchService = $axpSearchService;
        $this->configResolver = $configResolver;
        $this->contentService = $contentService;
        $this->criteriaBuilder = $criteriaBuilder;
    }

    public function getLocationsPathString($content)
    {
        try {
            return $this->axpSearchService->getPathStringByContent($content);
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage());
        }  
    }

    public function getContentsByAttributes(array $attributes, $extraCriterion = [], $sortClause = null )
    {
        try {
            $filters = [];
            $subtree = isset($attributes['locations']) ? $this->axpSearchService->getPathStringByLocation($attributes['locations']) : null;
            $contentTypeIdentifier = isset($attributes['content_types']) ? $attributes['content_types'] : null;
            $sortBy = isset($attributes['sort_by']) ? $attributes['sort_by'] : ( $sortClause != null ? $sortClause : null );
            $mode = isset($attributes['mode']) ? $attributes['mode'] :'all';
            $limit = isset($attributes['limit']) ? intval($attributes['limit']) : false;

            if (!empty($attributes['tags'])) {
                $tags = array_filter(array_map(function ($tag) {
                    if (trim($tag) != "") return trim($tag);
                }, explode(',', $attributes['tags'])));

                $extraCriterion["CustomField"] = [
                    [
                        "fieldId" => "gebweb_file_tags_tag_keywords_ms",
                        "operator" => 'IN',
                        "value" => $tags
                    ]
                ];
            }

            if(!empty($subtree)) $filters["Subtree"] = $subtree;
            if(!empty($contentTypeIdentifier)) $filters["ContentTypeIdentifier"] = explode(',', $contentTypeIdentifier);

            $filters = array_merge($filters, $extraCriterion);
            return $this->axpSearchService->fetch($filters, 'automatic_content', $sortBy, $mode, [], $limit); 
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage());
        }
    }

    public function getListedContent(array $blockParameters): ?array
    {
        try {
            $extraCriterion = [];
            $manualLocationIds = !empty($blockParameters['listed_content']) ? explode(',', $blockParameters['listed_content']) : [];
            $manualContents = $this->contentService->getContentsByLocationIds($manualLocationIds);
            $limit = !empty($blockParameters["limit"]) ? $blockParameters["limit"] : $this->configResolver->getParameter('limit_automatic_content', 'geb');
            $automaticContentAttributes = [
                'locations' => $blockParameters['locations'],
                'content_types' => $blockParameters['content_types'],
                'tags' => $blockParameters['tags'],
                'sort_by' => $blockParameters['sort_by'],
                'limit' => $limit,
                'mode' => 'contents'
            ];

            if(count($manualLocationIds) > 0){
                $parentLocations = !empty($blockParameters['locations']) ? explode(',', $blockParameters['locations']) : [];
                $locationsNotAlloweb = $this->criteriaBuilder->build(["LocationId" => array_merge($manualLocationIds, $parentLocations)]);
                $extraCriterion["LogicalNot"] = $locationsNotAlloweb;
            }

            $automaticContents = $this->getContentsByAttributes($automaticContentAttributes, $extraCriterion);
            
            $manualContents = !empty($manualContents) ? $manualContents : [];
            $automaticContents = !empty($automaticContents) ? $automaticContents : [];
            
            $listedContent = array_merge($manualContents, $automaticContents);
            return(!empty($listedContent)) ? $listedContent : null;
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage() . ' in file : ' . $e->getFile() . ' in line: ' . $e->getLine());
        }
    }

    public function getSubs(int $locationId, array $blockParameters): ?array
    {
        try {
            $limit = !empty($blockParameters["limit"]) ? $blockParameters["limit"] : $this->configResolver->getParameter('limit_automatic_content', 'geb');
            $automaticContentAttributes = [
                'locations' => $locationId,
                'content_types' => $blockParameters['content_types'],
                'tags' => $blockParameters['tags'],
                'sort_by' => $blockParameters['sort_by'],
                'limit' => $limit,
                'mode' => 'contents'
            ];

            $extraCriterion = [];
            $extraCriterion["LogicalNot"] = $this->criteriaBuilder->build(["LocationId" => $locationId]);
            $automaticContents = $this->getContentsByAttributes($automaticContentAttributes, $extraCriterion);
            return (!empty($automaticContents)) ? $automaticContents : null;
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage() . ' in file : ' . $e->getFile() . ' in line: ' . $e->getLine());
        }
    }
}
