<?php

namespace App\GEBWebBundle\Validator;

use JMS\TranslationBundle\Model\Message;
use JMS\TranslationBundle\Translation\TranslationContainerInterface;
use Symfony\Component\Validator\Constraint;

class ContentTypePlus extends Constraint implements TranslationContainerInterface
{
    public $message = 'ezplatform.content.is_not_expected_content_type';

    public $types;

    public static function getTranslationMessages(): array
    {
        return [
            Message::create('ezplatform.content.is_not_expected_content_type', 'validators')
                ->setDesc('The selected Content item isn\'t of the expected Content Type.'),
        ];
    }

    public function getDefaultOption()
    {
        return 'types';
    }

    public function getRequiredOptions()
    {
        return ['types'];
    }
}