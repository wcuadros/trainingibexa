<?php

namespace App\GEBWebBundle\Validator;

use eZ\Publish\API\Repository\ContentService;
use eZ\Publish\SPI\Specification\Content\ContentTypeSpecification;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ContentTypePlusValidator extends ConstraintValidator
{
    private $contentService;

    public function __construct(ContentService $contentService)
    {
        $this->contentService = $contentService;
    }

    public function validate($contentId, Constraint $constraint): void
    {
        if($contentId){
            $types = $constraint->types;
    
            if (!is_array($constraint->types)) {
                $types = [$types];
            }
    
            $content = $this->contentService->loadContent($contentId);
    
            foreach ($types as $expectedType) {
                $contentTypeSpecification = new ContentTypeSpecification($expectedType);
    
                if ($contentTypeSpecification->isSatisfiedBy($content)) {
                    return;
                }
            }
    
            $this->context->addViolation($constraint->message);
        }
    }
}