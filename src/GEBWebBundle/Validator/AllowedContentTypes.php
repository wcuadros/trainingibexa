<?php

namespace App\GEBWebBundle\Validator;

use Symfony\Component\Validator\Constraint;

class AllowedContentTypes extends Constraint
{
    public $message = '';
    public $types = [];
}