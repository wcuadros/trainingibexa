<?php

namespace App\GEBWebBundle\Validator;

use eZ\Publish\API\Repository\ContentTypeService;
use App\GEBWebBundle\Services\ContentService;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class AllowedContentTypesValidator extends ConstraintValidator
{
    /**
     * @var \App\GEBWebBundle\Services\ContentService
     */
    private $contentService;
    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;
    /**
     * @var \eZ\Publish\API\Repository\ContentTypeService
     */
    private $contentTypeService;

    public function __construct(
        ContentService $contentService,
        ContentTypeService $contentTypeService,
        LoggerInterface $logger
    )
    {
        $this->contentTypeService = $contentTypeService;
        $this->contentService = $contentService;
        $this->logger = $logger;
    }

    public function validate($locationlist, Constraint $constraint): void
    {
        if (!$constraint instanceof AllowedContentTypes) {
            throw new UnexpectedTypeException($constraint, AllowedContentTypes::class);
        }
        
        if($locationlist){
            $locationlist = explode(",", $locationlist);

            $types = $constraint->types;
            if (!is_array($constraint->types)) {
                $types = [$types];
            }

            foreach($locationlist as $locationId){
                try {
                    $content = $this->contentService->getContentByLocationId((int) $locationId);
                    if($content) {
                        $content_type = $content->contentType->identifier;
                        if(!in_array($content_type,$types)){
                            $initial_description = "Tipos de contenido aceptados";
                            $final_message = $initial_description . ": " . $this->getNamesTypes($types);
                            $this->context->addViolation($final_message);
                            return;
                        }
                    } else {
                        return;
                    }
                } catch(\Exception $e){
                    $this->logger->error($e->getMessage() . ', in the ' . $e->getFile() . ' in line:' . $e->getLine());
                    continue;
                }
            }
        }
    }

    private function getNamesTypes($types): string
    {
        $new_string = "";
        foreach($types as $index => $type){
            $contentTypeName = $this->contentTypeService->loadContentTypeByIdentifier($type)->getName();
            $contentTypeName = $contentTypeName == "" ? $type : "<<" . $contentTypeName . ">>";
            if($index == 0){
                $new_string = $contentTypeName;
            } else {
                $new_string = $new_string . ", " . $contentTypeName;
            }
        }
        return $new_string;
    }
}