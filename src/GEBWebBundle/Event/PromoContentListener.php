<?php

namespace App\GEBWebBundle\Event;

use App\GEBWebBundle\Services\ContentService;
use App\GEBWebBundle\Services\AutomaticContentService;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EzSystems\EzPlatformPageFieldType\FieldType\Page\Block\Renderer\BlockRenderEvents;
use EzSystems\EzPlatformPageFieldType\FieldType\Page\Block\Renderer\Event\PreRenderEvent;
use EzSystems\EzPlatformPageFieldType\FieldType\Page\Block\Renderer\Twig\TwigRenderRequest;

/**
 * Promo Content Block Listener
 */
class PromoContentListener implements EventSubscriberInterface
{
	const CONTENT_ID_FIELDS = ['featured_content', 'listed_content', 'cta_content'];

    private $automaticContentService;
	private $contentService;
    private $urlGenerator;

	public function __construct(
		ContentService $contentService,
		UrlGeneratorInterface $urlGenerator,
        AutomaticContentService $automaticContentService
	) {
		$this->contentService = $contentService;
		$this->urlGenerator = $urlGenerator;
        $this->automaticContentService = $automaticContentService;
	}
	
	public static function getSubscribedEvents()
	{
		return [
			BlockRenderEvents::getBlockPreRenderEventName('gebweb_promo_content') => 'onBlockPreRender'
		];
	}

	public function onBlockPreRender(PreRenderEvent $event)
	{
		$renderRequest = $event->getRenderRequest();
		if (!$renderRequest instanceof TwigRenderRequest) {
			return;
		}
		$parameters = $renderRequest->getParameters();
        
		foreach (self::CONTENT_ID_FIELDS as $field) {
            if($field == 'listed_content' && !empty($parameters['locations'])){
                $parameters[$field] = $this->automaticContentService->getListedContent($parameters);
            } else {
                $parameters[$field] = $this->getFieldContent($parameters[$field]);
            }
		}
		$parameters = $this->getCTA($parameters);
		$renderRequest->setParameters($parameters);
	}

	private function getFieldContent($field) {
		if (gettype($field) == "string") {
            return $this->contentService->getContentsByLocationIds($field);
		} elseif (gettype($field) == "integer") {
			$content = $this->contentService->getContentByContentId((int) $field);
			return (!empty($content)) ? $content : null;
		}
	}

	private function getCTA($parameters) {
		if (!empty($parameters['cta_content'])) {
			$cta = [
				'url' => $this->urlGenerator->generate('ez_urlalias', ['contentId' => $parameters['cta_content']->id], $this->urlGenerator::ABSOLUTE_URL),
				'label' => $parameters['cta_label'],
				'isExternal' => false
			];
		} elseif (!empty($parameters['cta_url'])) {
			$cta = [
				'url' => $parameters['cta_url'],
				'label' => $parameters['cta_label'],
				'isExternal' => true
			];
		}
		unset($parameters['cta_content'], $parameters['cta_url'], $parameters['cta_label']);
		$parameters['cta'] = $cta ?? null;
		return $parameters;
	}
}