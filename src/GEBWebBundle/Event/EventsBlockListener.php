<?php

declare(strict_types=1);

namespace App\GEBWebBundle\Event;

use EzSystems\EzPlatformPageFieldType\FieldType\Page\Block\Renderer\BlockRenderEvents;
use EzSystems\EzPlatformPageFieldType\FieldType\Page\Block\Renderer\Event\PreRenderEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use App\GEBWebBundle\Services\AutomaticContentService;
use Psr\Log\LoggerInterface;

class EventsBlockListener implements EventSubscriberInterface
{   
    private $automaticContentService;

    public function __construct(
        LoggerInterface $gebLogger,
        AutomaticContentService $automaticContentService
    ) {
        $this->gebLogger = $gebLogger;
        $this->automaticContentService = $automaticContentService;
    }

    /**
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            BlockRenderEvents::getBlockPreRenderEventName('gebweb_events') => 'onBlockPreRender',
        ];
    }

    /**
     * @param \EzSystems\EzPlatformPageFieldType\FieldType\Page\Block\Renderer\Event\PreRenderEvent $event
     *
     * @throws \eZ\Publish\API\Repository\Exceptions\InvalidArgumentException
     * @throws \eZ\Publish\API\Repository\Exceptions\NotFoundException
     * @throws \eZ\Publish\API\Repository\Exceptions\UnauthorizedException
     */
    public function onBlockPreRender(PreRenderEvent $event)
    {    
        try {
            $renderRequest = $event->getRenderRequest();
            $blockValue = $event->getBlockValue();
            $parameters = $renderRequest->getParameters();
            
            // Automatic Content Attributes
            $automaticContentAttributes = [
                'locations' => $blockValue->getAttribute('locations')->getValue(),
                'content_types' => $blockValue->getAttribute('content_types')->getValue(),
                'tags' => $blockValue->getAttribute('tags')->getValue(), 
                'limit' => $blockValue->getAttribute('limit')->getValue()
            ];
            
            $currentDate = new \DateTime("now");
            $recentEventsCriteria["Field"] = [
                [  
                    "fieldId" => "date",
                    "operator" => 'LT',
                    "value" => $currentDate->format('Y-m-d')
                ]
            ];

            $upcomingEventsCriteria["Field"] = [
                [  
                    "fieldId" => "date",
                    "operator" => 'GTE',
                    "value" => $currentDate->format('Y-m-d')
                ]
            ];
            
            $recentEvents = $this->automaticContentService->getContentsByAttributes($automaticContentAttributes, $recentEventsCriteria, 'event_date_desc');
            $upcomingEvents = $this->automaticContentService->getContentsByAttributes($automaticContentAttributes, $upcomingEventsCriteria, 'event_date');
            
            $parameters['upcomingEvents'] = $upcomingEvents;
            $parameters['recentEvents'] = $recentEvents;
            $parameters['isBlock'] = true;
            
            $renderRequest->setParameters($parameters);
        } catch(\Exception $e){
            $this->gebLogger->error($e->getMessage() . ' in file : ' . $e->getFile() . ' in line: ' . $e->getLine());
            return new Response('', 500);
        }
    }
}
