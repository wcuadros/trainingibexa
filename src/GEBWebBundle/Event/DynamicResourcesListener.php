<?php

namespace App\GEBWebBundle\Event;

use Psr\Log\LoggerInterface;
use App\GEBWebBundle\Services\ContentService;
use App\GEBWebBundle\Services\AutomaticContentService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EzSystems\EzPlatformPageFieldType\FieldType\Page\Block\Renderer\BlockRenderEvents;
use EzSystems\EzPlatformPageFieldType\FieldType\Page\Block\Renderer\Event\PreRenderEvent;
use EzSystems\EzPlatformPageFieldType\FieldType\Page\Block\Renderer\Twig\TwigRenderRequest;

/**
 * Dynamic Resources Block Listener
 */
class DynamicResourcesListener implements EventSubscriberInterface
{
    private $automaticContentService;
    private $contentService;

    public function __construct(
        LoggerInterface $gebLogger,
        ContentService $contentService,
        AutomaticContentService $automaticContentService
    ) {
        $this->gebLogger = $gebLogger;
        $this->contentService = $contentService;
        $this->automaticContentService = $automaticContentService;
    }

    public static function getSubscribedEvents()
    {
        return [
            BlockRenderEvents::getBlockPreRenderEventName('gebweb_dynamic_resources') => 'onBlockPreRender'
        ];
    }

    public function onBlockPreRender(PreRenderEvent $event)
    {
        $renderRequest = $event->getRenderRequest();
        if (!$renderRequest instanceof TwigRenderRequest) {
            return;
        }

        $parameters = $renderRequest->getParameters();
        
        if(!empty($parameters["locations"])) {
            $locations = explode(',', $parameters["locations"]);
            $automaticContents = [];
            foreach ($locations as $locationId) {
                try {
                    $automaticContents[] = [
                        "container" => $this->contentService->getContentByLocationId($locationId),
                        "subs" => $this->automaticContentService->getSubs($locationId, $parameters)
                    ];
                } catch (\Exception $e){
                    $this->gebLogger->error($e->getMessage() . ' in file : ' . $e->getFile() . ' in line: ' . $e->getLine());
                    continue;
                }
            }
            $parameters["automatic_contents"] = $automaticContents;
        }

        $renderRequest->setParameters($parameters);
    }
}