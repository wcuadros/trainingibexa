<?php

namespace App\GEBWebBundle\Controller;

use Psr\Log\LoggerInterface;
use eZ\Bundle\EzPublishCoreBundle\Controller;
use eZ\Publish\Core\MVC\ConfigResolverInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class BVCController extends Controller
{
    private $configResolver;
    protected $gebLogger;

    public function __construct(
        LoggerInterface $gebLogger,
        ConfigResolverInterface $configResolver
    ) {
        $this->gebLogger = $gebLogger;
        $this->configResolver = $configResolver;
    }
            
    /**
     * getDailyGEB
     *
     * @return JsonResponse
     */
    public function getDailyGEB(): JsonResponse
    {
        try {

            $client = new \SoapClient('https://www.bvc.com.co/ServiceNemotecnico/ServiceNemotecnico.jws?wsdl');
            $params = $this->getParamsAPI();
            $result = $client->__soapCall("XMLNemotecnico", $params);
            $conversion = $this->processXML($result);
            if($conversion["status"] == 200){
                return new JsonResponse($conversion["final_array"], $conversion["status"]);
            }
            return new JsonResponse($conversion, $conversion["status"]);
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage() . ' in file : ' . $e->getFile() . ' in line: ' . $e->getLine());
            return new JsonResponse([
                "status" => 500,
                "message" => "An error occurred while processing the SOAP request and XML values"
            ], 500);
        }
    }
    
    /**
     * getParamsAPI
     *
     * @return array
     */
    private function getParamsAPI(): array 
    {
        try {
            $params = $this->configResolver->getParameter('soapapi_eeb', 'geb');
            return [
                "In0" => $params["id"],
                "In1" => $params["ip"],
                "In2" => $params["login"],
                "In3" => $params["pass"],
            ];
        } catch (\Exception $e){
            $this->gebLogger->error($e->getMessage() . ' in file : ' . $e->getFile() . ' in line: ' . $e->getLine());
            return [];
        }
    }

    /**
     * processXML
     *
     * @param  string $result
     * @return array
     */
    private function processXML(string $result): array
    {
        $xml = simplexml_load_string($result);
        $json = json_encode($xml);
        $array = json_decode($json, TRUE);
        if(isset($array["Instrumento"])){
            foreach($array["Instrumento"] as $instrument){
                if($instrument["Papel"] == "GEB"){
                    return $this->getValuesEntity($instrument);
                }
            }
            return [
                "status" => 404,
                "message" => 'Entity "GEB" not found in BVC request list'
            ];
        }
        return [
            "status" => 400,
            "message" => 'The index "Instrument" is not found in the result of the request'
        ];
    }

    
    /**
     * getValuesEntity
     *
     * @param  array $instrument
     * @return array
     */
    private function getValuesEntity(array $instrument): array 
    {
        
        try { 
            $final_array = [
                "status" => 200,
                "final_array" => [
                    $instrument["Papel"] => [
                        "closeValue" => isset($instrument["PrecioCierre"]) ? floatval(str_replace(",", "", $instrument["PrecioCierre"])) : null,
                        "PreviousCloseValue" => isset($instrument["PrecioCierreAnterior"]) ? floatval(str_replace(",", "", $instrument["PrecioCierreAnterior"])) : null,
                        "Variation" => isset($instrument["Variacion"]) ? floatval(str_replace(",", "", $instrument["Variacion"])) : null,
                    ]
                ]
            ];
            return $final_array;
        } catch (\Exception $e){
            $this->gebLogger->error($e->getMessage() . ' in file : ' . $e->getFile() . ' in line: ' . $e->getLine());
            return [
                "status" => 500,
                "message" => "An error has been generated rendering the getValuesEntity function"
            ];
        }
    }
}
