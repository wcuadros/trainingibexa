<?php

namespace App\GEBWebBundle\Controller;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use eZ\Bundle\EzPublishCoreBundle\Controller;

use AXP\Services\SearchService;

use Psr\Log\LoggerInterface;
use Exception;

class SearchController extends Controller
{

    private SearchService $searchService;
    private LoggerInterface $logger;

    public function __construct(
        SearchService $searchService,
        LoggerInterface $logger
    ) {
        $this->searchService = $searchService;

        $this->logger = $logger;
    }

    public function showComponentAction(RequestStack $request, $currentContent, $subParamsKey = 'custom_search'): Response
    {
        try {
            $queryVars = $request->getMainRequest()->query;
            $paramsFilters = $this->getParameter('axp_search')['filters_map'][$subParamsKey];

            $filters = array_filter($queryVars->get('filters', []), fn($k) => isset($paramsFilters[$k]), ARRAY_FILTER_USE_KEY);

            $searchResponse = $this->searchService->build(
                $currentContent, 
                $filters, 
                $subParamsKey, 
                $queryVars->get('page', 1),
                10
            );

            if (!is_null($searchResponse)) {
                return $this->render(
                    '@ezdesign/search/main.html.twig', [
                    'searchResponse' => $searchResponse,
                    'json_unescaped_unicode' => JSON_UNESCAPED_UNICODE
                ]);
            } else {
                return new Response();
            }
        } catch(Exception $e) {
            $this->logger->info($e->getMessage() . ' in file : ' . $e->getFile() . ' in line: ' . $e->getLine());
            return new Response();
        }
    }
}
