<?php

namespace App\GEBWebBundle\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Psr\Log\LoggerInterface;


class NewsletterController extends Controller
{
    const URL = "https://apps.net-results.com/data/public/IncomingForm/Ma/submit";

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function submitAction(Request $request) : JsonResponse
    {
        try {
            $input_name = $request->get('input_name');
            $form_id = $request->get('form_id');
            $email = $request->get('email');
            $origin = $request->headers->get('Origin');
            $data = $input_name.'='.$email.'&form_id='.$form_id.'&__uri='.$origin;
            $response = new JsonResponse();

            if($email && $form_id && $input_name){
                $result = $this->executeSubmit(self::URL, $data);
                
                if($result['status'] == '200'){
                    $response->setStatusCode(200);
                }else{
                    $response->setStatusCode(500);
                }

                return $response;
            }

            $response->setStatusCode(500);
            return $response;

        } catch (\Exception $e) {
            $this->logger->error($e->getMessage() . ' in file : ' . $e->getFile() . ' in line: ' . $e->getLine());
            $response = new JsonResponse();
            $response->setStatusCode(500);
            return $response;
        }
    }

    private function executeSubmit($url, $data) : array
    {
        try {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_POST, TRUE);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: */*', 'Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HEADER, TRUE);
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);

            $response = curl_exec($curl);
            if (!$response) {
                throw new \UnexpectedValueException('Whoops! Curl Error - no connect');
            }
            $info = curl_getinfo($curl);
            return array(
                'status' => $info['http_code']
            );
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage() . ' in file : ' . $e->getFile() . ' in line: ' . $e->getLine());
            return array();
        }
    }
}