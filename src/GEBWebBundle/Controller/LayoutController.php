<?php

namespace App\GEBWebBundle\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use App\GEBWebBundle\Services\LayoutService;
use eZ\Publish\Core\MVC\ConfigResolverInterface;
use App\GEBWebBundle\Services\ContentService;
use App\GEBWebBundle\Services\AutomaticContentService;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Psr\Log\LoggerInterface;

use Exception;

class LayoutController extends Controller
{
    protected $gebLogger;
    private $configResolver;
    private $layoutService;
    private $contentService;
    private $automaticContentService;
    protected $container;

    public function __construct(
        ContentService $contentService, 
        AutomaticContentService $automaticContentService,
        ConfigResolverInterface $configResolver, 
        LayoutService $layoutService,
        Container $container,
        LoggerInterface $gebLogger
    ) {
        $this->contentService = $contentService;
        $this->automaticContentService = $automaticContentService;
        $this->gebLogger = $gebLogger;
        $this->configResolver = $configResolver;
        $this->layoutService = $layoutService;
        $this->container = $container;
    }

    public function headerAction($location, $currentPath, $rootLocation)
    {
        try {
            $headerContent = $this->layoutService->getHeaderContent();
            $headerSocialsContent = $this->layoutService->getHeaderSocialsContents();
            $headerUtilityNav = $this->layoutService->getHeaderUtilityNavContents();
            $headerMainNav = $this->layoutService->getHeaderMainNavContents();
            $mediaLocationId = isset($this->configResolver->getParameter('header', 'geb')["mediaLocationId"]) ? $this->configResolver->getParameter('header', 'geb')["mediaLocationId"] : null;
            
            return $this->render('@ezdesign/layout/header.html.twig', [
                'location' => $location,
                'headerContent' => $headerContent,
                'headerSocialsContent' => $headerSocialsContent,
                'headerUtilityNav' => $headerUtilityNav,
                'headerMainNav' => $headerMainNav,
                'currentPath' => $currentPath,
                'rootLocation' => $rootLocation,
                'mediaLocationId' => $mediaLocationId
            ]);
        } catch (Exception $e) {
            $this->gebLogger->error($e->getMessage() . ' in file : ' . $e->getFile() . ' in line: ' . $e->getLine());
            return new Response();
        }
    }

    public function footerAction()
    {
        try {
            $footerContent = $this->layoutService->getFooterContent();
            $footerSocialsContent = $this->layoutService->getFooterSocialsContents();
            $footerUtilityNav = $this->layoutService->getFooterUtilityNavContents();
            $footerMainNav = $this->layoutService->getFooterMainNavContents();
            
            return $this->render('@ezdesign/layout/footer.html.twig', [
                'footerContent' => $footerContent,
                'footerSocialsContent' => $footerSocialsContent,
                'footerUtilityNav' => $footerUtilityNav,
                'footerMainNav' => $footerMainNav,
            ]);
        } catch (Exception $e) {
            $this->gebLogger->error($e->getMessage() . ' in file : ' . $e->getFile() . ' in line: ' . $e->getLine());
            return new Response();
        }
    }

    public function megaMenuAction($megaMenuContent, $isMobile=false, $isHeader=true)
    {
        try {
            $mainLinkContent = $this->layoutService->getMainNavMainLink($megaMenuContent);
            $secondaryLinksContent = $this->layoutService->getMainNavSecondaryLinks($megaMenuContent);
            $featuredContent = $this->layoutService->getMainNavFeaturedContents($megaMenuContent);

            return $this->render('@ezdesign/layout/megamenu.html.twig', [
                'megaMenuContent' => $megaMenuContent,
                'secondaryLinksContent' => $secondaryLinksContent,
                'mainLinkContent' => $mainLinkContent,
                'featuredContent' => $featuredContent,
                'isMobile' => $isMobile,
                'isHeader' => $isHeader,
            ]);
        } catch (Exception $e) {
            $this->gebLogger->error($e->getMessage() . ' in file : ' . $e->getFile() . ' in line: ' . $e->getLine());
            return new Response();
        }
    }

    public function languageSwitcherAction($location, $isMobile=false)
    {
        try {         
            $translationSiteAccesses = $this->configResolver->getParameter('translation_siteaccesses');
        
            if (empty($translationSiteAccesses) or !is_array($translationSiteAccesses)) {
                throw new \InvalidArgumentException('No translation site acesseses defined');
            }

            foreach ($translationSiteAccesses as $tranlationSiteAccess) {
                    $translatedURL = $this->container->get('router')->generate('ez_urlalias', ['locationId' => $location->id, 'siteaccess' => $tranlationSiteAccess]);
                    $translationURLs[$tranlationSiteAccess] = $translatedURL;
            }
            
            return $this->render('@ezdesign/components/language_switcher.html.twig', [
                'translationURLs' => $translationURLs,
                'isMobile' => $isMobile
            ]);
        } catch (Exception $e) {
            $this->gebLogger->error($e->getMessage() . ' in file : ' . $e->getFile() . ' in line: ' . $e->getLine());
            return new Response();
        }
    }

    public function metadataAction($location, $rootLocation)
    {
        try {
            $metadata = $this->configResolver->getParameter('metadata', 'geb');
            $content = $this->contentService->getContentById($location->contentInfo->id);

            return $this->render(
                '@ezdesign/layout/metadata.html.twig',
                [
                    'metadata' => $metadata,
                    'content' => $content,
                    'location' => $location
                ]
            );
        } catch (Exception $e) {
            $this->gebLogger->error($e->getMessage() . ' in file : ' . $e->getFile() . ' in line: ' . $e->getLine());
            return new Response();
        }
    }

    public function mediaMenuAction($mainItem)
    {
        try {
            $automaticContentAttributes = [
                'locations' => $mainItem['location']->id,
                'content_types' => 'gebweb_event',
                'limit' => 5
            ];
            
            $currentDate = new \DateTime("now");
            $recentEventsCriteria["Field"] = [
                [  
                    "fieldId" => "date",
                    "operator" => 'LT',
                    "value" => $currentDate->format('Y-m-d')
                ]
            ];

            $upcomingEventsCriteria["Field"] = [
                [  
                    "fieldId" => "date",
                    "operator" => 'GTE',
                    "value" => $currentDate->format('Y-m-d')
                ]
            ];
            
            $recentEvents = $this->automaticContentService->getContentsByAttributes($automaticContentAttributes, $recentEventsCriteria, 'event_date_desc');
            $upcomingEvents = $this->automaticContentService->getContentsByAttributes($automaticContentAttributes, $upcomingEventsCriteria, 'event_date');

            return $this->render(
                '@ezdesign/pagebuilder/blocks/recent_upcoming_events.html.twig',
                [
                    'recentEvents' => $recentEvents,
                    'upcomingEvents' => $upcomingEvents,
                    'mainItemContent' => $mainItem['content'],
                    'mainItemLocation' => $mainItem['location'],
                    'isBlock' => false
                ]
            );
        } catch (Exception $e) {
            $this->gebLogger->error($e->getMessage() . ' in file : ' . $e->getFile() . ' in line: ' . $e->getLine());
            return new Response();
        }
    }
}
