<?php

namespace App\GEBWebBundle\Controller;

use Psr\Log\LoggerInterface;
use App\GEBWebBundle\Services\ContentService;
use eZ\Bundle\EzPublishCoreBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use eZ\Publish\Core\MVC\Symfony\View\ContentView;


class ViewController extends Controller
{
	protected $gebLogger;
    protected $contentService;

	public function __construct(
		LoggerInterface $gebLogger,
        ContentService $contentService
	) {
        $this->gebLogger = $gebLogger;
        $this->contentService = $contentService;
	}

	public function notFoundAction(ContentView $view)
	{
		throw $this->createNotFoundException('Página no encontrada');
	}

    public function getContent($contentId): Response
    {
        try {
            $content = $this->contentService->getContentById($contentId);
            $renderedContent = $this->renderView('@ezdesign/parts/default.html.twig', [
                'content' => $content
            ]);
            return new Response($renderedContent);
        } catch(\Exception $e){
            $this->gebLogger->error($e->getMessage() . ' in file : ' . $e->getFile() . ' in line: ' . $e->getLine());
            return new Response('', 500);
        }
    }

    public function fileAction(ContentView $view) {
        $content = $view->getContent();
        $file = $content->getFieldValue('file');
        $uri = "/content/download/{$content->id}/file/{$file->fileName}";
        return $this->redirect($uri);
    }

}