<?php

namespace App\CoreBundle\Controller;

use Psr\Log\LoggerInterface;
use App\CoreBundle\Services\ContentService;
use eZ\Bundle\EzPublishCoreBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use eZ\Publish\Core\MVC\Symfony\View\ContentView;


class BlockController extends Controller
{
	protected $gebLogger;
    protected $contentService;

	public function __construct(
		LoggerInterface $gebLogger,
        ContentService $contentService
	) {
        $this->gebLogger = $gebLogger;
        $this->contentService = $contentService;
	}

	
    /**
     * Renders icon field.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showIconAction($contentId)
    {
         try {

            $contentService = $this->getRepository()->getContentService();
            $content = $contentService->loadContent($contentId);
            return $this->render(
                '@Core/BlockItem/icon_svg.html.twig',
                array(
                    'content' => $content,
                )
            );
        } catch (\Exception $e) {
            return new Response();
        } 
    }

    public function newsItemAction($locationId)
    {
        try { 
            
            $repository = $this->getRepository();
            $locationService = $repository->getLocationService();
            $contentService = $repository->getContentService();
            $params = array('locationId' => $locationId,
                        'contentTypesInclude' => 'column',
                        'limit' => 3,
                        'sort' => 'DateModified',
                    );
                   
            $contentByParams = $this->contentService->getContentByParams($params);
            $columnList = array();
            
            foreach ($contentByParams as $index => $locationItem) {
                $contentItem = $contentService->loadContent($locationItem->contentInfo->id);
                $columnList[$index]['location'] = $locationItem;
                $columnList[$index]['content'] = $contentItem;
            }
            return $this->render(
                '@Core/BlockItem/column.html.twig',
                array(
                    'columnList' => $columnList,
                )
            );
    
        } catch (\Exception $e) {
            return new Response();
        }
    }

    public function showColumnistAction($contentId)
    {
         try {

            $contentService = $this->getRepository()->getContentService();
            $content = $contentService->loadContent($contentId);
            return $this->render(
                '@Core/BlockItem/columnist.html.twig',
                array(
                    'content' => $content,
                )
            );
        } catch (\Exception $e) {
            return new Response();
        } 
    }

}