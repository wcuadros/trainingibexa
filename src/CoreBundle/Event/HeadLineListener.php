<?php 

namespace App\CoreBundle\Event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EzSystems\EzPlatformPageFieldType\FieldType\Page\Block\Renderer\BlockRenderEvents;
use EzSystems\EzPlatformPageFieldType\FieldType\Page\Block\Renderer\Event\PreRenderEvent;
use App\CoreBundle\Services\ContentService;

class HeadLineListener implements EventSubscriberInterface
{
    private ContentService $contentService;


    public function __construct(ContentService $contentService)
    {
        $this->contentService = $contentService;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            BlockRenderEvents::getBlockPreRenderEventName('head_line') => 'onBlockPreRender',
        ];
    }

    /**
     *
     * @throws \eZ\Publish\API\Repository\Exceptions\NotFoundException
     * @throws \eZ\Publish\API\Repository\Exceptions\UnauthorizedException
     */
    public function onBlockPreRender(PreRenderEvent $event)
    {
        $blockValue = $event->getBlockValue();
        $renderRequest = $event->getRenderRequest();
        $parameters = $renderRequest->getParameters();
        try{
            if (isset($parameters['notices']) && !empty($parameters['notices'])) {
                $parameters['notices'] =  explode(',', $parameters['notices']);
                $notice_list = [];
                $notices = [];
                foreach ($parameters['notices'] as $key => $locationId) {
                    $contentNotice  = $this->contentService->getContentByLocationId((int) $locationId);
                    $notices[$key] = $contentNotice;
                }
                $parameters['notice'] = $notices;
            }
        }catch(\Exception $e) {
            dump($e->getMessage().', in the '.$e->getFile().' in line:'. $e->getLine());
            
            throw new \Exception('error'.$e->getMessage());
        } 

        $renderRequest->setParameters($parameters);
    }

    
}

