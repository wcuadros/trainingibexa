<?php 

namespace App\CoreBundle\Event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EzSystems\EzPlatformPageFieldType\FieldType\Page\Block\Renderer\BlockRenderEvents;
use EzSystems\EzPlatformPageFieldType\FieldType\Page\Block\Renderer\Event\PreRenderEvent;
use App\CoreBundle\Services\ContentService;

class FeaturedSectionListener implements EventSubscriberInterface
{
    private ContentService $contentService;


    public function __construct(ContentService $contentService)
    {
        $this->contentService = $contentService;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            BlockRenderEvents::getBlockPreRenderEventName('featured_section') => 'onBlockPreRender',
        ];
    }

    /**
     *
     * @throws \eZ\Publish\API\Repository\Exceptions\NotFoundException
     * @throws \eZ\Publish\API\Repository\Exceptions\UnauthorizedException
     */
    public function onBlockPreRender(PreRenderEvent $event)
    {
        $blockValue = $event->getBlockValue();
        $renderRequest = $event->getRenderRequest();
        $parameters = $renderRequest->getParameters();
        try{
            if (isset($parameters['sections']) && !empty($parameters['sections'])) {
                $parameters['sections'] =  explode(',', $parameters['sections']);
                $news_list = [];
                $sections = [];
                foreach ($parameters['sections'] as $key => $locationId) {
                    $contentSection  = $this->contentService->getContentByLocationId((int) $locationId);
                    $sections[$key] = $contentSection;
                    $contentIdentifier = $contentSection->contentType->identifier;
                    if($contentIdentifier == "section" ){
                        $news_list[$contentSection->versionInfo->contentInfo->id]  =  $this->contentService->getOneRelationContent($contentSection,"news");
                    }
                }
                $parameters['sections'] = $sections;
                $parameters['news_list'] = $news_list;
            }
        }catch(\Exception $e) {
            dump($e->getMessage().', in the '.$e->getFile().' in line:'. $e->getLine());
            
            throw new \Exception('error'.$e->getMessage());
        } 

        $renderRequest->setParameters($parameters);
    }

    
}

