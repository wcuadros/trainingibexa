<?php
/**
 * File containing the ContentService class.
 *
 * (c) www.aplyca.com
 * (c) Developer jdiaz@aplyca.com
 */

namespace App\CoreBundle\Services;
use eZ\Publish\API\Repository\Values\Content\Content;
use \eZ\Publish\API\Repository\ContentService as eZContentService;
use \eZ\Publish\API\Repository\LocationService as eZLocationService;
use eZ\Publish\API\Repository\Repository;
use Psr\Log\LoggerInterface;
use eZ\Publish\Core\MVC\ConfigResolverInterface;
use UnexpectedValueException;

use Symfony\Component\HttpFoundation\Request;
use eZ\Publish\API\Repository\Values\Content\LocationQuery;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\Query\SortClause;
/**
 * Helper class for getting ccb content easily.
 */
class ContentService
{
    /**
     * @var \eZ\Publish\API\Repository\ContentService
     */
    private $eZContentService;

    /**
     * @var \eZ\Publish\API\Repository\LocationService
     */
    private $eZLocationService;

    /**
     * @var LoggerInterface
     */
    protected $gebLogger;

    /**
     * @var \eZ\Publish\API\Repository\Repository
     */
    private $repository;

    /**
     * @var \eZ\Publish\Core\MVC\ConfigResolverInterface
     */
    private $configResolver;

    public function __construct(
        eZContentService $contentService, 
        eZLocationService $locationService, 
        LoggerInterface $gebLogger,
        Repository $repository,
        ConfigResolverInterface $configResolver
    ){
        $this->gebLogger = $gebLogger;
        $this->eZContentService = $contentService;
        $this->eZLocationService = $locationService;
        $this->repository = $repository;
        $this->configResolver = $configResolver;
    }

    /**
     * get only content and location from content relations
     * [getRelationsFieldsMenuArray description]
     * @param  [type]  $content [content]
     * @param  boolean $block   [description]
     * @return [type]           [description]
     */
    public function getRelations($content, $fieldIdentifier, $withChildren)
    {
        $relations = [];
        $contentIds = $content->getFieldValue($fieldIdentifier)->destinationContentIds;
        if (count($contentIds) > 0) {
            foreach ($contentIds as $contentId) {
                try {
                    $itemContent = $this->eZContentService->loadContent($contentId);
                    $mainLocationId = $itemContent->contentInfo->mainLocationId;

                    if ( !is_int($mainLocationId) ) {
                        throw new \UnexpectedValueException("Main Location ID for ".$contentId." is not valid");
                    } 

                    $itemLocation = $this->eZLocationService->loadLocation($mainLocationId);
                    $itemChildren = ( isset($withChildren) and $withChildren ) ? $this->getChildrenContentByLocationId($mainLocationId) : null;

                    if (!$itemLocation->invisible) {
                        $relations[$contentId] = [
                          'location' => $itemLocation,
                          'content' => $itemContent,
                          'children' => $itemChildren,
                        ];
                    }
                } catch (\Exception $e) {
                    $this->gebLogger->error($e->getMessage());
                }
            }
        }
        return $relations;
    }

    /**
     * get only content and location from content relations
     * [getRelationsFieldsMenuArray description]
     * @param  [type]  $content [content]
     * @param  boolean $block   [description]
     * @return [type]           [description]
     */

    public function getRelationContent($content, $fieldIdentifier)
    {
        $relationContent = [];

        if (is_object($content->getFieldValue($fieldIdentifier))){
            $contentId = $content->getFieldValue($fieldIdentifier)->destinationContentId;  
            if(!$contentId){
                return  $relationContent;
            } 
        } else {
            return  $relationContent;
        }
        
        try {
            $itemContent = $this->eZContentService->loadContent($contentId);
            $itemLocation = $this->eZLocationService->loadLocation($itemContent->contentInfo->mainLocationId);

            if (!$itemLocation->invisible) {
                $relationContent = [
                    'location' => $itemLocation,
                    'content' => $itemContent,
                ];
            }
        } catch (\Exception $e) {
            $relationContent = [];
            $this->gebLogger->error($e->getMessage());
        }
        
        return $relationContent;
    }

    /**
     * Get children content of a location by the given id
     *
     * @param int $locationId
     * @return LocationList
     */
    public function getChildrenContentByLocationId($locationId, $results = [])
    {
        if (!is_null($locationId)) {
            try {
                $location = $this->eZLocationService->loadLocation($locationId);
                $childrenLocations = $this->eZLocationService->loadLocationChildren($location);

                foreach ($childrenLocations as $childLocation) {
                    if ( !$childLocation->hidden ) {
                        $childContent = $this->getContentAllRelationByContentId($childLocation->contentInfo->id);
                        array_push($results, $childContent);
                    }
                }
            } catch (\Exception $e) {
                $this->gebLogger->error($e->getMessage());
            }
            
        }
        return $results;
    }

    public function getContentAllRelationByContentId($contentId){
        $content = $this->getContentByContentId($contentId);
        foreach($content->getFields() as $field){
            $value = $content->getFieldvalue($field->fieldDefIdentifier);
            if(isset($value->destinationContentId)){
                foreach($content->fields[$field->fieldDefIdentifier] as $lenguage){
                    $lenguage->destinationContentId = $this->getContentByContentId($value->destinationContentId);
                }
            }
            if(isset($value->destinationContentIds) && !empty($value->destinationContentIds)){
                foreach($content->fields[$field->fieldDefIdentifier] as $lenguage){
                    foreach($lenguage->destinationContentIds as $key => $itemContent){
                        $lenguage->destinationContentIds[$key] = $this->getContentByContentId($itemContent);
                    }
                }
            }
        }

        return $content;
    }

    public function getContentById($contentId)
    {
        return $this->eZContentService->loadContent($contentId);
    }

    /**
     * get all destination contentInfo of all related fields
     * @return array
     */
    public function getAllContentInfoRelatedByContentId($contentId)
    {
        $result = array();
        $content = $this->getContentById($contentId);
        $relationsFields = $this->eZContentService->loadRelations($content->getVersionInfo());
        foreach ($relationsFields as $key => $relation) {
            $result[$relation->sourceFieldDefinitionIdentifier][] = $relation->getDestinationContentInfo();
        }
        return  $result;
    }
    /**
     * Returns true if a Field is defined and has content.
     *
     * @param object \eZ\Publish\Core\Repository\Values\Content\Content $content         the Content
     * @param string                                                    $fieldIdentifier the Field identifier
     *
     * @return bool
     */
    public function fieldHasContent($content, $fieldIdentifier = null)
    {
        try {
            if (array_key_exists($fieldIdentifier, $content->fields)) {
                if ($this->fieldHelper->isFieldEmpty($content, $fieldIdentifier)) {
                    throw new \Exception("Field '".$fieldIdentifier."' is empty");
                } else {
                    return true;
                }
            } else {
                throw new \Exception("Field '".$fieldIdentifier."' not found");
            }
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Returns the absolut location of object.
     * @param int                                                    $fieldIdentifier the Field identifier
     *
     * @return location
     */
    public function getLocation($locationId)
    {
        $locationService = $this->repository->getLocationService();
        return $locationService->loadLocation($locationId);
    }


    public function getContentByLocationId($locationId)
    {
        $location = $this->eZLocationService->loadLocation($locationId);

        return $this->eZContentService->loadContent($location->contentInfo->id);
    }

    public function getLocationByLocationId($locationId)
    {
        return $this->eZLocationService->loadLocation($locationId);
    }

    /**
     * Return the locations of a content by given content id
     *
     * @param int $contentId
     * @return Location[]
     */
    public function getAllLocationsByContentId($contentId)
    {
        try {
            $content = $this->repository->getContentService()->loadContent($contentId);
            return $this->repository->getLocationService()->loadLocations($content->getVersionInfo()->getContentInfo());
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Return the content of a content by a given id
     *
     * @param int $contentId
     * @return Content
     */
    public function getContentByContentId($contentId)
    {
        try {
            $content = $this->repository->getContentService()->loadContent($contentId);
        } catch (\Throwable $th) {
            $content = [];
        }
        return $content;
    }

    /**
     *
     * @param string|array $locationIds
     */
    public function getContentsByLocationIds($locationIds): ?array
    {
        if (gettype($locationIds) == "string") {
            $locationIds = explode(',', $locationIds); 
        }
        $contentArr = [];
        foreach ($locationIds as $id) {
            try {
                $content = $this->getContentByLocationId((int) $id);
                if (!empty($content)) {
                    $contentArr[] = $content;
                }
            } catch (\Exception $e) {
                continue;
            }
        }
        return (!empty($contentArr)) ? $contentArr : null;
    }
    public function getRelationsContent( Content $content, $fieldIdentifier)
    {
        $relationContent =[];
        if (is_object($content->getFieldValue($fieldIdentifier))) {
            $relationContentIds = $content->getFieldValue($fieldIdentifier);
            foreach($relationContentIds->destinationContentIds as $relationContentId)
            {
                try{
                    $contentItem = $this->eZContentService->loadContent($relationContentId);
                    if (!$contentItem->contentInfo->isHidden) {
                        $relationContent[] = $contentItem;
                    } 
                } catch(\Exception $e) {
                    continue;
                }
            } 
        }  
        return $relationContent;
    }
    public function getOneRelationContent( Content $content, $fieldIdentifier)
    {
        $relationContent =[];
        if (is_object($content->getFieldValue($fieldIdentifier))) {
            $relationContentIds = $content->getFieldValue($fieldIdentifier);
            foreach($relationContentIds->destinationContentIds as $relationContentId)
            {
                try{
                    $contentItem = $this->eZContentService->loadContent($relationContentId);
                    if (!$contentItem->contentInfo->isHidden) {
                        $relationContent[] = $contentItem;
                    } 
                } catch(\Exception $e) {
                    continue;
                }
                break;
            } 
        }  
        return $relationContent;
    }
     /**
     * Get content in the given params search
     * Returns an empty array if no found.
     *
     * @return values array
     */
    public function getContentByParams($params)
    {
        $elements = array();
        $repository = $this->repository;
        $locationService = $repository->getLocationService();
        $searchService = $repository->getSearchService();
        $languageCode = $this->configResolver->getParameter('languages')[0];
        $criterion = array();

        $query = new Query();

        try {
            if (array_key_exists('locationId', $params)) {
                $criterion[] = new Criterion\Subtree($locationService->loadLocation($params['locationId'])->pathString);
            }

            if (array_key_exists('contentTypesInclude', $params)) {
                $criterion[] = new Criterion\ContentTypeIdentifier($params['contentTypesInclude']);
            }

            $criterion[] = new Criterion\Visibility(Criterion\Visibility::VISIBLE);
            $criterion[] = new Criterion\LanguageCode($languageCode);

            $query->query = new Criterion\LogicalAnd(
                $criterion
            );

            $sort = new SortClause\Location\Priority(Query::SORT_ASC);

            if (array_key_exists('sort', $params)) {
                if ($params['sort'] == 'DateModified') {
                    $query->sortClauses = array(
                        new SortClause\DateModified(Query::SORT_DESC),
                    );
                }
            } else {
                $query->sortClauses = array(
                    new SortClause\Location\Priority(Query::SORT_ASC),
                );
            }

            if (array_key_exists('limit', $params)) {
                $query->limit = $params['limit'];
            }

            $result = $searchService->findContent($query);
        } catch (\Exception $e) {
            throw new NotFoundException('No values found query fail', $locationId);
        }

        if ($result->totalCount > 0) {
            foreach ($result->searchHits as $index => $searchHit) {
                $locations = $locationService->loadLocations($searchHit->valueObject->versionInfo->contentInfo);

                foreach ($locations as $location) {
                    if (in_array($params['locationId'], $location->path)) {
                        $elements[$index] = $locationService->loadLocation($location->id);
                        break;
                    }
                }
            }
        }

        return $elements;
    }
}