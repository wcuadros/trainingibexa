<?php

namespace App\AdminBundle\Event;

use EzSystems\EzPlatformAdminUi\Menu\Event\ConfigureMenuEvent;
use EzSystems\EzPlatformAdminUi\Menu\MainMenuBuilder;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MenuListener implements EventSubscriberInterface
{

    public static function getSubscribedEvents()
    {
        return [
            ConfigureMenuEvent::MAIN_MENU => ['onMenuConfigure', 0],
        ];
    }

    public function onMenuConfigure(ConfigureMenuEvent $event)
    {
        $menu = $event->getMenu();

        $menu[MainMenuBuilder::ITEM_CONTENT]
            ->addChild('Contenidos auxiliares', [
                'route' => '_ez_content_view',
                'routeParameters' => [
                    'contentId' => 1918,
                    'locationId' => 1896
                ],
            ]);
        $menu[MainMenuBuilder::ITEM_CONTENT]
            ->addChild('Ejemplos de contenidos', [
                'route' => '_ez_content_view',
                'routeParameters' => [
                    'contentId' => 375,
                    'locationId' => 336
                ],
            ]);

        $menu[MainMenuBuilder::ITEM_CONTENT]
            ->addChild('Archivo', [
                'route' => '_ez_content_view',
                'routeParameters' => [
                    'contentId' => 56,
                    'locationId' => 58
                ],
            ]);
    }
}
