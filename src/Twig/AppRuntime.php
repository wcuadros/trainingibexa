<?php

namespace App\Twig;

use Psr\Log\LoggerInterface;
use eZ\Publish\Core\Helper\TranslationHelper;
use Twig\Extension\RuntimeExtensionInterface;

class AppRuntime implements RuntimeExtensionInterface
{
    protected $gebLogger;
    protected $translationHelper;

    public function __construct(
        TranslationHelper $translationHelper,
        LoggerInterface $gebLogger
    ) {
        $this->gebLogger = $gebLogger;
        $this->translationHelper = $translationHelper;
    }

    public function getBasicFields(array $list_content)
    {
        $result = [];
        foreach($list_content as $content){
            $fieldsIdentifiers = array_filter(array_map(function ($fieldDefinition) {
                if ($fieldDefinition->fieldTypeIdentifier == "ezstring") {
                    return $fieldDefinition->identifier;
                }
            }, $content->getContentType()->getFieldDefinitions()->ToArray()));

            $contentProccess = [];
            $contentProccess["content_id"] = $content->id;

            foreach($fieldsIdentifiers as $fieldIdentifier){
                $field = $this->translationHelper->getTranslatedField($content, $fieldIdentifier)->value;
                $contentProccess[$fieldIdentifier] = $field->text;
            }

            if(!isset($contentProccess["title"])){
                $promo_title = (isset($contentProccess["promo_title"]) && $contentProccess["promo_title"] != "") ? $contentProccess["promo_title"] : "";
                $contentProccess["title"] = $promo_title != "" ? $promo_title : $contentProccess["name"];
            }

            $result[] = $contentProccess;
        }
        return $result;
    }

    public function convertBytes($bytes, $to, $decimal_places = 2)
    {
        $to = strtoupper($to);
        $formulas = array(
            'KB' => number_format($bytes / 1024, $decimal_places),
            'MB' => number_format($bytes / 1048576, $decimal_places),
            'GB' => number_format($bytes / 1073741824, $decimal_places)
        );
        return isset($formulas[$to]) ? $formulas[$to] : 0;
    }
}