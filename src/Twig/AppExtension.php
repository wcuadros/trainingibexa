<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Twig\TwigFilter;
use App\Twig\AppRuntime;

class AppExtension extends AbstractExtension
{
	public function getFilters()
	{
		return [
			new TwigFilter('get_basic_fields', [AppRuntime::class, 'getBasicFields'], ['is_safe' => ['html']]),
			new TwigFilter('convert_bytes', [AppRuntime::class, 'convertBytes'], ['is_safe' => ['html']]),
		];
	}

	public function getFunctions()
	{
		return [
		];
	}
}