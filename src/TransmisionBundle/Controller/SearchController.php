<?php
/**
 * File containing the SearchController class.
 *
 * (c) www.aplyca.com
 * (c) Developer dduarte@aplyca.com
 */

namespace App\TransmisionBundle\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use App\TransmisionBundle\Services\SearchService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\TransmisionBundle\Services\ContentService;
use App\TransmisionBundle\Services\GeneralSearchService;
use App\TransmisionBundle\Pagination\Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;
use Psr\Log\LoggerInterface;


class SearchController extends Controller
{
    protected $gebLogger;
    private $searchService;
    private $gebContentService;

    public function __construct(
        LoggerInterface $gebLogger, 
        SearchService $searchService, 
        ContentService $gebContentService,
        GeneralSearchService $gebGeneralSearchService
    )
    {
        $this->gebLogger = $gebLogger;
        $this->searchService = $searchService;
        $this->gebContentService = $gebContentService;
        $this->gebGeneralSearchService = $gebGeneralSearchService;
    }

    /**
     * Renders general search results.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws NotFoundException if error found
     */
    public function generalAction(Request $request)
    {
        try {
            try {
                $searchSettings = $this->searchService->searchSettings;
                $rootLocationId = $this->getConfigResolver()->getParameter('content.tree_root.location_id');
                $pathString = $this->gebContentService->getLocation($rootLocationId)->pathString;

                $paramsQuery = array(
                    'subtreeLocationId'=> $pathString,
                    'keyword' => $request->get('SearchText'),
                    'contentTypeId'=> $searchSettings['included_classes'],
                    'limit' => $searchSettings['limit'],
                    'page' => $request->get('page') ?? 1
                );
                $dataSearch = $this->gebGeneralSearchService->getSearchResults($paramsQuery);
                $numberResults = $dataSearch['pager']->getNbResults();
                $extras = [];

                $response = new Response();
                $response->setSharedMaxAge($searchSettings['cache_ttl']);
            } catch (\Exception $e) {
                $numberResults = 0;
            }

            return $this->render(
                '@Transmision/Search/general.html.twig',
                array(
                    'searchText' => $request->get('SearchText'),
                    'results' => $dataSearch['pager']->getCurrentPageResults(),
                    'extras' => $extras,
                    'relatedNumberResults' => $numberResults,
                    'numberResults' => $numberResults,
                    'pager' => $dataSearch['pager'],
                ),
                $response
            );
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            throw $this->createNotFoundException('The search could not be performed');
        }
    }
}
