<?php

namespace App\TransmisionBundle\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use App\TransmisionBundle\Services\ChildrenService;
use App\TransmisionBundle\Services\ContentService;
use App\TransmisionBundle\Services\LayoutService;
use Symfony\Component\HttpFoundation\Response;

class HeaderController extends Controller
{
    private $transmisionLayoutService;
    private $childrenService;
    private $contentService;

    public function __construct(LayoutService $transmisionLayoutService, ChildrenService $childrenService, ContentService $contentService)
    {
        $this->transmisionLayoutService = $transmisionLayoutService;
        $this->childrenService = $childrenService;
        $this->contentService = $contentService;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response;
     */
    public function showAction()
    {
        try {
            $content = $this->transmisionLayoutService->getContent();
            $params = $this->getConfigResolver()->getParameter('layout', 'transmision');
            $headerMenu = $this->contentService->getRelations($content, "header_links");
            $headerEyebrow = $this->contentService->getRelations($content, "eyebrow_links");
            $socialNetworks = $this->transmisionLayoutService->getSocialNetworks();
            // $this->childrenService->getChildrenContent($params['socialnetworks_location_id'], 'social_network_transmision');

            $response = new Response();
            $response->setSharedMaxAge(86400);
            $response->headers->set('X-Location-Id', $content->contentInfo->mainLocationId);

            return $this->render(
                '@Transmision/Header/show.html.twig',
                [
                    'headerMenu' => $headerMenu,
                    'headerEyebrow' => $headerEyebrow,
                    'socialNetworks' => $socialNetworks,
                    'content' => $content,
                ]
            );
        } catch (Exception $e) {
            $this->logger->error($e->getMessage() . ' in file : ' . $e->getFile() . ' in line: ' . $e->getLine());
            return new Response();
        }
    }
    /**
     * @return \Symfony\Component\HttpFoundation\Response;
     */
    public function submenuMobileAction($locationId)
    {
        try {
            $submenuLocation = $this->childrenService->getChildren($locationId, 'menu_mobile_content_types', 10);

            return $this->render(
                '@Transmision/Header/submenu_mobile.html.twig',
                array(
                    'submenu_location' => $submenuLocation,
                )
            );
        } catch (Exception $e) {
            $this->logger->error($e->getMessage() . ' in file : ' . $e->getFile() . ' in line: ' . $e->getLine());
            return new Response();
        }
    }
}
