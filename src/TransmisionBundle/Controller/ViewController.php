<?php

namespace App\TransmisionBundle\Controller;

use App\TransmisionBundle\Services\ChildrenService;
use App\TransmisionBundle\Services\ContentService;
use App\TransmisionBundle\Services\PressService;
use App\TransmisionBundle\Services\ProjectService;
use eZ\Bundle\EzPublishCoreBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use eZ\Publish\Core\MVC\Symfony\View\ContentView;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Psr\Log\LoggerInterface;

class ViewController extends Controller
{
    protected $gebLogger;
    private $projectService;
    private $contentService;
    private $pressService;
    private $childrenService;
    private $containerInterface;

    public function __construct(
        LoggerInterface $gebLogger,
        ProjectService  $projectService,
        ContentService  $contentService,
        PressService    $pressService,
        ChildrenService $childrenService,
        Container $container
    )
    {
        $this->gebLogger = $gebLogger;
        $this->projectService = $projectService;
        $this->contentService = $contentService;
        $this->pressService = $pressService;
        $this->childrenService = $childrenService;
        $this->containerInterface = $container;
    }


    /**
     * Renders landingpage.
     *
     * @param $locationId
     * @param $viewType
     * @param bool $layout
     * @param array $params
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function landing_pageAction(ContentView $view, $locationId, $viewType, $layout = false, array $params = array())
    {
        try {
            $view->addParameters(
                [
                    'locationId' => $locationId,
                    'viewType' => $viewType,
                    'layout' => $layout,
                    array() + $params
                ]
            );

            return $view;
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            throw $this->createNotFoundException('The page could not be found. Error: ' . $e->getMessage());
        }
    }

    /**
     * Renders landingpage.
     *
     * @param $locationId
     * @param $viewType
     * @param bool $layout
     * @param array $params
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function landingPageProjectAction(ContentView $view, $locationId, $viewType, $layout = false, array $params = array(), Request $request)
    {
        try {

            $projectsLandingPages = $this->projectService->getProjectsLandingPages();
            $regions = $this->projectService->getRegions();
            $regionId = null;

            if (!is_null($request->get('region'))) {
                $regionId = $this->projectService->getCurrentRegionId($request->get('region'), $regions);
                $regionProjects = $this->projectService->getProjectsByRegion($locationId, $regionId);
            } else {
                $regionProjects = $this->projectService->getProjects($locationId, 'transmission_project');
            }
            $location = $this->projectService->getLocation($locationId);

            $view->addParameters(
                [
                    'locationId' => $locationId,
                    'viewType' => $viewType,
                    'layout' => $layout,
                    'region_projects' => $regionProjects,
                    'projects_landingpages' => $projectsLandingPages,
                    'regions' => $regions,
                    'parent_location' => $location,
                    'id_region' => $regionId,
                    array() + $params
                ]
            );

            return $view;
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            throw $this->createNotFoundException('The page could not be found. Error: ' . $e->getMessage());
        }
    }

    /**
     * Renders landingpage.
     *
     * @param $locationId
     * @param $viewType
     * @param bool $layout
     * @param array $params
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function projectAction(ContentView $view, $locationId, $viewType, $layout = false, array $params = array())
    {
        try {
            $location = $this->projectService->getLocation($locationId);
            $regionContentId = $this->projectService->getRegionOfProject($location);
            $regions = $this->projectService->getRegions();
            $projectsLandingPages = $this->projectService->getProjectsLandingPages();
            $parentLocation = $this->projectService->getParentLocation($location);

            $view->addParameters(
                [
                    'locationId' => $locationId,
                    'viewType' => $viewType,
                    'layout' => $layout,
                    'projects_landingpages' => $projectsLandingPages,
                    'regions' => $regions,
                    'parent_location' => $parentLocation,
                    'id_region' => $regionContentId,
                    array() + $params
                ]
            );

            return $view;
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            throw $this->createNotFoundException('The page could not be found. Error: ' . $e->getMessage());
        }
    }

    /**
     *
     * @param $locationId
     * @param $viewType
     * @param bool $layout
     * @param array $params
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function pressAction(ContentView $view, $locationId, $viewType, $layout = false, array $params = array())
    {
        try {
            $data = $this->pressService->getPressReleases($locationId);

            $view->addParameters(
                [
                    'locationId' => $locationId,
                    'viewType' => $viewType,
                    'layout' => $layout,
                    "data" => $data,
                    array() + $params
                ]
            );

            return $view;
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            throw $this->createNotFoundException('The page could not be found. Error: ' . $e->getMessage());
        }
    }

    /**
     *
     * @param $locationId
     * @param $viewType
     * @param bool $layout
     * @param array $params
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function pressByYearAction(ContentView $view, $locationId, $viewType, $layout = false, array $params = array())
    {
        try {
            $data = $this->pressService->getPressReleasesByYear($locationId);

            $view->addParameters(
                [
                    'locationId' => $locationId,
                    'viewType' => $viewType,
                    'layout' => $layout,
                    "data" => $data,
                    array() + $params
                ]
            );

            return $view;
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            throw $this->createNotFoundException('The page could not be found. Error: ' . $e->getMessage());
        }
    }

    public function infopageAction(ContentView $view, $locationId, $viewType, $layout = false, array $params = array())
    {
        try {
            $content = $this->contentService->getContentByLocationId($locationId);
            $childrens = $this->childrenService->getChildrenContent($locationId, ['infopage', 'external_link', 'folder']);
            
            $view->addParameters(
                [
                    'locationId' => $locationId,
                    'viewType' => $viewType,
                    'layout' => $layout,
                    "content" => $content,
                    "childrens" => $childrens,
                    array() + $params
                ]
            );

            return $view;
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            throw $this->createNotFoundException('The page could not be found. Error: ' . $e->getMessage());
        }
    }

    public function ourPeopleAction(ContentView $view, $locationId, $viewType, $layout = false, array $params = array())
    {
        try {
            $content = $this->contentService->getContentByLocationId($locationId);
            $children = $this->childrenService->getChildrenContent($locationId, ['people']);

            $view->addParameters(
                [
                    'locationId' => $locationId,
                    'viewType' => $viewType,
                    'layout' => $layout,
                    "content" => $content,
                    "children" => $children,
                    array() + $params
                ]
            );

            return $view;
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            throw $this->createNotFoundException('The page could not be found. Error: ' . $e->getMessage());
        }
    }

    public function galleryAction(ContentView $view, $locationId, $viewType, $layout = false, array $params = array())
    {
        try {
            $content = $this->contentService->getContentByLocationId($locationId);
            $contentImages = $this->childrenService->getChildrenContent($locationId, 'image');

            $view->addParameters(
                [
                    'locationId' => $locationId,
                    'viewType' => $viewType,
                    'layout' => $layout,
                    "content" => $content,
                    "images" => $contentImages,
                    array() + $params
                ]
            );

            return $view;
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            throw $this->createNotFoundException('The page could not be found. Error: ' . $e->getMessage());
        }
    }

    public function energyForPeaceGalleryAction(Request $request, $locationId, $viewType, $layout = false, array $params = array())
    {
        try {
            $location = $this->contentService->getLocationByLocationId($locationId);
            $parentLocation = $this->contentService->getLocationByLocationId($location->parentLocationId);

            $contentTypesDefinition = 'content_type_filter';
            $yearLocationId = '';
            $years = $this->childrenService->getLocationChildren($locationId, 'content_type_filter');

            $content = $this->contentService->getContentByLocationId($locationId);
            $galleryTypeIndex = reset($content->getFieldValue('type_gallery')->selection);

            if (!empty($request->get('year_filter'))) {
                $yearLocationId = $request->get('year_filter');
                $contentType = ($galleryTypeIndex == 1) ? ['video', 'external_video'] : 'image';
                $items = $this->childrenService->getChildrenContent($yearLocationId, $contentType);
            } else {
                $contentTypesDefinition = ($galleryTypeIndex == 1) ? 'content_type_gallery_video' : 'content_type_gallery';
                $items = $this->childrenService->getSubtreeChildren($locationId, $contentTypesDefinition);
            }

            return $this->render(
                '@Transmision/Location/full/gallery/energy_for_peace_gallery.html.twig',
                array(
                    'locationId' => $locationId,
                    'parentLocation' => $parentLocation,
                    'years' => $years,
                    'content' => $content,
                    'items' => $items,
                    'yearLocationId' => $yearLocationId,
                )
            );
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            throw $this->createNotFoundException('The page could not be found. Error: ' . $e->getMessage());
        }
    }

    public function sliderGalleryAction(array $params = array())
    {
        try {
            $locationId = $params['locationId'];
            $content = $this->contentService->getContentByLocationId($locationId);
            $contentImages = $this->childrenService->getChildrenContent($locationId, 'image');

            return $this->render(
                '@Transmision/BlockItem/general/slider_gallery.html.twig',
                array(
                    "content" => $content,
                    "images" => $contentImages
                )
            );
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            throw $this->createNotFoundException('The page could not be found. Error: ' . $e->getMessage());
        }
    }

    public function categoryAction(ContentView $view, $locationId, $viewType, $layout = false, array $params = array())
    {
        try {
            $press_releases = $this->pressService->getPressReleasesByMoth($locationId);
            $parentLocation = $this->contentService->getLocationByLocationId($locationId)->parentLocationId;
            $pressRealeaseInergiaSettings = $this->containerInterface->getParameter('transmision.frontend_group.inergia_press_release');
    
            $view->addParameters(
                [
                    'locationId' => $locationId,
                    'viewType' => $viewType,
                    'layout' => $layout,
                    "press_releases_filtered" => $press_releases,
                    "press_releases_inergia" => $parentLocation == $pressRealeaseInergiaSettings['location_id'],
                    array() + $params
                ]
            );

            return $view;
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            throw $this->createNotFoundException('The page could not be found. Error: ' . $e->getMessage());
        }
    }

    public function inergiaAction(ContentView $view, $locationId, $viewType, $layout = false, array $params = array())
    {
        try {
            $configResolver = $this->getConfigResolver();
            $pressRealeaseInergiaSettings = $this->containerInterface->getParameter('transmision.frontend_group.inergia_press_release');
            $inergiaMap = $this->contentService->getContentByLocationId($pressRealeaseInergiaSettings['inergia_map_location_id']);
            
            $view->addParameters(
                [
                    'locationId' => $locationId,
                    'viewType' => $viewType,
                    'layout' => $layout,
                    "inergia_map" => $inergiaMap,
                    array() + $params
                ]
            );

            return $view;
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            throw $this->createNotFoundException('The page could not be found. Error: ' . $e->getMessage());
        }
    }

    public function pressReleaseInergiaAction(ContentView $view, $locationId, $viewType, $layout = false, array $params = array())
    {
        try {
            $location = $this->contentService->getLocationByLocationId($locationId);
            $parentContent = $this->contentService->getContentByLocationId($location->parentLocationId);
            $colorId = reset($parentContent->getFieldValue('color')->selection);

            $view->addParameters(
                [
                    'locationId' => $locationId,
                    'viewType' => $viewType,
                    'layout' => $layout,
                    "colorId" => $colorId,
                    array() + $params
                ]
            );

            return $view;
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            throw $this->createNotFoundException('The page could not be found. Error: ' . $e->getMessage());
        }
    }

    public function pressReleaseInergiaLandingpageAction($contentId, $alias)
    {
        try {
            $content = $this->contentService->getContentById($contentId);
            $location = $this->contentService->getLocationByLocationId($content->versionInfo->contentInfo->mainLocationId);
            $parentContent = $this->contentService->getContentByLocationId($location->parentLocationId);
            $colorId = reset($parentContent->getFieldValue('color')->selection);

            return $this->render(
                '@Transmision/Line/Inergia/press_release.html.twig',
                array(
                    "content" => $content,
                    "parentContent" => $parentContent,
                    "colorId" => $colorId,
                    "alias" => $alias
                )
            );
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            throw $this->createNotFoundException('The page could not be found. Error: ' . $e->getMessage());
        }
    }
}
