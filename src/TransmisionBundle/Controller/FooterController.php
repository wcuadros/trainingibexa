<?php

namespace App\TransmisionBundle\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use App\TransmisionBundle\Services\LayoutService;
use Symfony\Component\HttpFoundation\Response;

class FooterController extends Controller
{
    private $transmisionLayoutService;

    public function __construct(LayoutService $transmisionLayoutService)
    {
        $this->transmisionLayoutService = $transmisionLayoutService;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response;
     */
    public function showAction()
    {
        try {
            $content = $this->transmisionLayoutService->getFooterContent();

            $columns = $this->transmisionLayoutService->getFooterColumns($content, ["second_column"]);

            $response = new Response();
            $response->setSharedMaxAge(86400);
            $response->headers->set('X-Location-Id', $content->contentInfo->mainLocationId);

            return $this->render(
                '@Transmision/Footer/show.html.twig',
                [
                    'columns' => $columns,
                    'content' => $content,
                ]
            );
        } catch (Exception $e) {
            $this->logger->error($e->getMessage() . ' in file : ' . $e->getFile() . ' in line: ' . $e->getLine());
            return new Response();
        }
    }
}
