<?php

namespace App\TransmisionBundle\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Psr\Log\LoggerInterface;

class ErrorController extends Controller
{
    protected $gebLogger;

    public function __construct(LoggerInterface $gebLogger)
    {
        $this->gebLogger = $gebLogger;
    }

    /**
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function errorAction($statusCode)
    {
      try {
        
        return $this->render('@Transmision/Error/error.html.twig');

        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            throw $this->createNotFoundException('The page could not be found. Error: '.$e->getMessage());
        }
    }
}
