<?php

/**
 * File containing the LineController class.
 *
 * (c) www.aplyca.com
 */

namespace App\TransmisionBundle\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use eZ\Publish\Core\MVC\Symfony\View\ContentView;
use Symfony\Component\HttpFoundation\Response;

class LineController extends Controller
{
    /**
     * Renders search default view.
     *
     * @param $locationId
     * @param $viewType
     * @param bool  $layout
     * @param array $params
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchDefaultAction(ContentView $view, $locationId, $viewType, $layout = false, array $params = array())
    {
        try {
            $view->addParameters(
                [
                    'locationId' => $locationId,
                    'viewType' => $viewType,
                    'layout' => $layout,
                    array() + $params
                ]
            );

            return $view;
        } catch (\Exception $e) {

            return new Response();
        }
    }
}
