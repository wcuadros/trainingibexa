<?php
/**
 * File containing the Head class.
 *
 * (c) www.aplyca.com
 * (c) Developer aplyca.com
 */

namespace App\TransmisionBundle\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use eZ\Publish\Core\Helper\TranslationHelper;
use App\TransmisionBundle\Services\ContentService;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\HttpFoundation\Response;
use Psr\Log\LoggerInterface;

class HeadController extends Controller
{
    private $contentService;
    private $containerInterface;
    protected $gebLogger;
    /** @var \eZ\Publish\Core\Helper\TranslationHelper */
    private $translationHelper;

    public function __construct(TranslationHelper $translationHelper, ContentService $contentService, Container $containerInterface, LoggerInterface $gebLogger)
    {
        $this->contentService = $contentService;
        $this->containerInterface = $containerInterface;
        $this->gebLogger = $gebLogger;
        $this->translationHelper = $translationHelper;
    }

    /**
     * Renders metadata.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function metadataAction($location, $rootLocation)
    {
        try {
            $metadata = $this->containerInterface->getParameter('transmision.frontend_group.metadata');
            $content = $this->contentService->getContentById($location->contentInfo->id);

            try {
                if ($location->id == $rootLocation->id) {
                    throw new \UnexpectedValueException('Root location found (home).');
                } else {
                    $metadata['title'] = $this->translationHelper->getTranslatedContentName($content);
                }
            } catch (\Exception $e) {
                $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
                $metadata['title'] = '';
            }

            try {
                if ($this->contentService->fieldHasContent($content, 'keywords_metadata')) {
                    $metadata['keywords'] = $content->getFieldValue('keywords_metadata');
                } else {
                    throw new \Exception('No metadata keywords found.');
                }
            } catch (\Exception $e) {
                // Use metada default
            }

            return $this->render(
                '@Transmision/Head/metadata.html.twig',
                [
                    'metadata' => $metadata,
                    'content' => $content,
                ]
            );
        } catch (Exception $e) {
            $this->gebLogger->error($e->getMessage() . ' in file : ' . $e->getFile() . ' in line: ' . $e->getLine());
            return new Response();
        }
    }
}
