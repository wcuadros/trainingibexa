<?php

namespace App\TransmisionBundle\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;

use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

use Psr\Log\LoggerInterface;

class ApiController extends Controller
{
    public function __construct(
        LoggerInterface $logger,
        HttpClientInterface $client
    ) {
        $this->logger = $logger;
        $this->client = $client;
    }

    public function healthCheck()
    {
        $response = new JsonResponse();

        $response->setContent('Healthcheck');
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/html');

        return $response;
    }
}