<?php

namespace App\TransmisionBundle\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use eZ\Publish\Core\MVC\ConfigResolverInterface;
use eZ\Publish\API\Repository\Repository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Psr\Log\LoggerInterface;

class MainController extends Controller
{
    protected $gebLogger;
    private $repository;
    private $containerInterface;

    public function __construct(LoggerInterface $gebLogger, Container $container, Repository $repository)
    {
        $this->gebLogger = $gebLogger;
        $this->containerInterface = $container;
        $this->repository = $repository;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response;
     */
    public function sliderAction()
    {
        try {
            $contentService = $this->repository->getContentService();
            $maqueta = $this->containerInterface->getParameter('transmision.frontend_group.layout');
            $content = $contentService->loadContent($maqueta['layout_content_id']);
            $slidersContentsIds = $content->getFieldValue('banner');
            $contentsSlides = array();

            foreach ($slidersContentsIds->destinationContentIds as $slideContentId) {
                try{
                    $contentsSlides[] = $contentService->loadContent($slideContentId);
                } catch (\Exception $e) {
                    $this->gebLogger->error("Slider content not found '". $slideContentId."' " . $e->getMessage());
                }
            }

            return $this->render(
                '@Transmision/Main/slider.html.twig',
                [
                    'contentsSlides' => $contentsSlides,
                ]
            );
        } catch (Exception $e) {
            $this->logger->error($e->getMessage() . ' in file : ' . $e->getFile() . ' in line: ' . $e->getLine());
            return new Response();
        }
    }
}
