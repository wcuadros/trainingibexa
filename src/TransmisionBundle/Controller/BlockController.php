<?php
/**
 * File containing the BlockController class.
 *
 * (c) www.aplyca.com
 */

namespace App\TransmisionBundle\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use App\TransmisionBundle\Services\ChildrenService;
use App\TransmisionBundle\Services\ContentService;
use App\TransmisionBundle\Services\PressService;
use App\TransmisionBundle\Services\ProjectService;
use Symfony\Component\HttpFoundation\Response;
use Psr\Log\LoggerInterface;

class BlockController extends Controller
{
    protected $gebLogger;
    private $projectService;
    private $contentService;
    private $pressService;
    private $childrenService;

    public function __construct(
        LoggerInterface $gebLogger,
        ProjectService  $projectService,
        ContentService  $contentService,
        PressService    $pressService,
        ChildrenService $childrenService
    )
    {
        $this->gebLogger = $gebLogger;
        $this->projectService = $projectService;
        $this->contentService = $contentService;
        $this->pressService = $pressService;
        $this->childrenService = $childrenService;
    }

    /**
     * Renders quantity.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function promosItemAction($locationId, $blockType, $variationType)
    {
        try {
            $repository = $this->getRepository();
            $locationService = $repository->getLocationService();
            $contentTypeService = $this->getRepository()->getContentTypeService();
            $location = $locationService->loadLocation($locationId);
            $content = $this->contentService->getContentById($location->contentInfo->id);
            $contentType = $contentTypeService->loadContentType($content->contentInfo->contentTypeId);

            $children = array();
            $relationsContent = array();
            
            if ($contentType->identifier == 'folder') {
                $children = $this->projectService->getContentHighlightedChildren($locationId, 'people');
            }

            if ($contentType->identifier == 'gallery') {
                $children = $this->projectService->getContentChildren($locationId, 'content_type_gallery');
            }

            if ($contentType->identifier == 'landing_page') {
                $recentFolder =$this->childrenService->getRecentChildren($locationId, 'content_types', 1);
                if (count ($recentFolder) > 0) {
                    $children = array_slice($this->projectService->getContentHighlightedChildren( reset( $recentFolder )->id, 'press_release'), 0, 3);
                    if (empty($children)) {
                        $children = $this->pressService->getPressReleasesByLocationId(reset( $recentFolder )->id, 3);
                    }
                }
            }
            if ($contentType->identifier == 'infopage') {
                $children = $this->projectService->getContentChildren($locationId, 'content_types', 1);
            }
            if ($contentType->identifier == 'quantities') {
                $children = $this->projectService->getContentChildren($locationId, 'content_type_quantities', 3);
            }
            if ($contentType->identifier == 'highlighted_news') {
                $relationsContent['news'] = $this->contentService->getRelations($content, 'news_list');
                $relationsContent['link'] = $this->contentService->getRelationContent($content, 'link');
            }
            if ($contentType->identifier == 'featured_transmission' || $contentType->identifier == 'highlighted_color') {
                $relationsContent['featured'] = $this->contentService->getRelations($content, 'featured');
                $relationsContent['featured_link'] = $this->contentService->getRelationContent($content, 'featured_link');
            }
            if ($contentType->identifier == 'highlighted_multimedia') {
                $relationsContent['media'] = $this->contentService->getRelations($content, 'media');
            }
            if ($contentType->identifier == 'highlighted_content_media') {
                $relationsContent['featured_contents'] = $this->contentService->getRelations($content, 'featured_contents');
                $relationsContent['featured_multimedia'] = $this->contentService->getRelationContent($content, 'featured_multimedia');
            }
            $template = 'default';
            if (is_object($contentType)) {
                $template = $contentType->identifier;
            }

            return $this->render(
                '@Transmision/BlockItem/promos/'.$template.'.html.twig',
                array(
                    'locationId' => $locationId,
                    'contentId' => $location->contentInfo->id,
                    'name' => $content->contentInfo->name,
                    'blockType' => $blockType, 
                    'variationType' => $variationType,
                    'content' => $content,
                    'children' => $children,
                    'relationsContent' => $relationsContent,
                )
            );
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            return new Response();
        }
    }

    public function gallerySliderAction($locationId)
    {
        try{
            $repository = $this->getRepository();
            $locationService = $repository->getLocationService();
            $location = $locationService->loadLocation($locationId);
            $content = $this->contentService->getContentById($location->contentInfo->id);
            $images = $this->childrenService->getChildrenContent($locationId, 'image');

            return $this->render(
                '@Transmision/BlockItem/general/slider_gallery.html.twig',
                array(
                    'locationId' => $locationId,
                    'content' => $content,
                    'images' => $images,
                )
            );
        }catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            return new Response();
        }
    }
    
}
