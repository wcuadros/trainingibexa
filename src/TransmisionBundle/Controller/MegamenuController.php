<?php

namespace App\TransmisionBundle\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use App\TransmisionBundle\Services\MegamenuService;
use Symfony\Component\HttpFoundation\Response;
use Psr\Log\LoggerInterface;

class MegamenuController extends Controller
{
    protected $gebLogger;
    private $megamenuService;

    public function __construct(LoggerInterface $gebLogger, MegamenuService $megamenuService)
    {
        $this->gebLogger = $gebLogger;
        $this->megamenuService = $megamenuService;
    }

    /**
       * Renders Megamenu Develoment projects.
       *
       * @param $contentId
       * @param $viewType
       * @param bool  $layout
       * @param array $params
       *
       * @return \Symfony\Component\HttpFoundation\Response
       */
    public function showDevelomentProjectsAction($contentId, $viewType, $layout = false, array $params = array())
    {
        try {
            $develomentProjects = $this->megamenuService->getDevelomentProjects();

            return $this->render(
                '@Transmision/Content/Megamenus/develoment_projects.html.twig',
                array(
                    'develomentProjects' => $develomentProjects,
                    'params' => $params
                )
              );
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            return new Response("Fail");
        }
    }
    /**
       * Renders Megamenu Infrastructure in Operation.
       *
       * @param $contentId
       * @param $viewType
       * @param bool  $layout
       * @param array $params
       *
       * @return \Symfony\Component\HttpFoundation\Response
       */
    public function showInfrastructureOperationAction($contentId, $viewType, $layout = false, array $params = array())
    {
        try {
            $subEstaciones = $this->megamenuService->getSubEstaciones();
            $transmissionLines = $this->megamenuService->getTransmissionLines();

            return $this->render(
                '@Transmision/Content/Megamenus/infrastructure_operation.html.twig',
                array(
                    'subEstaciones' => $subEstaciones,
                    'transmissionLines' => $transmissionLines,
                    'params' => $params
                )
              );
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            return new Response();

        }
    }

    /**
       * Renders Megamenu Megamenu two columns
       *
       * @param $contentId
       * @param $viewType
       * @param bool  $layout
       * @param array $params
       *
       * @return \Symfony\Component\HttpFoundation\Response
       */
    public function showMegamenuTwoColumnsAction($contentId, $viewType, $layout = false, array $params = array())
    {
        try {
            $columnsData = $this->megamenuService->getMegamenuTwoColumnsData($contentId);

            return $this->render(
                '@Transmision/Content/Megamenus/megamenu_two_columns.html.twig',
                array(
                    'columnsData' => $columnsData,
                    'params' => $params
                )
              );
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            return new Response();

        }
    }


    public function showMegamenuOneColumnAction($contentId, $viewType, $layout = false, array $params = array())
    {
        try {
            $columnData= $this->megamenuService->getMegamenuOneColumnData($contentId);
            return $this->render(
                '@Transmision/Content/Megamenus/megamenu_one_column.html.twig',
                array(
                    'columnData' => $columnData["column"],
                    'items' => $columnData["children"],
                    'params' => $params
                )
            );
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            return new Response();

        }
    }

    /**
       * Renders Megamenu Megamenu three columns
       *
       * @param $contentId
       * @param $viewType
       * @param bool  $layout
       * @param array $params
       *
       * @return \Symfony\Component\HttpFoundation\Response
       */
    public function showMegamenuThreeColumnsAction($contentId, $viewType, $layout = false, array $params = array())
    {
        try {
            $data = $this->megamenuService->getMegamenuThreeData($contentId);

            return $this->render(
                '@Transmision/Content/Megamenus/megamenu_three_columns.html.twig',
                array(
                    'data' => $data,
                    'params' => $params
                )
              );
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            return new Response();

        }
    }
    /**
       * Renders Megamenu Megamenu featured three columns
       *
       * @param $contentId
       * @param $viewType
       * @param bool  $layout
       * @param array $params
       *
       * @return \Symfony\Component\HttpFoundation\Response
       */
    public function showMegamenuFeaturedColumnsAction($contentId, $viewType, $layout = false, array $params = array())
    {
        try {
            $columns = array();
            $data = $this->megamenuService->getMegamenuFeaturedColumnsData($contentId);

            return $this->render(
                '@Transmision/Content/Megamenus/megamenu_featured_three_columns.html.twig',
                array(
                    'data' => $data,
                    'params' => $params
                )
              );
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            return new Response();

        }
    }
}
