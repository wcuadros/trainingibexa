<?php
/**
 * File containing the PartsController class.
 *
 * (c) www.aplyca.com
 * (c) Developer dduarte@aplyca.com
 */

namespace App\TransmisionBundle\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use App\TransmisionBundle\Services\ChildrenService;
use App\TransmisionBundle\Services\ContentService;
use App\TransmisionBundle\Services\PartsService;
use App\TransmisionBundle\Services\PressService;
use App\TransmisionBundle\Services\ProjectService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Psr\Log\LoggerInterface;

class PartsController extends Controller
{
    protected $gebLogger;
    private $childrenService;
    private $contentService;
    private $pressService;
    private $partsService;
    private $containerInterface;

    public function __construct(
        LoggerInterface $gebLogger,
        ProjectService  $projectService,
        ContentService  $contentService,
        PressService    $pressService,
        ChildrenService $childrenService,
        PartsService    $partsService,
        Container $container
    )
    {
        $this->gebLogger = $gebLogger;
        $this->projectService = $projectService;
        $this->contentService = $contentService;
        $this->pressService = $pressService;
        $this->childrenService = $childrenService;
        $this->partsService = $partsService;
        $this->containerInterface = $container;
    }

    /**
     * Renders breadcrumbs.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function breadcrumbsAction($locationId, $depth = false, $fromLocationId = false, $additionalBreadcrumb = '')
    {
        try {
            $repository = $this->getRepository();
            $locationService = $repository->getLocationService();
            $contentService = $repository->getContentService();
            $languageService = $repository->getContentLanguageService();
            $languageCode = $languageService->getDefaultLanguageCode();

            $rootLocationId = $this->getConfigResolver()->getParameter('content.tree_root.location_id');
            $path = $locationService->loadLocation($locationId ?? $rootLocationId)->path;
            $isRootLocation = false;
            $breadcrumbs = array();

            if ($fromLocationId) {
                $rootLocationId = $fromLocationId;
            }
            for ($i = 0; $i < count($path); ++$i) {
                $location = $locationService->loadLocation($path[$i]);

                if ($location->contentInfo->id > 0 && $location->depth > 1) {
                    $content = $contentService->loadContent($location->contentInfo->id);
                    $contentName = $content->versionInfo->getName($languageCode);

                    if (empty($contentName)) {
                        $contentName = $content->contentInfo->name;
                    }
                }
                // if root location hasn't been found yet
                if (!$isRootLocation) {
                    // If we reach the root location We begin to add item to the breadcrumb from it
                    if ($location->id == $rootLocationId) {
                        $isRootLocation = true;
                        $breadcrumbs[] = array('text' => $contentName, 'url' => $this->generateUrl('ez_urlalias', ['locationId' => $location->id], UrlGeneratorInterface::ABSOLUTE_URL));
                    }
                } // The root location has already been reached, so we can add items to the breadcrumb
                else {
                    if (!$depth) {
                        $breadcrumbs[] = array('text' => $contentName, 'url' => $this->generateUrl('ez_urlalias', ['locationId' => $location->id], UrlGeneratorInterface::ABSOLUTE_URL));
                    } else {
                        if ($location->depth <= $depth or $i == count($path) - 1) {
                            $breadcrumbs[] = array('text' => $contentName, 'url' => $this->generateUrl('ez_urlalias', ['locationId' => $location->id], UrlGeneratorInterface::ABSOLUTE_URL));
                        }
                    }
                }
            }

            if ($additionalBreadcrumb != '') {
                $breadcrumbs[] = array('text' => $additionalBreadcrumb, 'url' => '');
            }

            // We don't want the breadcrumb to be displayed if we are on the frontpage
            // which means we display it only if we have several items in it
            if (count($breadcrumbs) <= 1) {
                return new Response();
            }

            return $this->render(
                '@Transmision/Parts/breadcrumbs.html.twig',
                array(
                    'breadcrumbs' => $breadcrumbs,
                )
            );
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            return new Response();
        }
    }

    /**
     * Renders sidebar.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function sidebarAction($locationId, $depth = false, $contentTypeNav = 'sidenav_content_types', $publishOrdered = false, $inergiaLogo = false)
    {
        try {
            $repository = $this->getRepository();
            $contentService = $repository->getContentService();
            $locationService = $repository->getLocationService();
            $contentTypeService = $repository->getContentTypeService();
            $location = $repository->getLocationService()->loadLocation($locationId);

            if ($depth) {
                $goBack = $locationService->loadLocation($location->path[$depth]);
                $sidebar['left_navigation']['go_back'] = $goBack;

                return $this->render(
                    '@Transmision/Parts/sidebar.html.twig',
                    array(
                        'sidebar' => $sidebar,
                        'location' => $location,
                        'inergiaLogo' => $inergiaLogo
                    )
                );
            }

            if ($location->depth <= 3) {
                $parentLocationId = $location->contentInfo->mainLocationId;
            } else {
                $parentLocationId = $location->parentLocationId;
            }

            $sidebar = array();
            $parent = $locationService->loadLocation($parentLocationId);
            $sidebar['left_navigation']['parent'] = $parent;
            $sidebar['left_navigation']['current_id'] = $location->id;

            $siblings = $this->childrenService->getChildren($parentLocationId, $contentTypeNav);
            foreach ($siblings as $index => $sibling) {
                if ($sibling->id == $locationId) {
                    $sidebar['left_navigation']['siblings'][$index]['current_location'] = true;
                }

                $contentSibling = $contentService->loadContent($sibling->contentInfo->id);
                $sidebar['left_navigation']['siblings'][$index]['location'] = $sibling;
                $sidebar['left_navigation']['siblings'][$index]['content'] = $contentSibling;

                $siblingContentType = $contentTypeService->loadContentType($sibling->contentInfo->contentTypeId);
                $sidebar['left_navigation']['siblings'][$index]['contentType'] = $siblingContentType;
                try {
                    if ($publishOrdered) {
                        $siblingChildren = $this->childrenService->getRecentChildren($sibling->id, $contentTypeNav);
                    } else {
                        $siblingChildren = $this->childrenService->getChildren($sibling->id, $contentTypeNav);
                    }
                    foreach ($siblingChildren as $childIndex => $siblingChild) {
                        $locationSiblingChild = $locationService->loadLocation($siblingChild->contentInfo->mainLocationId);
                        $contentSiblingChild = $contentService->loadContent($locationSiblingChild->contentInfo->id);
                        $siblingChildContentType = $contentTypeService->loadContentType($locationSiblingChild->contentInfo->contentTypeId);
                        $sidebar['left_navigation']['siblings'][$index]['children'][$childIndex]['location'] = $locationSiblingChild;
                        $sidebar['left_navigation']['siblings'][$index]['children'][$childIndex]['content'] = $contentSiblingChild;
                        $sidebar['left_navigation']['siblings'][$index]['children'][$childIndex]['contentType'] = $siblingChildContentType;
                    }
                } catch (\Exception $e) {
                    continue;
                }
            }

            return $this->render(
                '@Transmision/Parts/sidebar.html.twig',
                array(
                    'sidebar' => $sidebar,
                    'location' => $location,
                    'inergiaLogo' => $inergiaLogo
                )
            );
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            return new Response();
        }
    }

    /**
     * Renders sidebar.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function customSidebarAction($locationId, $depth = false, $contentTypeNav = 'sidenav_content_types')
    {
        try {
            $location = $this->contentService->getLocationByLocationId($locationId);
            $sidebar = $this->partsService->getSideBar($locationId);
            return $this->render(
                '@Transmision/Parts/custom_sidebar.html.twig',
                array(
                    'sidebar' => $sidebar,
                    'location' => $location,
                )
            );
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            return new Response();
        }
    }

    public function sidebarPressAction($locationId, $depth = false, $publishOrdered = false, $inergiaLogo = false)
    {
        try {
            $sidebarPress = $this->pressService->sidebarPress($locationId, $depth, $publishOrdered);
            $colorId = null;
            $inergiaLocationId = null;
            if ($sidebarPress['sidebar']['left_navigation']['parentContent']->getFieldValue('color') != null) {
                $colorId = reset($sidebarPress['sidebar']['left_navigation']['parentContent']->getFieldValue('color')->selection);
            }

            if ($inergiaLogo) {
                $InergiaSettings = $this->containerInterface->getParameter('transmision.frontend_group.inergia_press_release');
                $inergiaLocationId = $InergiaSettings["location_id"];
            }

            return $this->render(
                '@Transmision/Parts/sidebar_press.html.twig',
                array(
                    'data_nav' => $sidebarPress,
                    'colorId' => $colorId,
                    'inergiaLocationId' => $inergiaLocationId
                )
            );
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            return new Response();
        }
    }

    public function getDocumentsProjectAction($locationId, $type = 'documents')
    {
        try {
            $documents = $this->projectService->getDocumentsProject($locationId, $type);

            return $this->render(
                '@Transmision/Parts/' . $type . '.html.twig',
                array(
                    'documents' => $documents,
                )
            );
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            return new Response();
        }
    }

    public function getTimelineProjectAction($locationId, $type = 'timeline')
    {
        try {
            $timelines = $this->projectService->getTimelineProject($locationId, $type);

            return $this->render(
                '@Transmision/Parts/' . $type . '.html.twig',
                array(
                    'timelines' => $timelines,
                )
            );
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            return new Response();
        }
    }

    public function getGalleryProjectAction($locationId, $type = 'gallery')
    {
        try {
            $gallery = $this->projectService->getGalleryProject($locationId, $type);

            return $this->render(
                '@Transmision/Parts/' . $type . '.html.twig',
                array(
                    'gallery' => $gallery,
                )
            );
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            return new Response();
        }
    }

    public function highlightedItemsSidebarAction($locationId)
    {
        try {
            $content = $this->contentService->getContentByLocationId($locationId);
            $highlightedItems = $this->contentService->getRelations($content, 'highlighted_items');

            return $this->render(
                '@Transmision/Parts/highlighted_items_sidebar.html.twig',
                array(
                    'highlightedItems' => $highlightedItems,
                )
            );
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
            return new Response();
        }
    }
}
