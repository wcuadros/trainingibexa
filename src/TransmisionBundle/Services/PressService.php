<?php
/**
 * File containing the MegamenuService class.
 *
 * (c) www.aplyca.com
 * (c) Developer rcermeno@aplyca.com
 */

namespace App\TransmisionBundle\Services;

use \eZ\Publish\API\Repository\LocationService as eZLocationService;
use Psr\Log\LoggerInterface;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\SortClause;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

/**
 * Helper class for getting ccb content easily.
 */
class PressService
{
    /**
     * @var \eZ\Publish\API\Repository\LocationService
     */
    private $locationService;
    
    /**
     * @var LoggerInterface
     */
    protected $gebLogger;
    
    private $childrenService;

    private $contentService;

    /**
     * @var Container
     */
    private $container;

    /**
     * @var array
     */
    public $pressSettings;


    public function __construct(eZLocationService $locationService, ContentService $contentService, ChildrenService $childrenService, LoggerInterface $gebLogger = null, Container $container) {
        $this->gebLogger = $gebLogger;
        $this->container = $container;
        $this->contentService = $contentService;
        $this->locationService = $locationService;
        $this->childrenService = $childrenService;
        $this->pressSettings = $this->container->getParameter('transmision.frontend_group.press');
    }

    /**
     *
     * @return array
     *
     */
    public function getPressReleases($locationId)
    {
        try {
            $data = array();
            $contentType = $this->pressSettings['years']['content_types'];
            $sortClauses =  array(new SortClause\Field($contentType, "name", Query::SORT_DESC));
            $params = array(
              'parentLocationId' => $locationId,
              'contentType' => $contentType,
              'sortClauses' => $sortClauses
            );

            $data = $this->childrenService->getChildren($params);

            foreach ($data as $key => $year) {
                $data[$key] = array(
                    "year" => $year,
                    "press_releases" => $this->getPressReleasesByLocationId($year->id)
                );
            }
            
            return $data;

        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage());
            return array();
        }
    }
    
    /**
     * @return array
     */
    public function getPressReleasesByMoth($locationId)
    {
        $press_relese_by_month = [];
        try {
            $press_releases = $this->getPressReleasesByYear($locationId);
            $monthList = $this->pressSettings['months'];
            foreach ($press_releases as $press_release) {
                $date = $press_release->getFieldValue("date")->date;
                $year = $month =  (int)$date->format('Y');
                $month =  $monthList[(int)$date->format('m') - 1];
                $press_relese_by_month[$year][$month][] = $press_release;
            }   
            return $press_relese_by_month;
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage());
            return array();
        }
    }


    /**
     * @return array
     */
    public function getPressReleasesByYear($locationId)
    {
        try {
            $releases = array();
            $settings = $this->pressSettings['press_releases'];
            $sortClauses =  array(
                new SortClause\Field($settings['content_types'], "date", Query::SORT_DESC)
            );

            $params = array(
                'parentLocationId' => $locationId,
                'contentType' => $settings['content_types'],
                'sortClauses' => $sortClauses,
            );

            $releases = $this->childrenService->getChildren($params);
            
            foreach ($releases as $key => $childLocation) {
                $releases[$key] = $this->contentService->getContentById($childLocation->__get('contentInfo')->id);
            }

            return $releases;
            
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage());
            return array();
        }
    }
    /**
     * @return array
     */
    public function getPressReleasesByLocationId($locationId, $limit = null)
    {
        try {
            $releases = array();
            $settings = $this->pressSettings['press_releases'];
            $sortClauses =  array(
                new SortClause\Field($settings['content_types'], "date", Query::SORT_DESC)
            );
            
            $params = array(
                'parentLocationId' => $locationId,
                'contentType' => $settings['content_types'],
                'sortClauses' => $sortClauses,
                'limit' =>  $limit ?? $settings['limit_by_year'],
            );

            $releases = $this->childrenService->getChildren($params);
            foreach ($releases as $key => $childLocation) {
                $releases[$key] = $this->contentService->getContentById($childLocation->__get('contentInfo')->id);
            }

            return $releases;
            
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage());
            return array();
        }
    }
    /**
     * Renders sidebar.
     *
     * @return 
     */
    public function sidebarPress($locationId, $depth = false, $publishOrdered = false)
    {
        $result = array();
        $location = $this->locationService->loadLocation($locationId);

        if ($depth) {
            $goBack = $this->locationService->loadLocation($location->path[$depth]);
            $sidebar['left_navigation']['go_back'] = $goBack;

            return $result = array(
                    'sidebar' => $sidebar,
                    'location' => $location,
                );
        }

        if ($location->depth <= 3) {
            $parentLocationId = $location->contentInfo->mainLocationId;
        } else {
            $parentLocationId = $location->parentLocationId;
        }

        $sidebar = array();
        $parent = $this->locationService->loadLocation($parentLocationId);
        $sidebar['left_navigation']['parent'] = $parent;
        $sidebar['left_navigation']['parentContent'] = $this->contentService->getContentById($parent->contentInfo->id);
        $sidebar['left_navigation']['current_id'] = $location->id;

        $settingsPress = $this->childrenService->childrenSettings['sidenavpress'];
        
        if ($publishOrdered) {
            $siblings  = $this->childrenService->getRecentChildren($parentLocationId, 'sidenav_content_types');
        } else {
            $siblings = $this->childrenService->getChildrenOrderByField($parentLocationId, 'sidenav_content_types', $settingsPress['content_type_parent'], $settingsPress['field_order_parent']);
        }

        foreach ($siblings as $index => $sibling) {
            if ($sibling->id == $locationId) {
                $sidebar['left_navigation']['siblings'][$index]['current_location'] = true;
            }
            $contentSibling = $this->contentService->getContentById($sibling->contentInfo->id);
            $sidebar['left_navigation']['siblings'][$index]['location'] = $sibling;
            $sidebar['left_navigation']['siblings'][$index]['content'] = $contentSibling;
            
            try {
                if ($publishOrdered) {
                    $siblingChildren = $this->childrenService->getRecentChildren($sibling->id, 'sidenav_content_types');
                } else {
                    $siblingChildren = $this->childrenService->getChildrenOrderByField($sibling->id, 'sidenav_content_types', $settingsPress['content_type'], $settingsPress['field_order']);
                }
                foreach ($siblingChildren as $childIndex => $siblingChild) {
                    $locationSiblingChild = $this->locationService->loadLocation($siblingChild->contentInfo->mainLocationId);
                    $contentSiblingChild = $this->contentService->getContentById($locationSiblingChild->contentInfo->id);

                    $sidebar['left_navigation']['siblings'][$index]['children'][$childIndex]['location'] = $locationSiblingChild;
                    $sidebar['left_navigation']['siblings'][$index]['children'][$childIndex]['content'] = $contentSiblingChild;
                }
            } catch (\Exception $e) {
                continue;
            }
        }
        
        return $result = array(
            'sidebar' => $sidebar,
            'location' => $location,
        );
    }
}
