<?php

/**
 * File containing the SearchService class.
 *
 * (c) www.aplyca.com
 */

namespace App\TransmisionBundle\Services;

use eZ\Publish\API\Repository\SearchService as ezSearchService;
use eZ\Publish\API\Repository\Values\Content\LocationQuery;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\Query\SortClause;

use App\TransmisionBundle\Handlers\Pagination\Pagerfanta\LocationAggregationsAdapter;
use App\TransmisionBundle\Services\ContentService;

use Pagerfanta\Pagerfanta;
use Psr\Log\LoggerInterface;
use Exception;
use ReflectionClass;

/**
 * Helper class for getting search content easily.
 */
class GeneralSearchService
{
    private $parameters;

    private $contentService;
    private $ezSearchService;
    private $logger;

    public function __construct(
        ContentService $contentService,
        ezSearchService $ezSearchService,
        LoggerInterface $logger
    ) {
        $this->contentService = $contentService;
        $this->ezSearchService = $ezSearchService;
        $this->logger = $logger;
    }

    /**
     * Generate filter criteria from filters in queryString and parameters.yml definition.
     *
     * @param array $queryParams
     * @return array
     */
    private function generateFiltersCriteria($queryParams = [])
    {
        $filters = $queryParams['filters'] ?? [];
        $keyword = $queryParams['keyword'] ?? '';

        $criteria = [
            new Criterion\Visibility(Criterion\Visibility::VISIBLE)
        ];

        try {
            if (!empty($keyword)) {
                $criteria[] = new Criterion\FullText($keyword);
            }

            if (isset($queryParams['parentLocationId'])) {
                $criteria[] = new Criterion\ParentLocationId($queryParams['parentLocationId']);
            }

            if (isset($queryParams['subtreeLocationId'])) {
                $criteria[] = new Criterion\Subtree($queryParams['subtreeLocationId']);
            }

            if (isset($queryParams['contentTypeId'])) {
                $contentTypesCriterion = array_map(function ($contentType) {
                    return new Criterion\ContentTypeIdentifier($contentType);
                }, $queryParams['contentTypeId']);

                $criteria[] = new Criterion\LogicalOr($contentTypesCriterion);
            }

            foreach ($filters as $filterKey => $filterValues) {
                if (array_key_exists($filterKey, $this->parameters['allowed_filters']) && !empty($filterValues)) {
                    $filterDefinition = $this->parameters['allowed_filters'][$filterKey];

                    if (!isset($filterDefinition['criterion']) || empty($filterDefinition['criterion'])) {
                        continue;
                    }

                    $criteria[] = $this->parseFilterWithParameters($filterDefinition, $filterValues);
                }
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage() . ' in file : ' . $e->getFile() . ' in line: ' . $e->getLine());
        }

        return $criteria;
    }

    /**
     * Init query and facets for search
     *
     * @param array $queryParams
     * @return array
     */
    public function getSearchResults($queryParams = [])
    {
        $results = [
            'data' => [],
            'pager' => null,
            'aggregations' => []
        ];

        $sort = $queryParams['sort'] ?? ['publish_date' => 'desc'];

        try {
            $query = new LocationQuery();

            $queryCriteria = $this->generateFiltersCriteria(
                array_merge_recursive($queryParams, $queryParams['extraQuery'] ?? [])
            );

            $query->query = new Criterion\LogicalAnd($queryCriteria);
            $query->limit = $queryParams['limit'] ?? 100;

            $query->sortClauses = [];

            foreach ($sort as $fieldSort => $directionSort) {
                $sort = $directionSort == 'asc' ? LocationQuery::SORT_ASC : LocationQuery::SORT_DESC;

                if ($fieldSort == 'publish_date') {
                    $query->sortClauses[] = new SortClause\DatePublished($sort);
                } else if ($fieldSort == 'relevance') {
                    $query->sortClauses[] = new SortClause\Score($sort);
                } else if ($fieldSort == 'alphabetically') {
                    $query->sortClauses[] = new SortClause\ContentName($sort);
                }
            }

            //$query->aggregations = $this->getSortedAggregations($queryParams['locationId']);

            $adapter = new LocationAggregationsAdapter($query, $this->ezSearchService);

            $pager = new Pagerfanta($adapter);
            $pager->setMaxPerPage($query->limit);
            $pager->setCurrentPage($queryParams['page'] ?? 1);

            $locations = $pager->getCurrentPageResults();
            $results['pager'] = $pager;
            $results['aggregations'] = $adapter->getAggregations();

            foreach ($locations as $location) {
                $results['data'][] = [
                    'location' => $location->valueObject,
                    'content' => $this->contentService->getContentByLocationId($location->valueObject->id)
                ];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage() . ' in file : ' . $e->getFile() . ' in line: ' . $e->getLine());
        }

        return $results;
    }
}
