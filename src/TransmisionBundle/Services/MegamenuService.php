<?php
/**
 * File containing the MegamenuService class.
 *
 * (c) www.aplyca.com
 * (c) Developer rcermeno@aplyca.com
 */

namespace App\TransmisionBundle\Services;

use \eZ\Publish\API\Repository\LocationService as eZLocationService;
use Psr\Log\LoggerInterface;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\SortClause;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

/**
 * Helper class for getting ccb content easily.
 */
class MegamenuService
{
    /**
     * @var \eZ\Publish\API\Repository\LocationService
     */
    private $locationService;
    
    /**
     * @var LoggerInterface
     */
    protected $gebLogger;
    
    private $childrenService;

    private $contentService;

    /**
     * @var array
     */
    public $childrenSettings;
    /**
     * @var array
     */
    public $megamenuSettings;

    /**
     * @var Container
     */
    private $container;

    private $regions;

    public function __construct(
        eZLocationService $locationService,
        ContentService $contentService,
        ChildrenService $childrenService,
        ProjectService $projectService,
        LoggerInterface $gebLogger = null,
        Container $container
        ) {
        $this->gebLogger = $gebLogger;
        $this->container = $container;
        $this->contentService = $contentService;
        $this->locationService = $locationService;
        $this->childrenService = $childrenService;
        $this->projectService = $projectService;
        $this->childrenSettings = $this->container->getParameter('transmision.frontend_group.children');
        $this->megamenuSettings = $this->container->getParameter('transmision.frontend_group.megamenu');
    }

    /**
     *
     * @return array
     *
     */
    public function getDevelomentProjects()
    {
        try {
            $result = array();
            $contentType = $this->childrenSettings['project_content_types'];
            $parentLocationId = $this->megamenuSettings['develoment_projects']['parent_location_id'];
            $limit = $this->megamenuSettings['develoment_projects']['limit'];
            $sortClauses =  array(new SortClause\Field($contentType[0], "name", Query::SORT_ASC));

            $params = array(
              'parentLocationId' => $parentLocationId,
              'contentType' => $contentType,
              'sortClauses' => $sortClauses,
              'limit' => $limit,
            );

            $result["projects"] =  $this->childrenService->getChildren($params);

            return array_chunk($result['projects'], 3);
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage());
            return array();
        }
    }
    
    /**
     *
     * @return array
     *
     */
    public function getSubEstaciones()
    {
        try {
            $contentType = $this->childrenSettings['project_content_types'];
            $regions = $this->projectService->getNameListRegions();
            $parentLocationId = $this->megamenuSettings['infrastructure_opreration']['parent_location_id'];
            $limit = $this->megamenuSettings['infrastructure_opreration']['subestaciones']['limit'] / 4;
            $projectType = $this->megamenuSettings['infrastructure_opreration']['subestaciones']['type'];
            $field = ['name' => 'project_type', "value"=> $projectType];
            $sortClauses =  array(new SortClause\Field($contentType[0], "name", Query::SORT_ASC));

            $params = array(
              'parentLocationId' => $parentLocationId,
              'contentType' => $contentType,
              'fields' => $field,
              'limit' => $limit,
            );
            
            $result = array();
            foreach ($regions as $key => $region) {
                $idRegion = $key;
                $params['fieldsRelations'] = ['name' => 'region', "value"=> [$idRegion]];
                $childrensByRegion = $this->childrenService->getChildren($params);
                if (!empty($childrensByRegion)) {
                    $result[$region->text] = $this->childrenService->getChildren($params);
                }
            }
            
            return $result;
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage());
            return array();
        }
    }
    /**
     *
     * @return array
     *
     */
    public function getTransmissionLines()
    {
        try {
            $result = array();
            $parentLocationId = $this->megamenuSettings['infrastructure_opreration']['parent_location_id'];
            $contentType = $this->childrenSettings['project_content_types'];
            $projectType = $this->megamenuSettings['infrastructure_opreration']['transmission_lines']['type'];
            $field = ['name' => 'project_type', "value"=> $projectType];
            $sortClauses =  array(new SortClause\Field($contentType[0], "name", Query::SORT_ASC));
            $limit = $this->megamenuSettings['infrastructure_opreration']['transmission_lines']['limit'];

            $params = array(
              'parentLocationId' => $parentLocationId,
              'contentType' => $contentType,
              'fields' => $field,
              'sortClauses' => $sortClauses,
              'limit' => $limit,
            );
            $result =  $this->childrenService->getChildren($params);

            return array_chunk($result, 2);

        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage());
            return array();
        }
    }

    /**
     * @return array
     */
    public function getMegamenuOneColumnData($contentId)
    {
        try {
            $column =$this->contentService->getAllContentInfoRelatedByContentId($contentId)["column"];
            $columnData = array(
                "column" => reset($column),
                "children" => $this->childrenService->getChildren(reset($column)->mainLocationId, 'content_types')
            );
           
            return $columnData;
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage());
            return array();
        }
    }

    /**
     * @return array
     */
    public function getMegamenuTwoColumnsData($contentId)
    {
        try {
            $columns = array();
            $columns = $this->contentService->getAllContentInfoRelatedByContentId($contentId);
            foreach ($columns as $key => $column) {
                if (isset($columns[$key]) && !empty($columns[$key])) {
                    $firstContenInfo = reset($column);
                    $columns[$key] = array(
                        "featured" => $this->contentService->getContentById($firstContenInfo->id),
                        "related" => $column
                    );
                }
            }
            return $columns;
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage());
            return array();
        }
    }
    /**
     * @return array
     */
    public function getMegamenuThreeData($contentId)
    {
        try {
            $columns = array();
            $columns = $this->contentService->getAllContentInfoRelatedByContentId($contentId);
        
            foreach ($columns as $key => $column) {
                if (isset($columns[$key]) && !empty($columns[$key])) {
                    $listChildren = $this->childrenService->getChildren(reset($column)->mainLocationId, 'content_types');
                    $columns[$key] = [
                      "objRelated" => reset($column),
                      "children" => count($listChildren) <= 5 ? $listChildren : array_slice($listChildren, 0, 5),
                    ];
                }
            }
        
            $projectLocationId = $this->megamenuSettings['develoment_projects']['parent_location_id'];
            $projectLocation = $this->locationService->loadLocation($projectLocationId);
            $regions = $this->projectService->getRegions();
        
            return array(
                "columns" => $columns,
                "projectLocation" => $projectLocation,
                "regions" => $regions,
            );
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage());
            return array();
        }
    }
    /**
     * @return array
     */
    public function getMegamenuFeaturedColumnsData($contentId)
    {
        try {
            $columns = array();
            $featured = null;
            $columns = $this->contentService->getAllContentInfoRelatedByContentId($contentId);
        
            if (isset($columns['featured']) && !empty($columns['featured'])) {
                $objRelatedContentInfo = reset($columns['featured']);
                $featured = $this->contentService->getContentById($objRelatedContentInfo->id);
                unset($columns['featured']);
            }

            return array(
              "featured"=> $featured,
              "columns" => $columns
            );
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage());
            return array();
        }
    }
}
