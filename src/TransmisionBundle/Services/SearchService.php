<?php
/**
 * File containing the SearchService class.
 *
 * (c) www.aplyca.com
 * (c) Developer dduarte@aplyca.com
 */

namespace App\TransmisionBundle\Services;

use eZ\Publish\API\Repository\Repository;
use eZ\Publish\Core\MVC\Legacy\Kernel;
use Symfony\Component\HttpFoundation\RequestStack;
use App\TransmisionBundle\Pagination\Pagerfanta\GEBSearchAdapter;
use App\TransmisionBundle\Pagination\Pagerfanta\Pagerfanta;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use eZ\Publish\Core\MVC\ConfigResolverInterface;
use Pagerfanta\Adapter\ArrayAdapter;
use eZFunctionHandler;
use eZHTTPTool;

/**
 * Helper class for getting search content easily.
 */
class SearchService
{
    /**
     * The legacy kernel instance (eZ Publish 4).
     *
     * @var \Closure
     */
    private $legacyKernelClosure;

    /**
     * @var \eZ\Publish\API\Repository\Repository
     */
    private $repository;

    /**
     * @var Symfony\Component\HttpFoundation\Request
     */
    private $request;

    /**
     * @var Container
     */
    private $container;

    private $configResolver;

    /**
     * @var int
     */
    public $rootLocationId;

    /**
     * @var array
     */
    public $searchSettings;

    public function __construct(Repository $repository, RequestStack $requestStack, Container $container, ConfigResolverInterface $configResolver)
    {
//        $this->legacyKernelClosure = $kernelClosure;
        $this->repository = $repository;
        $this->request = $requestStack->getCurrentRequest();
        $this->container = $container;
        $this->configResolver = $configResolver;
        $this->rootLocationId = $this->configResolver->getParameter('content.tree_root.location_id');
        $this->searchSettings = $this->container->getParameter('transmision.frontend_group.search');
    }

    /**
     * @return \eZ\Publish\Core\MVC\Legacy\Kernel
     */
    protected function getLegacyKernel()
    {
        $legacyKernelClosure = $this->legacyKernelClosure;

        return $legacyKernelClosure();
    }

    public function doSearch($params)
    {
        $searchParams = array();

        if (array_key_exists('searchText', $params)) {
            $searchParams['query'] = $params['searchText'];
        }

        if (array_key_exists('subTree', $params)) {
            $searchParams['subtree_array'] = $params['subTree'];
        }

        if (array_key_exists('sortBy', $params)) {
            $searchParams['sort_by'] = $params['sortBy'];
        }

        if (array_key_exists('offset', $params)) {
            $searchParams['offset'] = $params['offset'];
        }

        if (array_key_exists('limit', $params)) {
            $searchParams['limit'] = $params['limit'];
        }

        if (array_key_exists('classId', $params)) {
            $searchParams['class_id'] = $params['classId'];
        }

        if (array_key_exists('facet', $params)) {
            $searchParams['facet'] = $params['facet'];
        }

        if (array_key_exists('filter', $params)) {
            $searchParams['filter'] = $this->getFilterParameters($params['additionalFilter'], $params['filter']);
        }

        return ['SearchCount' => 0, 'SearchExtras' => null];
    }

    public function getResults($searchResults)
    {
        $results = array();

        $locationService = $this->repository->getLocationService();

        if ($searchResults['SearchCount'] > 0) {
            foreach ($searchResults['SearchResult'] as $index => $searchResult) {
                $results[] = $locationService->loadLocation($searchResult->MainNodeID);
            }
        }

        return $results;
    }

    public function getFilterParameters($additionalFilter, $filter)
    {
        $http = eZHTTPTool::instance();
        $filterList = array();

        if ($http->hasGetVariable('filter')) {
            $getFilterValue = $http->getVariable('filter');
            if (is_array($getFilterValue) && !empty($getFilterValue)) {
                foreach ($getFilterValue as $filterCond) {
                    list($name, $value) = explode(':', $filterCond, 2);
                    $filterList[$name][] = $name.':'.$value;
                }
            }
        }

        $explodedFilter = explode(':', $filter, 2);

        if (!array_key_exists($explodedFilter[0], $filterList)) {
            $filterList[$explodedFilter[0]][] = $explodedFilter[0].':'.$explodedFilter[1];
        }

        if (count($additionalFilter) > 0) {
            foreach ($additionalFilter as $filterId => $filterValue) {
                if (!array_key_exists($filterId, $filterList)) {
                    $filterList[$filterId][] = $filterId.':'.$filterValue;
                }
            }
        }

        foreach ($filterList as $filterId => $filterValue) {
            if (count($filterValue) > 1) {
                $filterList[$filterId] = array_merge(array('OR'), $filterValue);
            }
        }

        return $filterList;
    }

    /**
     * Get Paginated search with Pagerfanta.
     *
     * @return \Pagination\Pagerfanta\Pagerfanta
     *
     * @throws Pagerfanta\Pagerfanta exeptions if error found
     */
    public function getPaginatedSearch(array $overrideParams = array())
    {
        $request = $this->request;
        $requestSearchText = $request->get('SearchText');

        $searchSettings = $overrideParams + $this->searchSettings;
        $rootLocationId = $this->rootLocationId;
        $sortBy = array();

        if (array_key_exists('subtree', $searchSettings)) {
            $rootLocationId = $searchSettings['subtree'];
        }

        if (array_key_exists('sort_by', $searchSettings)) {
            $sortBy = $searchSettings['sort_by'];
        }

        $searchParams = array(
            'searchText' => $requestSearchText,
            'subTree' => array($rootLocationId),
            'classId' => $searchSettings['included_classes'],
            'sortBy' => $sortBy,
        );

        // $pager = new Pagerfanta(
        //     new GEBSearchAdapter(
        //         $this->getLegacyKernel(),
        //         $overrideParams + $searchParams,
        //         $this->repository->getLocationService()
        //     )
        // );

        // $pager->setMaxPerPage($searchSettings['limit']);

        // try {
        //     $pager->setCurrentPage($request->get('page', 1));
        // } catch (\Exception $e) {
        //     $pager->setCurrentPage(1);
        // }

        $pager = new Pagerfanta(new ArrayAdapter([]));

        return $pager;
    }
}
