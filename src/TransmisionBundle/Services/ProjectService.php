<?php
/**
 * File containing the ContentService class.
 *
 * (c) www.aplyca.com
 * (c) Developer dduarte@aplyca.com
 */

namespace App\TransmisionBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use \eZ\Publish\API\Repository\LocationService;
use eZ\Publish\Core\MVC\ConfigResolverInterface;
use Psr\Log\LoggerInterface;

/**
 * Helper class for getting ccb content easily.
 */

class ProjectService
{
    /**
     * @var \App\TransmisionBundle\Services\ContentService
     */
    private $contentService;
    /**
     * [private description]
     * @var [type]
     */
    private $childrenService;
    /**
     * @var \eZ\Publish\API\Repository\LocationService
     */
    private $locationService;

    private $configResolver;
    /**
     * @var LoggerInterface
     */
    protected $gebLogger;
     /**
     * @var Container
     */
    private $container;
    /**
     * @var array
     */
    public $projectSettings;

    public function __construct(ContentService $contentService, ChildrenService $childrenService, LocationService $locationService, ConfigResolverInterface $configResolver, LoggerInterface $gebLogger = null, Container $container)
    {
        $this->container = $container;
        $this->contentService = $contentService;
        $this->childrenService = $childrenService;
        $this->locationService = $locationService;
        $this->configResolver = $configResolver;
        $this->gebLogger = $gebLogger;
        $this->projectSettings = $this->container->getParameter('transmision.frontend_group.project');
    }

    /**
     * [getLocation return location]
     * @param  [type] $locationId [description]
     * @return [type]             [description]
     */
    public function getLocation($locationId)
    {
        return $this->locationService->loadLocation($locationId);
    }

    /**
     * [getRegions get all regions]
     * @return [type] [content  regions]
     */
    public function getRegions()
    {
        $itemsRegion = array();
        $regions = $this->childrenService->getChildren($this->projectSettings['regions_location_id'], 'project_content_type_region');
        if (isset($regions)) {
            foreach ($regions as $index =>$region) {
                $contentRegion = $this->contentService->getContentById($region->contentInfo->id);
                $itemsRegion[] = $contentRegion;
            }
        }
        return $itemsRegion;
    }
    
    /**
     * [getRegions get names of the regions order for ids]
     * @return array
     */
    public function getNameListRegions()
    {
        $listName = array();
        $regions = $this->getRegions();
        foreach ($regions as $region) {
          $listName[$region->id] = $region->getFieldValue('name');
        }
        return $listName;
    }


    /**
     * [getProjectByRegion get content of project]
     * @param  [type] $locationProject [description]
     * @return [type]                  [description]
     */
    public function getProjectByRegion($locationProject)
    {
        $projects = array();

        if (isset($locationProject)) {
            $contentProject= $this->contentService->getContentById($locationProject->contentInfo->id);
            $projects['content'] =  $contentProject;
            $projects['location'] =  $locationProject;
        }
        return $projects;
    }

    /**
     * [getProjectsLandingPages get landing pages]
     * @return [type] [location]
     */
    public function getProjectsLandingPages()
    {
        $rootLocationId = $this->configResolver->getParameter('content.tree_root.location_id');
        
        return $this->childrenService->getChildren($rootLocationId, 'content_type_parentproject');
    }

    /**
     * [getParentLocation get location of parent]
     * @param  [type] $location [description]
     * @return [type]           [location]
     */
    public function getParentLocation($location)
    {
        $parentLocation = $this->locationService->loadLocation($location->parentLocationId);

        return $parentLocation;
    }

    /**
     * [getRegionProjects get all the projects associated with a region]
     * @param  [type] $parentLocationId [description]
     * @param  [type] $regionContentId [description]
     * @return [type]             [location]
     */

    public function getProjectsByRegion($parentLocationId, $regionContentId)
    {
        $projectByRegion = array();
        $resultContent = $this->childrenService->getChildrenContentByFieldRelation($parentLocationId, 'project_content_types', $this->projectSettings['project_type_field'], (array)$regionContentId, 'transmission_project','project_type');

        foreach ($resultContent as $key => $content) {

            $projectType = reset($content->getFieldValue('project_type')->selection);

            if ($projectType == '0'){
                $projectByRegion['proyectos'][] =
                    [
                        "location" => $this->getLocation($content->versionInfo->contentInfo->mainLocationId),
                        "content" => $content,
                    ];
            }
            if ($projectType == '1'){
                $projectByRegion['subestaciones'][] =
                    [
                        "location" => $this->getLocation($content->versionInfo->contentInfo->mainLocationId),
                        "content" => $content,
                    ];
            }
            if ($projectType == '2'){
                $projectByRegion['lineas_transmision'][] =
                    [
                        "location" => $this->getLocation($content->versionInfo->contentInfo->mainLocationId),
                        "content" => $content,
                    ];
            }
        }
        return $projectByRegion;

    }

    /**
     * [getRegionOfProject Return the Content Id of the region of the project ]
     * @param  [type] $location [description]
     * @return [type]           [id content]
     */
    public function getRegionOfProject($location)
    {
        $content = $this->contentService->getContentById($location->contentInfo->id);
        $fieldName = $this->projectSettings['project_type_field'];
        $destinationContentId = $content->getFieldValue($fieldName)->destinationContentId;

        return $destinationContentId;
    }

    public function getCurrentRegionId($id, $regions){
        if (is_null($id)) {
           $id = reset($regions)->id;
        }

        return $id;
    }
    public function getDocumentsProject($locationId, $type){
        $documents = array();
        $locations = $this->childrenService->getSubtreeChildren($locationId, 'content_type_'.$type);

        foreach ($locations as $index => $location) {
            $content = $this->contentService->getContentById($location->contentInfo->id);
            $documents[$index] = [
                "location" => $location,
                "content" => $content,
            ];
        }
        return $documents;
    }
    
    public function getTimelineProject($locationId, $type){

        $timelines = array();
        $contentType = reset($this->childrenService->childrenSettings['content_type_timeline']);
        $locationTimelines = $this->childrenService->getSubChildrenOrderByField($locationId, 'content_type_'.$type, $contentType, $this->projectSettings['timeline_type_field']);
        
        if(count($locationTimelines) > 0){
            $locationTimelineFirst = reset($locationTimelines);
            $parentLocation= $this->getParentLocation($locationTimelineFirst);
            $timelines['parent_content'] = $this->contentService->getContentById($parentLocation->contentInfo->id);

            foreach ($locationTimelines as $index => $location) {
                $timelines['item_timeline'][] = $this->contentService->getContentById($location->contentInfo->id);
            }
        }

        return $timelines;
    }
    public function getGalleryProject($locationId, $type){

        $gallery = array();
        $locationGallery = $this->childrenService->getSubtreeChildren($locationId, 'content_type_'.$type);
       
        if(count($locationGallery) > 0){
            foreach ($locationGallery as $index => $location) {
                $gallery[$index] = $this->contentService->getContentById($location->contentInfo->id);
            }
        }
        return $gallery;
    }
    public function getContentChildren($locationId, $contentType, $limit = 1000){

        $children = array();
        $locationChildren = $this->childrenService->getChildren($locationId, $contentType, $limit);

        if(count($locationChildren) > 0){
            foreach ($locationChildren as  $location) {
                $children[] = $this->contentService->getContentById($location->contentInfo->id);
            }
        }
       return $children;
    }

    /**
     * [getProjects get all the projects]
     * @param  [type] $parentLocationId [description]
     * @param  [type] $contentType [description]
     * @return [type]             [location]
     */

    public function getProjects($parentLocationId, $contentType='')
    {
        $projects = array();
        $resultContent = $this->childrenService->getChildrenContent($parentLocationId, $contentType);

        foreach ($resultContent as $content) {
            $projectType = reset($content->getFieldValue('project_type')->selection);
            if ($projectType == '0') {
                $projects['proyectos'][] =
                    [
                        "location" => $this->getLocation($content->versionInfo->contentInfo->mainLocationId),
                        "content" => $content,
                    ];
            }
            if ($projectType == '1') {
                $projects['subestaciones'][] =
                    [
                        "location" => $this->getLocation($content->versionInfo->contentInfo->mainLocationId),
                        "content" => $content,
                    ];
            }
            if ($projectType == '2') {
                $projects['lineas_transmision'][] =
                    [
                        "location" => $this->getLocation($content->versionInfo->contentInfo->mainLocationId),
                        "content" => $content,
                    ];
            }
        }
        return $projects;
    }

    public function getContentHighlightedChildren($locationId, $contentType, $limit = 100){
        try {

            $childrenContent = $this->childrenService->getChildrenContent($locationId, $contentType, $limit);
            $children = array();
            foreach ($childrenContent as $content) {
                if($content->getFieldValue('highlighted')->bool){
                    $children [] = $content;
                }
            }
            return $children;
        } catch (\Exception $e) {
            return false;
        }
    }
}
