<?php
/**
 * File containing the IOCacheResolver class.
 *
 * (c) www.aplyca.com
 * (c) Developer rcermeno@aplyca.com
 */

namespace App\TransmisionBundle\Services;

use eZ\Bundle\EzPublishCoreBundle\Imagine\IORepositoryResolver;


/**
 * LiipImagineBundle cache resolver using eZ IO repository.
 */
class IORepositoryRelativeResolver extends IORepositoryResolver
{
    /**
     * Returns base URL, with scheme, host and port, for current request context.
     * If no delivery URL is configured for current SiteAccess, will return base URL from current RequestContext.
     *
     * @return string
     */
    protected function getBaseUrl()
    {
        return '';
    }
}
