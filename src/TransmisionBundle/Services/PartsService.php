<?php
/**
 * File containing the ContentService class.
 *
 * (c) www.aplyca.com
 * (c) Developer dduarte@aplyca.com
 */

namespace App\TransmisionBundle\Services;

use eZ\Publish\API\Repository\SearchService;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\Query\SortClause;
use eZ\Publish\API\Repository\Values\Content\LocationQuery;
use \App\TransmisionBundle\Services\ContentService;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Psr\Log\LoggerInterface;

/**
 * Helper class for getting ccb content easily.
 */
class PartsService
{
    /**
     * @var \App\TransmisionBundle\Services\ContentService
     */
    private $contentService;

    /**
     * @var eZ\Publish\API\Repository\SearchService;
     */
    private $searchService;

    /**
     * @var LoggerInterface
     */
    protected $gebLogger;

    /**
     * @var array
     */
    public $childrenSettings;

    /**
     * @var Container
     */
    private $container;


    public function __construct(Container $container, ContentService $contentService, SearchService $searchService, LoggerInterface $gebLogger = null)
    {
        $this->container = $container;
        $this->contentService = $contentService;
        $this->searchService = $searchService;
        $this->gebLogger = $gebLogger;
        $this->childrenSettings = $this->container->getParameter('transmision.frontend_group.children');
    }

    /**
     *
     * @return array
     *
     */
    public function getSideBar($locationId)
    {
        try {
            $sidebar = array();
            $location = $this->contentService->getLocationByLocationId($locationId);

            $parentLocationId = ($location->depth <= 3) ? $location->id : $location->parentLocationId;
            $parent = $this->contentService->getLocationByLocationId($parentLocationId);

            $contentTypes = $this->childrenSettings['sidenav_content_types'];

            $sidebar['parent'] = $parent;
            $sidebar['siblings'] = $this->getSidebarItems($parentLocationId, $contentTypes);

            foreach ($sidebar['siblings'] as $sibling) {
                $sidebar[$sibling->id] = $this->getSidebarItems($sibling->id, $contentTypes);
            }
            return $sidebar;
        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage());
            return array();
        }
    }

    public function getSidebarItems($locationId, $contentTypes)
    {
        try {

            $items = array();

            $query = new LocationQuery();
            $query->query = new Criterion\LogicalAnd(
                [
                    new Criterion\ParentLocationId($locationId),
                    new Criterion\ContentTypeIdentifier($contentTypes),
                    new Criterion\Visibility(Criterion\Visibility::VISIBLE),
                    new Criterion\Field('show_on_the_sidebar', Criterion\Operator::EQ, 1)
                ]);

            $query->sortClauses = array(
                new SortClause\Location\Priority(LocationQuery::SORT_ASC),
            );

            $resultLocation = $this->searchService->findLocations($query);

            if ($resultLocation->totalCount > 0) {
                $resultElements = array();
                foreach ($resultLocation->searchHits as $searchHit) {
                    $items[] = $searchHit->valueObject;
                }
            }
            return $items;

        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage());
            return array();
        }
    }


}