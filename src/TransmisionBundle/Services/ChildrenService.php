<?php
/**
 * File containing the CCBChildrenService class.
 *
 * (c) www.aplyca.com
 */

namespace App\TransmisionBundle\Services;

use eZ\Publish\API\Repository\Repository;
use eZ\Publish\API\Repository\Values\Content\Location;
use eZ\Publish\API\Repository\Values\Content\LocationQuery;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\Query\SortClause;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion\Operator;
use eZ\Publish\Core\Base\Exceptions\NotFoundException;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Psr\Log\LoggerInterface;

/**
 * Service class for getting ccb channels easily.
 */
class ChildrenService
{
    /**
     * @var \eZ\Publish\API\Repository\Repository
     */
    private $repository;
    private $container;

    /**
     * @var LoggerInterface
     */
    protected $gebLogger;

    /**
     * @var array
     */
    public $childrenSettings;

    public function __construct(Container $container, Repository $repository, LoggerInterface $gebLogger = null)
    {
        $this->container = $container;
        $this->repository = $repository;
        $this->gebLogger = $gebLogger;
        $this->childrenSettings = $this->container->getParameter('transmision.frontend_group.children');
    }

    /**
     * Get children items.
     *
     * @param int $parentLocationId the location ID of the parent
     * @param string $contentTypesDefinition the type of the content types allowed in children list
     *
     * @return array of \eZ\Publish\Core\Repository\Values\Content\Location when children found
     *
     * @throws NotFoundException if no children found
     */
    public function call_getChildren($parentLocationId, $contentTypesDefinition = 'content_types', $limit = 1000)
    {
        $resultElements = array();
        $query = new LocationQuery();
        try {
            $query->query = new Criterion\LogicalAnd(
                array(
                    new Criterion\ParentLocationId($parentLocationId),
                    new Criterion\ContentTypeIdentifier($this->getChildrenTypes($contentTypesDefinition)),
                    new Criterion\Visibility(Criterion\Visibility::VISIBLE),
                )
            );

            $query->sortClauses = array(
                new SortClause\Location\Priority(Query::SORT_ASC),
            );

            $query->limit = $limit;
            $result = $this->repository->getSearchService()->findLocations($query);
        } catch (\Exception $e) {
            throw new NotFoundException('No values found query fail', $parentLocationId);
        }

        if ($result->totalCount > 0) {
            $resultElements = array();
            foreach ($result->searchHits as $searchHit) {
                $resultElements[] = $searchHit->valueObject;
            }
        }

        return $resultElements;
    }

    /**
     * Get children items.
     *
     * @param int $parentLocationId the location ID of the parent
     * @param string $contentTypesDefinition the type of the content types allowed in children list
     *
     * @return array of \eZ\Publish\Core\Repository\Values\Content\Location when children found
     *
     * @throws NotFoundException if no children found
     */
    public function getChildrenByField($parentLocationId, $contentTypesDefinition = 'content_types', $fieldName = '', $fieldValue = '')
    {
        $resultElements = array();
        $query = new LocationQuery();
        try {
            $query->query = new Criterion\LogicalAnd(
                array(
                    new Criterion\ParentLocationId($parentLocationId),
                    new Criterion\ContentTypeIdentifier($this->getChildrenTypes($contentTypesDefinition)),
                    new Criterion\Field($fieldName, Operator::EQ, $fieldValue),
                    new Criterion\Visibility(Criterion\Visibility::VISIBLE)
                )
            );

            $query->sortClauses = array(
                new SortClause\Location\Priority(Query::SORT_ASC),
            );

            $query->limit = 1000;
            $result = $this->repository->getSearchService()->findLocations($query);
        } catch (\Exception $e) {
            throw new NotFoundException('No values found query fail', $parentLocationId);
        }

        if ($result->totalCount > 0) {
            $resultElements = array();
            foreach ($result->searchHits as $searchHit) {
                $resultElements[] = $searchHit->valueObject;
            }
        }

        return $resultElements;
    }

    /**
     * Get children items.
     *
     * @param int $parentLocationId the location ID of the parent
     * @param string $contentTypesDefinition the type of the content types allowed in children list
     *
     * @return array of \eZ\Publish\Core\Repository\Values\Content\Location when children found
     *
     * @throws NotFoundException if no children found
     */
    public function getChildrenByFieldRelation($parentLocationId, $contentTypesDefinition = 'content_types', $fieldName = '', $fieldValue = array())
    {
        $resultElements = array();
        $query = new LocationQuery();
        try {
            $query->query = new Criterion\LogicalAnd(
                array(
                    new Criterion\ParentLocationId($parentLocationId),
                    new Criterion\ContentTypeIdentifier($this->getChildrenTypes($contentTypesDefinition)),
                    new Criterion\FieldRelation($fieldName, Operator::IN, $fieldValue),
                    new Criterion\Visibility(Criterion\Visibility::VISIBLE),
                )
            );

            $query->sortClauses = array(
                new SortClause\Location\Priority(LocationQuery::SORT_ASC),
            );

            $query->limit = 1000;
            $result = $this->repository->getSearchService()->findLocations($query);
        } catch (\Exception $e) {
            throw new NotFoundException('No values found query fail', $parentLocationId);
        }

        if ($result->totalCount > 0) {
            $resultElements = array();
            foreach ($result->searchHits as $searchHit) {
                $resultElements[] = $searchHit->valueObject;
            }
        }

        return $resultElements;
    }

    /**
     * Get children items.
     *
     * @param int $parentLocationId the location ID of the parent
     * @param string $contentTypesDefinition the type of the content types allowed in children list
     *
     * @return array of \eZ\Publish\Core\Repository\Values\Content\Location when children found
     *
     * @throws NotFoundException if no children found
     */
    public function getSubtreeChildren($parentLocationId, $contentTypesDefinition = 'content_types')
    {
        $resultElements = array();
        $locationService = $this->repository->getLocationService();
        $query = new LocationQuery();
        try {
            $query->query = new Criterion\LogicalAnd(
                array(
                    new Criterion\Subtree($locationService->loadLocation($parentLocationId)->pathString),
                    new Criterion\ContentTypeIdentifier($this->getChildrenTypes($contentTypesDefinition)),
                    new Criterion\Visibility(Criterion\Visibility::VISIBLE)
                )
            );

            $query->sortClauses = array(
                new SortClause\Location\Priority(Query::SORT_ASC),
            );

            $query->limit = 100;
            $result = $this->repository->getSearchService()->findLocations($query);
        } catch (\Exception $e) {
            throw new NotFoundException('No values found query fail', $parentLocationId);
        }
        if ($result->totalCount > 0) {
            $resultElements = array();
            foreach ($result->searchHits as $searchHit) {
                $resultElements[] = $searchHit->valueObject;
            }
        }

        return $resultElements;
    }

    /**
     * Get children items.
     *
     * @param int $parentLocationId the location ID of the parent
     * @param string $contentType the type of the content types allowed in children list
     *
     * @return array of \eZ\Publish\Core\Repository\Values\Content\Location when children found
     *
     * @throws NotFoundException if no children found
     */
    public function getSubChildrenOrderByField($parentLocationId, $contentTypesDefinition = 'content_types', $contentType, $fieldIdentifier)
    {
        $resultElements = array();
        $locationService = $this->repository->getLocationService();
        $sortDirection = Query::SORT_ASC;
        $query = new LocationQuery();
        try {
            $query->query = new Criterion\LogicalAnd(
                array(
                    new Criterion\Subtree($locationService->loadLocation($parentLocationId)->pathString),
                    new Criterion\ContentTypeIdentifier($this->getChildrenTypes($contentTypesDefinition)),
                    new Criterion\Visibility(Criterion\Visibility::VISIBLE)
                )
            );

            $query->sortClauses = array(
                new SortClause\Field ($contentType, $fieldIdentifier, $sortDirection, $languageCode),
            );

            $query->limit = 100;
            $result = $this->repository->getSearchService()->findLocations($query);

        } catch (\Exception $e) {
            throw new NotFoundException('No values found query fail', $parentLocationId);
        }

        if ($result->totalCount > 0) {
            $resultElements = array();
            foreach ($result->searchHits as $searchHit) {
                $resultElements[] = $searchHit->valueObject;
            }
        }

        return $resultElements;
    }

    /**
     * Get children items.
     *
     * @param int $parentLocationId the location ID of the parent
     * @param string $contentType the type of the content types allowed in children list
     *
     * @return array of \eZ\Publish\Core\Repository\Values\Content\Location when children found
     *
     * @throws NotFoundException if no children found
     */
    public function getChildrenOrderByField($parentLocationId, $contentTypesDefinition = 'content_types', $contentType, $fieldIdentifier)
    {
        $resultElements = array();
        $locationService = $this->repository->getLocationService();
        $sortDirection = Query::SORT_DESC;
        $query = new LocationQuery();
        try {
            $query->query = new Criterion\LogicalAnd(
                array(
                    new Criterion\ParentLocationId($parentLocationId),
                    new Criterion\ContentTypeIdentifier($this->getChildrenTypes($contentTypesDefinition)),
                    new Criterion\Visibility(Criterion\Visibility::VISIBLE)
                )
            );

            $query->sortClauses = array(
                new SortClause\Field ($contentType, $fieldIdentifier, $sortDirection, $languageCode),
            );

            $query->limit = 100;
            $result = $this->repository->getSearchService()->findLocations($query);

        } catch (\Exception $e) {
            throw new NotFoundException('No values found query fail', $parentLocationId);
        }

        if ($result->totalCount > 0) {
            $resultElements = array();
            foreach ($result->searchHits as $searchHit) {
                $resultElements[] = $searchHit->valueObject;
            }
        }

        return $resultElements;
    }

    /**
     * Get children items.
     *
     * @param int $parentLocationId the location ID of the parent
     * @param string $contentTypesDefinition the type of the content types allowed in children list
     *
     * @return array of \eZ\Publish\Core\Repository\Values\Content when children found
     *
     * @throws NotFoundException if no children found
     */
    public function getChildrenContentByFieldRelation($parentLocationId, $contentTypesDefinition = 'content_types', $fieldName = '', $fieldValue = array(), $contentType, $fieldIdentifier)
    {
        $resultElements = array();
        $sortDirection = Query::SORT_ASC;
        $query = new LocationQuery();
        try {
            $query->query = new Criterion\LogicalAnd(
                array(
                    new Criterion\ParentLocationId($parentLocationId),
                    new Criterion\ContentTypeIdentifier($this->getChildrenTypes($contentTypesDefinition)),
                    new Criterion\FieldRelation($fieldName, Operator::IN, $fieldValue),
                    new Criterion\Visibility(Criterion\Visibility::VISIBLE)
                )
            );

            $query->sortClauses = array(
                new SortClause\Field ($contentType, $fieldIdentifier, $sortDirection, $languageCode),
            );

            $query->limit = 100;
            $result = $this->repository->getSearchService()->findContent($query);

        } catch (\Exception $e) {
            throw new NotFoundException('No values found query fail', $parentLocationId);
        }
        if ($result->totalCount > 0) {
            $resultElements = array();

            foreach ($result->searchHits as $searchHit) {

                $resultElements[] = $searchHit->valueObject;
            }
        }

        return $resultElements;
    }

    /**
     * Get allowed children content types.
     *
     * @param string $contentTypesDefinition the type of the content types allowed in children list
     *
     * @return array of content types identifiers, empty array if no settings found
     */

    public function getChildrenTypes($contentTypesDefinition)
    {
        try {
            $childrenSettings = $this->childrenSettings;
            if (empty($childrenSettings) or !is_array($childrenSettings)) {
                throw new \InvalidArgumentException('Children settings in not an array or empty');
            }

            if (array_key_exists($contentTypesDefinition, $childrenSettings)) {
                return $childrenSettings[$contentTypesDefinition];
            } else {
                throw new \InvalidArgumentException('Children settings type: ' . $contentTypesDefinition . ' is not defined');
            }
        } catch (\Exception $e) {
            if (isset($this->gebLogger)) {
                $this->gebLogger->error($e->getMessage());
            }

            return array();
        }
    }

    public function call_getChildrenByParams($params)
    {
        $resultElements = array();
        $query = new LocationQuery();
        $default = array(
            'parentLocationId' => '',
            'contentType' => '',
            'fields' => [],
            'fieldsRelations' => [],
            'sortClauses' => [],
            'limit' => 1000
        );

        $params = array_merge($default, $params);

        try {
            $listCriteria = array(
                new Criterion\ParentLocationId($params['parentLocationId']),
                new Criterion\ContentTypeIdentifier($params['contentType']),
                new Criterion\Visibility(Criterion\Visibility::VISIBLE),
            );
            $listSortClauses = array();

            if (!empty($params['fields'])) {
                $listCriteria[] = new Criterion\Field($params['fields']['name'], Operator::EQ, $params['fields']['value']);
            }

            if (!empty($params['fieldsRelations'])) {
                $listCriteria[] = new Criterion\FieldRelation($params['fieldsRelations']['name'], Operator::IN, $params['fieldsRelations']['value']);
            }

            if (!empty($params['sortClauses'])) {
                foreach ($params['sortClauses'] as $sortClause) {
                    $listSortClauses[] = $sortClause;
                }
            }

            $query->query = new Criterion\LogicalAnd($listCriteria);
            $query->sortClauses = $listSortClauses;
            $query->limit = $params['limit'];
            $result = $this->repository->getSearchService()->findLocations($query);

        } catch (\Exception $e) {
            throw new \Exception('No values found: query fail', $params['parentLocationId']);
        }

        if ($result->totalCount > 0) {
            $resultElements = array();
            foreach ($result->searchHits as $searchHit) {
                $resultElements[] = $searchHit->valueObject;
            }
        }
        return $resultElements;
    }

    public function __call($method, $arguments)
    {
        switch ($method) {
            case 'getChildren':
                switch (count($arguments)) {
                    case 1:
                        return $this->call_getChildrenByParams($arguments[0]);
                    case 2:
                        return $this->call_getChildren($arguments[0], $arguments[1]);
                    case 3:
                        return $this->call_getChildren($arguments[0], $arguments[1], $arguments[2]);
                    default:
                        throw new \InvalidArgumentException("One or more arguments are missing or left.");
                }
            default:
                throw new \Exception("Method not found for class " . ChildrenService::class);

        }
    }

    /**
     * Get children items.
     *
     * @param int $parentLocationId the location ID of the parent
     * @param string $contentTypesDefinition the type of the content types allowed in children list
     *
     * @return array of \eZ\Publish\Core\Repository\Values\Content when children found
     *
     * @throws NotFoundException if no children found
     */
    public function getChildrenContent($parentLocationId, $contentType = 'infopage', $limit = 100)
    {
        try {
            $query = new LocationQuery();
            $query->query = new Criterion\LogicalAnd(
                [
                    new Criterion\ParentLocationId($parentLocationId),
                    new Criterion\ContentTypeIdentifier($contentType),
                    new Criterion\Visibility(Criterion\Visibility::VISIBLE),
                ]
            );
            $query->limit = $limit;
            $query->sortClauses = array(
                new SortClause\Location\Priority(Query::SORT_ASC),
            );
            $resultLocation = $this->repository->getSearchService()->findLocations($query);

            $resultElements = array();

            if ($resultLocation->totalCount > 0) {
                foreach ($resultLocation->searchHits as $searchHit) {
                    $resultElements [] = $this->repository->getContentService()->loadContent($searchHit->valueObject->contentInfo->id);
                }
            }
            return $resultElements;

        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage());
        }

    }

    public function getLocationChildren($parentLocationId, $contentTypesDefinition = 'content_types')
    {
        try {
            $query = new LocationQuery();
            $query->query = new Criterion\LogicalAnd(
                [
                    new Criterion\ParentLocationId($parentLocationId),
                    new Criterion\ContentTypeIdentifier($this->getChildrenTypes($contentTypesDefinition)),
                    new Criterion\Visibility(Criterion\Visibility::VISIBLE),
                ]
            );
            $query->limit = 100;
            $query->sortClauses = array(
                new SortClause\Location\Priority(Query::SORT_ASC),
            );
            $resultLocation = $this->repository->getSearchService()->findLocations($query);

            $resultElements = array();

            if ($resultLocation->totalCount > 0) {
                $resultElements = array();
                foreach ($resultLocation->searchHits as $searchHit) {
                    $resultElements[] = $searchHit->valueObject;
                }
            }
            return $resultElements;

        } catch (\Exception $e) {
            $this->gebLogger->error($e->getMessage());
        }

    }

    public function getRecentChildren($parentLocationId, $contentTypesDefinition = 'content_types', $limit = 100)
    {
        $resultElements = array();
        $query = new LocationQuery();
        try {
            $query->query = new Criterion\LogicalAnd(
                array(
                    new Criterion\ParentLocationId($parentLocationId),
                    new Criterion\ContentTypeIdentifier($this->getChildrenTypes($contentTypesDefinition)),
                    new Criterion\Visibility(Criterion\Visibility::VISIBLE)
                )
            );

            $query->sortClauses = array(
                new SortClause\DatePublished(Query::SORT_DESC),
            );

            $query->limit = $limit;

            $result = $this->repository->getSearchService()->findLocations($query);
        } catch (\Exception $e) {
            throw new NotFoundException('No values found query fail', $parentLocationId);
        }

        if ($result->totalCount > 0) {
            $resultElements = array();
            foreach ($result->searchHits as $searchHit) {
                $resultElements[] = $searchHit->valueObject;
            }
        }
        return $resultElements;
    }

}
