<?php
/**
 * File containing the ContentService class.
 *
 * (c) www.aplyca.com
 * (c) Developer jdiaz@aplyca.com
 */

namespace App\TransmisionBundle\Services;

use \eZ\Publish\API\Repository\ContentService as eZContentService;
use \eZ\Publish\API\Repository\LocationService as eZLocationService;
use eZ\Publish\API\Repository\Repository;
use Psr\Log\LoggerInterface;

/**
 * Helper class for getting ccb content easily.
 */
class ContentService
{
    /**
     * @var \eZ\Publish\API\Repository\ContentService
     */
    private $eZContentService;

    /**
     * @var \eZ\Publish\API\Repository\LocationService
     */
    private $locationService;

    /**
     * @var LoggerInterface
     */
    protected $gebLogger;

    /**
     * @var \eZ\Publish\API\Repository\Repository
     */
    private $repository;

    public function __construct(
        eZContentService $contentService, 
        eZLocationService $locationService, 
        LoggerInterface $gebLogger = null,
        Repository $repository
    ){
        $this->gebLogger = $gebLogger;
        $this->eZContentService = $contentService;
        $this->locationService = $locationService;
        $this->repository = $repository;
    }

    /**
     * get only content and location from content relations
     * [getRelationsFieldsMenuArray description]
     * @param  [type]  $content [content]
     * @param  boolean $block   [description]
     * @return [type]           [description]
     */

    public function getRelations($content, $fieldIdentifier)
    {
        $relations = [];
        $contentIds = $content->getFieldValue($fieldIdentifier)->destinationContentIds;

        if (count($contentIds) > 0) {
            foreach ($contentIds as $contentId) {
                try {
                    $itemContent = $this->eZContentService->loadContent($contentId);
                    $itemLocation = $this->locationService->loadLocation($itemContent->contentInfo->mainLocationId);

                    if (!$itemLocation->invisible) {
                        $relations[$contentId] = [
                          'location' => $itemLocation,
                          'content' => $itemContent,
                        ];
                    }
                } catch (\Exception $e) {
                    $this->gebLogger->error($e->getMessage());

                    continue;
                }
            }
        }
        return $relations;
    }
    /**
     * get only content and location from content relations
     * [getRelationsFieldsMenuArray description]
     * @param  [type]  $content [content]
     * @param  boolean $block   [description]
     * @return [type]           [description]
     */

    public function getRelationContent($content, $fieldIdentifier)
    {
        $relationContent = [];

        if (is_object($content->getFieldValue($fieldIdentifier))){
            $contentId = $content->getFieldValue($fieldIdentifier)->destinationContentId;  
            if(!$contentId){
                return  $relationContent;
            } 
        } else {
            return  $relationContent;
        }
        
        try {
            $itemContent = $this->eZContentService->loadContent($contentId);
            $itemLocation = $this->locationService->loadLocation($itemContent->contentInfo->mainLocationId);

            if (!$itemLocation->invisible) {
                $relationContent = [
                    'location' => $itemLocation,
                    'content' => $itemContent,
                ];
            }
        } catch (\Exception $e) {
            $relationContent = [];
            $this->gebLogger->error($e->getMessage());
        }
        
        return $relationContent;
    }

    public function getContentById($contentId)
    {
        return $this->eZContentService->loadContent($contentId);
    }

    /**
     * get all destination contentInfo of all related fields
     * @return array
     */
    public function getAllContentInfoRelatedByContentId($contentId)
    {
        $result = array();
        $content = $this->getContentById($contentId);
        $relationsFields = $this->eZContentService->loadRelations($content->getVersionInfo());
        foreach ($relationsFields as $key => $relation) {
            $result[$relation->sourceFieldDefinitionIdentifier][] = $relation->getDestinationContentInfo();
        }
        return  $result;
    }
    /**
     * Returns true if a Field is defined and has content.
     *
     * @param object \eZ\Publish\Core\Repository\Values\Content\Content $content         the Content
     * @param string                                                    $fieldIdentifier the Field identifier
     *
     * @return bool
     */
    public function fieldHasContent($content, $fieldIdentifier = null)
    {
        try {
            if (array_key_exists($fieldIdentifier, $content->fields)) {
                if ($this->fieldHelper->isFieldEmpty($content, $fieldIdentifier)) {
                    throw new \Exception("Field '".$fieldIdentifier."' is empty");
                } else {
                    return true;
                }
            } else {
                throw new \Exception("Field '".$fieldIdentifier."' not found");
            }
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Returns the absolut location of object.
     * @param int                                                    $fieldIdentifier the Field identifier
     *
     * @return location
     */
    public function getLocation($locationId)
    {
        $locationService = $this->repository->getLocationService();
        return $locationService->loadLocation($locationId);
    }


    public function getContentByLocationId($locationId)
    {
        $location = $this->locationService->loadLocation($locationId);

        return $this->eZContentService->loadContent($location->contentInfo->id);
    }

    public function getLocationByLocationId($locationId)
    {
        return $this->locationService->loadLocation($locationId);
    }

    /**
     * Return the locations of a content by given content id
     *
     * @param int $contentId
     * @return Location[]
     */
    public function getAllLocationsByContentId($contentId)
    {
        try {
            $content = $this->repository->getContentService()->loadContent($contentId);
            return $this->repository->getLocationService()->loadLocations($content->getVersionInfo()->getContentInfo());
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Return the content of a content by a given id
     *
     * @param int $contentId
     * @return Content
     */
    public function getContentByContentId($contentId)
    {
        try {
            $content = $this->repository->getContentService()->loadContent($contentId);
        } catch (\Throwable $th) {
            $content = [];
        }
        return $content;
    }

}
