<?php

/**
 * File containing the ContentService class.
 *
 * (c) www.aplyca.com
 * (c) Developer dduarte@aplyca.com
 */

namespace App\TransmisionBundle\Services;

use App\TransmisionBundle\Services\ChildrenService;

/**
 * Helper class for getting ccb content easily.
 */
class LayoutService
{
    /**
     * @var \App\TransmisionBundle\Services\ContentService
     */
    private $contentService;
    private $childrenService;

    /**
     * @var array
     */
    private $params;

    public function __construct(
        ContentService $contentService,
        ChildrenService $childrenService,
        array $params
    ) {
        $this->contentService = $contentService;
        $this->childrenService = $childrenService;
        $this->params = $params;
    }

    /**
     * get Layout content
     * @return [type]           [description]
     */
    public function getContent()
    {
        return $this->contentService->getContentById($this->params["layout_content_id"]);
    }

    /**
     * get Footer content
     * @return [type]           [description]
     */
    public function getFooterContent()
    {
        return $this->contentService->getContentById($this->params["footer_content_id"]);
    }

    public function getSocialNetworks()
    {
        return $this->childrenService->getChildrenContent($this->params['socialnetworks_location_id'], 'social_network_transmision');
    }
    /**
     * get Footer content
     * @return [type]           [description]
     */
    public function getFooterColumns($content, $identifiers)
    {
        $columns = [];
        foreach ($identifiers as $identifier) {
            try {
                $columns[$identifier] = $this->contentService->getRelations($content, $identifier);
            } catch (\Exception $e) {
                continue;
            }
        }

        return $columns;
    }
}
