<?php

namespace App\TransmisionBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('contentRaw', [$this, 'formatContentRaw']),
        ];
    }

    public function formatContentRaw($url)
    {
       return htmlspecialchars_decode($url);
    }
}
