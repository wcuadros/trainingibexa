{if $attribute.content.filename}
{def
  $file_path=concat("/content/download/", $attribute.contentobject_id, "/", $attribute.id, "/", $attribute.content.original_filename)|ezurl(no, full)


  $preview_image=$attribute.object.data_map.thumbnail.content.video_poster.full_path|ezurl(no, full)
  $default_size=array(100%,388)
  $player_size=array($attribute.content.width,$attribute.content.height)
  $poster=$attribute.object.data_map.image.content.original.full_path|ezurl('no')
}
    {switch name=mediaType match=$attribute.content.mime_type}
    {case match='video/mp4'}
    {if and($player_size[0]|ne(0),$player_size[1]|ne(0))}
      {set $default_size=$player_size}
    {/if}
    
    <div class="card-thumbnail">
     	<div class="responsive-embed">
      		<video controls {if $attribute.object.data_map.autoplay_video.content|eq(1)}autoplay{/if} preload="none" width="{$default_size[0]}" height="{$default_size[1]}" poster='{$attribute.object.data_map.thumbnail.content.original.url|ezurl('no')}'>
        		<source src="{$file_path}" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
        		<object class="vjs-flash-fallback" width="{$default_size[0]}" height="{$default_size[1]}" type="application/x-shockwave-flash"
        			data="https://releases.flowplayer.org/swf/flowplayer-3.2.1.swf">
          			<param name="movie" value="https://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
          			<param name="allowfullscreen" value="true" />
          			<param name="flashvars" value='config={ldelim}"canvas":{ldelim}"background-color":"#EBEBEB", "backgroundImage":{$preview_image|ezurl("")}, "backgroundGradient": "none"{rdelim}, "playlist":[{ldelim}"url": "{$file_path}","autoPlay":false, "autoBuffering":false{rdelim}]{rdelim}' />
        		</object>
      		</video>
    	</div>
      </div>
    {/case}
    {/switch}
{else}
    {'No media file is available.'|i18n( 'design/ezwebin/view/ezmedia' )}
{/if}
