{if $object.data_map.file.has_content}
    {def $file = $object.data_map.file}
        <ul class="geb-list-archive u-pl-2">
            <li>
                <a href={concat("content/download/", $file.contentobject_id, "/", $file.id, "/file/", $file.content.original_filename)|ezurl}>
                <span class="geb-list-archive__item-icon" data-icon="arrow-down-circle"></span> 
                <span>
                    {$file.content.original_filename}
                </span>
                </a>
            </li>
        </ul>
    {undef $file}
{/if}
