<?php /* #?ini charset="utf-8"?

[General]
AllowedTypes[]=Transmision_Promos
AllowedTypes[]=Transmision_General

[Transmision_Promos]
Name=Transmision Promo
ManualAddingOfItems=enabled
CustomAttributes[]=variation_type
CustomAttributeNames[variation_type]=Tipo de variación
CustomAttributeTypes[variation_type]=select
CustomAttributeSelection_variation_type[]
CustomAttributeSelection_variation_type[none]=No Aplica
CustomAttributeSelection_variation_type[left]=Izquierda
CustomAttributeSelection_variation_type[right]=Derecha
CustomAttributes[]=container_type
CustomAttributeNames[container_type]=Tipo del bloque
CustomAttributeTypes[container_type]=select
CustomAttributeSelection_container_type[]
CustomAttributeSelection_container_type[default]= Cuatro columnas
CustomAttributeSelection_container_type[one_column_one_highlights]=Una columna y un destacado(3 columnas) 
CustomAttributeSelection_container_type[two_column_one_highlights]= Dos columnas y un destacado ( 2 columnas)
ViewList[]=transmision_promo
ViewName[transmision_promo]=Transmision promo
AllowedClasses[]=gallery
AllowedClasses[]=quantities
AllowedClasses[]=highlighted_news
AllowedClasses[]=featured_transmission
AllowedClasses[]=infopage
AllowedClasses[]=frontpage
AllowedClasses[]=folder
AllowedClasses[]=highlighted_multimedia
AllowedClasses[]=highlighted_content_media
AllowedClasses[]=landing_page
AllowedClasses[]=highlighted_color

[Transmision_General]
Name=Transmision general
ManualAddingOfItems=enabled
CustomAttributes[]=full_gallery
CustomAttributeNames[full_gallery]=Galería Destacada
CustomAttributeTypes[full_gallery]=select
CustomAttributeSelection_full_gallery[]
CustomAttributeSelection_full_gallery[false]=No Aplica
CustomAttributeSelection_full_gallery[true]=Aplica
ViewList[]=transmision_General
ViewName[transmision_General]=Transmisión general
AllowedClasses[]=gallery
AllowedClasses[]=slider_gallery
AllowedClasses[]=quantities
AllowedClasses[]=highlighted_news
AllowedClasses[]=featured_transmission
AllowedClasses[]=infopage
AllowedClasses[]=highlighted_multimedia
AllowedClasses[]=landing_page


*/
