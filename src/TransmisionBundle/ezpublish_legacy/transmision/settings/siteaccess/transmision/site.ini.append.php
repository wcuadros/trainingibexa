<?php /* #?ini charset="utf-8"?

[SiteSettings]
SiteName=Transmision
RootNodeDepth=2
DefaultAccess=transmision
AllowedRedirectHosts[]=www.transmision.com
SiteURL=www.transmision.com

[SiteAccessSettings]
RequireUserLogin=false

[UserSettings]
AnonymousUserID=7925

[InformationCollectionSettings]
EmailReceiver=

[DesignSettings]
SiteDesign=transmision
AdditionalSiteDesignList[]
AdditionalSiteDesignList[]=ezdemo
AdditionalSiteDesignList[]=ezflow
AdditionalSiteDesignList[]=base
AdditionalSiteDesignList[]=standard

[RegionalSettings]
Locale=esl-CO
ContentObjectLocale=esl-CO
ShowUntranslatedObjects=disabled
SiteLanguageList[]=esl-CO
TextTranslation=enabled

[Session]
ForceStart=enabled

[SiteAccessRules]
Rules[]
Rules[]=access;enable
Rules[]=moduleall
Rules[]=access;disable
Rules[]=module;ezinfo
Rules[]=module;user/forgotpassword

*/
