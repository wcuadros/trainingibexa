<?php /* #?ini charset="utf-8"?

[full_image]
Source=node/view/full.tpl
MatchFile=full/image.tpl
Subdir=templates
Match[class_identifier]=image

[view_ezmedia_video_file]
Source=content/datatype/view/ezmedia.tpl
MatchFile=content/datatype/view/ezmedia/video/file.tpl
Subdir=templates
Match[class_identifier]=video
Match[attribute_identifier]=file

[embed_image]
Source=content/view/embed.tpl
MatchFile=embed/image.tpl
Subdir=templates
Match[class_identifier]=image

[embed_inline_image]
Source=content/view/embed-inline.tpl
MatchFile=embed-inline/image.tpl
Subdir=templates
Match[class_identifier]=image

[full_file]
Source=node/view/full.tpl
MatchFile=full/file.tpl
Subdir=templates
Match[class_identifier]=file

[embed_file]
Source=content/view/embed.tpl
MatchFile=embed/file.tpl
Subdir=templates
Match[class_identifier]=file

[embed_inline_file]
Source=content/view/embed-inline.tpl
MatchFile=embed-inline/file.tpl
Subdir=templates
Match[class_identifier]=file
*/ ?>
