<?php
/**
 * File containing the GEBSearchAdapter class.
 *
 * @copyright Copyright (C) eZ Systems AS. All rights reserved.
 * @license For full copyright and license information view LICENSE file distributed with this source code.
 *
 * @version //autogentag//
 */

namespace App\TransmisionBundle\Pagination\Pagerfanta;

use eZ\Publish\Core\MVC\Legacy\Kernel;
use eZ\Publish\API\Repository\LocationService;

/**
 * Pagerfanta adapter for eZ Publish content search.
 * Will return results as Content objects. (PHP 7 Version)
 */
class GEBSearchAdapter extends GEBSearchHitAdapter
{
    /**
     * @var eZ\Publish\API\Repository\LocationService
     */
    protected $locationService;

    public function __construct(Kernel $legacyKernel, $searchParams, LocationService $locationService)
    {
        parent::__construct($legacyKernel, $searchParams);
        $this->locationService = $locationService;
    }

    /**
     * Returns a slice of the results as Content objects.
     *
     * @param integer $offset The offset.
     * @param integer $length The length.
     *
     * @return \eZ\Publish\API\Repository\Values\Content\Content[]
     */
    public function getSlice($offset, $length)
    {
        $list = array();

        foreach (parent::getSlice($offset, $length) as $hit) {
            $location = $this->locationService->loadLocation($hit->MainNodeID);

            if (!$location->invisible) {
                $list[] = $location;
            }
        }

        return $list;
    }
}

