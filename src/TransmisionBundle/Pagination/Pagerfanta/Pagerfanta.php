<?php
/**
 * File containing the Pagerfanta class.
 *
 * (c) www.aplyca.com
 * (c) Developer dduarte@aplyca.com
 */

namespace App\TransmisionBundle\Pagination\Pagerfanta;

/**
 * Pagerfanta adapter for eZ Publish content search.
 * Will return results as SearchHit objects.
 */
class Pagerfanta extends \Pagerfanta\Pagerfanta
{
    /**
     * @var array
     */
    public $extras;

    /**
     * Returns the extras of the results.
     *
     * @return array
     */
    public function getExtras()
    {
        if ($this->notCachedExtras()) {
            //$this->extras = $this->getAdapter()->getExtras();
            $this->extras = ['spellcheck_collation' => ''];
        }

        return $this->extras;
    }
    private function notCachedExtras()
    {
        return $this->extras === null;
    }
}

