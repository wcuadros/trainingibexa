<?php
/**
 * File containing the GEBSearchHitAdapter class.
 *
 * @copyright Copyright (C) eZ Systems AS. All rights reserved.
 * @license For full copyright and license information view LICENSE file distributed with this source code.
 *
 * @version //autogentag//
 */

namespace App\TransmisionBundle\Pagination\Pagerfanta;

use Pagerfanta\Adapter\AdapterInterface;
use eZ\Publish\Core\MVC\Legacy\Kernel;
use eZFunctionHandler;
use eZHTTPTool;

/**
 * Pagerfanta adapter for eZ Publish content search.
 * Will return results as SearchHit objects.
 */
class GEBSearchHitAdapter implements AdapterInterface
{
    /**
     * @var eZ\Publish\Core\MVC\Legacy\Kernel
     */
    private $legacyKernel;

    /**
     * @var int
     */
    private $nbResults;

    /**
     * @var string
     */
    private $extras;

    /**
     * @var array
     */
    private $defaultSearchParams;

    /**
     * Constructor.
     *
     * @param \eZ\Publish\Core\MVC\Legacy\Kernel $legacyKernel
     * @param string                             $searchTerm
     */
    public function __construct(Kernel $legacyKernel, $searchParams)
    {
        $this->legacyKernel = $legacyKernel;
        $this->defaultSearchParams = $searchParams;
    }

    /**
     * Returns the number of results.
     *
     * @return integer The number of results.
     */
    public function getNbResults()
    {
        if (isset($this->nbResults)) {
            return $this->nbResults;
        }

        $searchResults = $this->doSearch($this->defaultSearchParams + array( 'limit' => 0 ));

        return $this->nbResults = $searchResults['SearchCount'];
    }

    /**
     * Returns the extras of results.
     *
     * @return array with the search extras.
     */
    public function getExtras()
    {
        if (isset($this->extras)) {
            return $this->extras;
        }

        $searchResults = $this->doSearch($this->defaultSearchParams);

        return $this->extras = $this->getExtrasArray($searchResults["SearchExtras"]);
    }

    /**
     * Returns the extras array of results.
     *
     * @return array with the search extras in array.
     */
    public function getExtrasArray($searchExtras)
    {
        return array(
            "spellcheck_collation" => $searchExtras->attribute("spellcheck_collation"),
        );
    }

    /**
     * Returns as slice of the results, as SearchHit objects.
     *
     * @param integer $offset The offset.
     * @param integer $length The length.
     *
     * @return \eZ\Publish\API\Repository\Values\Content\Search\SearchHit The slice.
     */
    public function getSlice($offset, $length)
    {
        $searchParams = $this->defaultSearchParams + array(
            'offset' => $offset,
            'limit' => $length,
        );

        $searchResults = $this->doSearch($searchParams);

        if (!isset($this->nbResults)) {
            $this->nbResults = $searchResults['SearchCount'];
        }

        if (!isset($this->extras)) {
            $this->extras = $this->getExtrasArray($searchResults["SearchExtras"]);
        }

        return $searchResults['SearchResult'];
    }

    /**
     * Executes the eZFind query via callback function.
     *
     * @param array $searchParams
     *
     * @return array eZFindResult
     */
    private function doSearch(array $searchParams)
    {
       /* $searchResults = $this->legacyKernel->runCallback(
            function () use ($searchParams) {

                $filter = $searchParams['filter'];
                if (self::existsFilterVariable()) {
                    $filter = self::getFilterParameters($searchParams['additionalFilter']);
                } elseif(!is_array($filter)) {
                    $filter = (string)$filter;
                }

                $search = eZFunctionHandler::execute(
                    'ezfind',
                    'search',
                    array(
                        'query' => $searchParams['searchText'],
                        'subtree_array' => $searchParams['subTree'],
                        'sort_by' => $searchParams['sortBy'],
                        'class_id' => $searchParams['classId'],
                        'facet' => $searchParams['facet'],
                        'filter' => $filter,
                        'offset' => $searchParams['offset'],
                        'limit' => $searchParams['limit'],
                        'spell_check' => array(true),
                    )
                );

                return $search;
            }
        );*/

        return ['SearchCount' => 0, 'SearchExtras' => null];
    }

    public static function existsFilterVariable()
    {
        $http = eZHTTPTool::instance();
        if ($http->hasGetVariable('filter')) {
            return true;
        }

        return false;
    }

    public static function getFilterParameters($additionalFilter)
    {
        $http = eZHTTPTool::instance();
        $filterList = array();

        if ($http->hasGetVariable('filter')) {
            foreach ($http->getVariable('filter') as $filterCond) {
                list($name, $value) = explode(':', $filterCond, 2);
                $filterList[$name][] = $name.':'.$value;
            }
        }

        foreach ($additionalFilter as $filterId => $filterValue) {
            if (!array_key_exists($filterId, $filterList)) {
                $filterList[$filterId][] = $filterId.':'.$filterValue;
            }
        }

        foreach ($filterList as $filterId => $filterValue) {
            if (count($filterValue) > 1) {
                $filterList[$filterId] = array_merge(array('OR'), $filterValue);
            }
        }

        return $filterList;
    }
}

