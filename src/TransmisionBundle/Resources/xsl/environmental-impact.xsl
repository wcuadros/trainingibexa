<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
        version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:xhtml="http://ez.no/namespaces/ezpublish3/xhtml/"
        xmlns:custom="http://ez.no/namespaces/ezpublish3/custom/"
        xmlns:image="http://ez.no/namespaces/ezpublish3/image/"
        exclude-result-prefixes="xhtml custom image"
        xmlns:math="http://exslt.org/math"
        extension-element-prefixes="math">

  <xsl:template match="custom[@name='environmental-impact']">
    <xsl:variable name="identifier">
      <xsl:value-of select="@custom:identifier"/>
    </xsl:variable>
    <xsl:variable name="accordion_id">
      <xsl:value-of select="(floor(math:random()*10000) mod 10000) + 1" />
    </xsl:variable>
    <xsl:for-each select="child::*">
      <xsl:if test="count(child::*) &gt; 0">
        <ul class="geb-accordion-a" data-accordion="" data-multi-expand="true" data-allow-all-closed="true">
          <xsl:for-each select="child::*">
              <li data-accordion-item="">
                <a href="#!" class="geb-accordion-a__title" id="owerlg-accordion-label" aria-expanded="false" aria-selected="false"><xsl:value-of select="@custom:title"/></a>
                  <div class="geb-accordion-a__content u-p-0" data-tab-content="" role="tabpanel" aria-labelledby="owerlg-accordion-label" aria-hidden="true" id="owerlg-accordion" style="display: none;">
                      <ul class="geb-accordion-b" data-accordion="" data-multi-expand="true" data-allow-all-closed="true" role="tablist" data-n="4cr4pt-n">
                          <xsl:for-each select="child::*">
                            <xsl:choose>
                                <xsl:when test="count(child::custom[@name='environmental-impact-item']) &gt; 0">
                                  <xsl:for-each select="child::custom[@name='environmental-impact-item']">
                                    <xsl:variable name="impact-stretches-children">
                                        <xsl:copy-of select="@custom:environmental-impact-item"/>
                                    </xsl:variable>
                                    <xsl:if test="$impact-stretches-children">
                                        <li data-accordion-item="">
                                            <a href="#!" class="geb-accordion-b__title" aria-controls="{concat($identifier,'-',$accordion_id, position(), '-accordion')}" role="tab" id="{concat($identifier,'-',$accordion_id, position(), '-accordion-label')}" aria-expanded="false" aria-selected="false">
                                              <xsl:value-of select="@custom:title"/>
                                            </a>
                                            <div class="geb-accordion-b__content" data-tab-content="" role="tabpanel" aria-labelledby="{concat($identifier,'-',$accordion_id, position(), '-accordion-label')}" aria-hidden="true" id="{concat($identifier,'-',$accordion_id, position(), '-accordion')}">
                                              <xsl:copy-of select="@*"/>
                                              <xsl:apply-templates/>
                                            </div>
                                        </li>
                                    </xsl:if>
                                  </xsl:for-each>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:copy-of select="@custom:environmental-impact-stretches"/>
                                    <xsl:apply-templates/>
                                </xsl:otherwise>
                            </xsl:choose>
                          </xsl:for-each>
                      </ul>
                  </div>
              </li>
          </xsl:for-each>
        </ul>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>
