<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
        version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:xhtml="http://ez.no/namespaces/ezpublish3/xhtml/"
        xmlns:custom="http://ez.no/namespaces/ezpublish3/custom/"
        xmlns:image="http://ez.no/namespaces/ezpublish3/image/"
        exclude-result-prefixes="xhtml custom image"
        xmlns:math="http://exslt.org/math"
        extension-element-prefixes="math">

  <xsl:template match="custom[@name='transmision-accordion']">
    <xsl:variable name="identifier">
      <xsl:value-of select="@custom:identifier"/>
    </xsl:variable>
    <xsl:for-each select="child::*">
      <xsl:if test="count(child::*) &gt; 0">
        <ul class="geb-accordion-c" data-accordion="" data-multi-expand="true" data-allow-all-closed="true">
          <xsl:for-each select="child::*">
              <li data-accordion-item="">
                <a href="#!" class="geb-accordion-c__title" id="owerlg-accordion-label" aria-expanded="false" aria-selected="false"><xsl:value-of select="@custom:title"/></a>
                  <div class="geb-accordion-c__content u-p-4" data-tab-content="" role="tabpanel" aria-labelledby="owerlg-accordion-label" aria-hidden="true" id="owerlg-accordion" style="display: none;">
                    <xsl:copy-of select="@*"/>
                    <xsl:apply-templates/>
                  </div>
              </li>
          </xsl:for-each>
        </ul>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>
