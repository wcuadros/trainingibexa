SET default_storage_engine=InnoDB;
-- Set storage engine schema version number
UPDATE ezsite_data SET value='6.4.0' WHERE name='ezpublish-version';

--
-- EZP-25880: Make ezuser.login case in-sensitive across databases, using case in-sensitive index
--

ALTER TABLE ezuser DROP KEY ezuser_login, ADD UNIQUE KEY ezuser_login (login);

SET default_storage_engine=InnoDB;
-- Set storage engine schema version number
UPDATE ezsite_data SET value='6.13.0' WHERE name='ezpublish-version';

-- IMPORTANT:
-- * This only covers eZ Platform kernel, see doc for all steps (incl. database schema) needed when updating: https://doc.ezplatform.com/en/2.5/updating/updating_ez_platform/

--
-- EZP-25880: Make ezuser.login case in-sensitive across databases, using case in-sensitive index
--

ALTER TABLE ezuser DROP KEY ezuser_login, ADD UNIQUE KEY ezuser_login (login);

--
-- EZP-26397: marked User.user_account as not searchable
--
UPDATE ezcontentclass_attribute SET is_searchable = 0 WHERE data_type_string = 'ezuser';


-- BEGIN script for EZP-26070: PHP API support for new Publish permission

-- Create content/publish policy for roles that have content/create or content/edit policies
INSERT INTO `ezpolicy` (`module_name`, `function_name`, `original_id`, `role_id`)
  SELECT DISTINCT 'content', 'publish', `original_id`, `role_id`
  FROM `ezpolicy`
  WHERE `module_name` = 'content' AND `function_name` IN ('create', 'edit')
    AND NOT EXISTS (SELECT 1 FROM `ezpolicy` WHERE `module_name` = 'content' AND `function_name` = 'publish');

-- Create limitations for just created policy (policies)
INSERT INTO `ezpolicy_limitation` (`identifier`, `policy_id`)
  SELECT DISTINCT `l0`.`identifier`, `p1`.`id`
  FROM `ezpolicy_limitation` AS `l0`
  JOIN `ezpolicy` AS `p0` ON `l0`.`policy_id` = `p0`.`id`
  JOIN `ezpolicy` AS `p1` ON `p0`.`role_id` = `p1`.`role_id` AND `p1`.`module_name` = 'content' AND `p1`.`function_name` = 'publish'
  WHERE `p0`.`module_name` = 'content' AND `p0`.`function_name` IN ('create', 'edit');

-- Create content/publish limitation values entries based on existing entries matched by limitation identifier and role
INSERT INTO `ezpolicy_limitation_value` (`limitation_id`, `value`)
  SELECT `l1`.`id`, `value`
  FROM `ezpolicy_limitation` AS `l0`
  JOIN `ezpolicy_limitation_value` AS `lv0` ON `lv0`.`limitation_id` = `l0`.`id`
  JOIN `ezpolicy` AS `p0` ON `p0`.`id` = `l0`.`policy_id`
  JOIN `ezpolicy_limitation` AS `l1` ON `l0`.`identifier` = `l1`.`identifier`
  JOIN `ezpolicy` AS `p1`
    ON `l1`.`policy_id` = `p1`.`id`
    AND `p1`.`module_name` = 'content'
    AND `p1`.`function_name` = 'publish'
    AND `p1`.`role_id` = `p0`.`role_id`;

-- END script for EZP-26070

--
-- EZP-24744: Increase password security
--

ALTER TABLE ezuser CHANGE password_hash password_hash VARCHAR(255) default NULL;

SET default_storage_engine=InnoDB;
-- Set storage engine schema version number
UPDATE ezsite_data SET value='6.4.0' WHERE name='ezpublish-version';

--
-- EZP-25880: Make ezuser.login case in-sensitive across databases, using case in-sensitive index
--

ALTER TABLE ezuser DROP KEY ezuser_login, ADD UNIQUE KEY ezuser_login (login);

SET default_storage_engine=InnoDB;
-- Set storage engine schema version number
UPDATE ezsite_data SET value='6.4.0' WHERE name='ezpublish-version';

--
-- EZP-25880: Make ezuser.login case in-sensitive across databases, using case in-sensitive index
--

ALTER TABLE ezuser DROP KEY ezuser_login, ADD UNIQUE KEY ezuser_login (login);

--
-- EZP-25817: Invalid subtree path when assigning role with subtree limitation
--

UPDATE ezuser_role SET limit_value = CONCAT(limit_value, '/') WHERE limit_identifier = 'Subtree' AND limit_value REGEXP '[[:digit:]]$';

SET default_storage_engine=InnoDB;
-- Set storage engine schema version number
UPDATE ezsite_data SET value='6.6.0' WHERE name='ezpublish-version';

--
-- EZP-26397: marked User.user_account as not searchable
--
UPDATE ezcontentclass_attribute SET is_searchable = 0 WHERE data_type_string = 'ezuser';

-- BEGIN script for EZP-26070: PHP API support for new Publish permission

-- Create content/publish policy for roles that have content/create or content/edit policies
INSERT INTO `ezpolicy` (`module_name`, `function_name`, `original_id`, `role_id`)
  SELECT DISTINCT 'content', 'publish', `original_id`, `role_id`
  FROM `ezpolicy`
  WHERE `module_name` = 'content' AND `function_name` IN ('create', 'edit')
    AND NOT EXISTS (SELECT 1 FROM `ezpolicy` WHERE `module_name` = 'content' AND `function_name` = 'publish');

-- Create limitations for just created policy (policies)
INSERT INTO `ezpolicy_limitation` (`identifier`, `policy_id`)
  SELECT DISTINCT `l0`.`identifier`, `p1`.`id`
  FROM `ezpolicy_limitation` AS `l0`
  JOIN `ezpolicy` AS `p0` ON `l0`.`policy_id` = `p0`.`id`
  JOIN `ezpolicy` AS `p1` ON `p0`.`role_id` = `p1`.`role_id` AND `p1`.`module_name` = 'content' AND `p1`.`function_name` = 'publish'
  WHERE `p0`.`module_name` = 'content' AND `p0`.`function_name` IN ('create', 'edit');

-- Create content/publish limitation values entries based on existing entries matched by limitation identifier and role
INSERT INTO `ezpolicy_limitation_value` (`limitation_id`, `value`)
  SELECT `l1`.`id`, `value`
  FROM `ezpolicy_limitation` AS `l0`
  JOIN `ezpolicy_limitation_value` AS `lv0` ON `lv0`.`limitation_id` = `l0`.`id`
  JOIN `ezpolicy` AS `p0` ON `p0`.`id` = `l0`.`policy_id`
  JOIN `ezpolicy_limitation` AS `l1` ON `l0`.`identifier` = `l1`.`identifier`
  JOIN `ezpolicy` AS `p1`
    ON `l1`.`policy_id` = `p1`.`id`
    AND `p1`.`module_name` = 'content'
    AND `p1`.`function_name` = 'publish'
    AND `p1`.`role_id` = `p0`.`role_id`;

-- END script for EZP-26070

SET default_storage_engine=InnoDB;
-- Set storage engine schema version number
UPDATE ezsite_data SET value='6.7.8' WHERE name='ezpublish-version';

CREATE INDEX `ezcontentobject_tree_contentobject_id_path_string` ON `ezcontentobject_tree` (`path_string`, `contentobject_id`);
CREATE INDEX `ezcontentobject_section` ON `ezcontentobject` (`section_id`);

SET default_storage_engine=InnoDB;
-- Set storage engine schema version number
UPDATE ezsite_data SET value='6.11.0' WHERE name='ezpublish-version';

-- eZ Publish users who has upgraded will get warning that table was missing,
-- this is fine as you'll already removed it when upgrading to 5.4

DROP TABLE IF EXISTS  ezsearch_return_count;

SET default_storage_engine=InnoDB;
-- Set storage engine schema version number
UPDATE ezsite_data SET value='6.12.0' WHERE name='ezpublish-version';

--
-- EZP-24744: Increase password security
--

ALTER TABLE ezuser CHANGE password_hash password_hash VARCHAR(255) default NULL;

UPDATE ezsite_data SET value='7.5.0' WHERE name='ezpublish-version';

-- IMPORTANT:
-- * If you use DFS (cluster), also make sure to apply "dbupdate-7.1.0-to-7.2.0-dfs.sql"
-- * This only covers eZ Platform kernel, see doc for all steps (incl. database schema) needed when updating: https://doc.ezplatform.com/en/2.5/updating/updating_ez_platform/


-- 6.13.0/7.1.0 - 7.2.0

--
-- EZP-28950: MySQL UTF8 doesn't support 4-byte chars
-- This shortens indexes so that 4-byte content can fit.
-- After running these, convert the table character set, see doc/upgrade/7.2.md
--

BEGIN;
ALTER TABLE `ezbasket` DROP KEY `ezbasket_session_id`;
ALTER TABLE `ezbasket` ADD KEY `ezbasket_session_id` (`session_id` (191));

ALTER TABLE `ezcollab_group` DROP KEY `ezcollab_group_path`;
ALTER TABLE `ezcollab_group` ADD KEY `ezcollab_group_path` (`path_string` (191));

ALTER TABLE `ezcontent_language` DROP KEY `ezcontent_language_name`;
ALTER TABLE `ezcontent_language` ADD KEY `ezcontent_language_name` (`name` (191));

ALTER TABLE `ezcontentobject_attribute` DROP KEY `sort_key_string`;
ALTER TABLE `ezcontentobject_attribute` ADD KEY `sort_key_string` (`sort_key_string` (191));

ALTER TABLE `ezcontentobject_name` DROP KEY `ezcontentobject_name_name`;
ALTER TABLE `ezcontentobject_name` ADD KEY `ezcontentobject_name_name` (`name` (191));

ALTER TABLE `ezcontentobject_trash` DROP KEY `ezcobj_trash_path`;
ALTER TABLE `ezcontentobject_trash` ADD KEY `ezcobj_trash_path` (`path_string` (191));

ALTER TABLE `ezcontentobject_tree` DROP KEY `ezcontentobject_tree_path`;
ALTER TABLE `ezcontentobject_tree` ADD KEY `ezcontentobject_tree_path` (`path_string` (191));

ALTER TABLE `ezimagefile` DROP KEY `ezimagefile_file`;
ALTER TABLE `ezimagefile` ADD KEY `ezimagefile_file` (`filepath` (191));

ALTER TABLE `ezkeyword` DROP KEY `ezkeyword_keyword`;
ALTER TABLE `ezkeyword` ADD KEY `ezkeyword_keyword` (`keyword` (191));

ALTER TABLE `ezorder_status` DROP KEY `ezorder_status_name`;
ALTER TABLE `ezorder_status` ADD KEY `ezorder_status_name` (`name` (191));

ALTER TABLE `ezpolicy_limitation_value` DROP KEY `ezpolicy_limitation_value_val`;
ALTER TABLE `ezpolicy_limitation_value` ADD KEY `ezpolicy_limitation_value_val` (`value` (191));

ALTER TABLE `ezprest_authcode` DROP PRIMARY KEY;
ALTER TABLE `ezprest_authcode` ADD PRIMARY KEY (`id` (191));

ALTER TABLE `ezprest_authcode` DROP KEY `authcode_client_id`;
ALTER TABLE `ezprest_authcode` ADD KEY `authcode_client_id` (`client_id` (191));

ALTER TABLE `ezprest_clients` DROP KEY `client_id_unique`;
ALTER TABLE `ezprest_clients` ADD UNIQUE KEY `client_id_unique` (`client_id` (191),`version`);

ALTER TABLE `ezprest_token` DROP PRIMARY KEY;
ALTER TABLE `ezprest_token` ADD PRIMARY KEY (`id` (191));

ALTER TABLE `ezprest_token` DROP KEY `token_client_id`;
ALTER TABLE `ezprest_token` ADD KEY `token_client_id` (`client_id` (191));

ALTER TABLE `ezsearch_object_word_link` DROP KEY `ezsearch_object_word_link_identifier`;
ALTER TABLE `ezsearch_object_word_link` ADD KEY `ezsearch_object_word_link_identifier` (`identifier` (191));

ALTER TABLE `ezsearch_search_phrase` DROP KEY IF EXISTS `ezsearch_search_phrase_phrase`;
ALTER TABLE `ezsearch_search_phrase` ADD UNIQUE KEY `ezsearch_search_phrase_phrase` (`phrase`);

ALTER TABLE `ezurl` DROP KEY `ezurl_url`;
ALTER TABLE `ezurl` ADD KEY `ezurl_url` (`url` (191));

ALTER TABLE `ezurlalias` DROP KEY `ezurlalias_desturl`;
ALTER TABLE `ezurlalias` ADD KEY `ezurlalias_desturl` (`destination_url` (191));

ALTER TABLE `ezurlalias` DROP KEY `ezurlalias_source_url`;
ALTER TABLE `ezurlalias` ADD KEY `ezurlalias_source_url` (`source_url` (191));

--
-- EZP-29146: As a developer, I want a API to manage bookmarks
--

ALTER TABLE `ezcontentbrowsebookmark`
ADD INDEX `ezcontentbrowsebookmark_user_location` (`node_id`, `user_id`);

ALTER TABLE `ezcontentbrowsebookmark`
ADD INDEX `ezcontentbrowsebookmark_location` (`node_id`);

UPDATE `ezcontentbrowsebookmark`
SET `ezcontentbrowsebookmark`.`user_id` = 26421
WHERE `ezcontentbrowsebookmark`.`user_id` != 26421;

ALTER TABLE `ezcontentbrowsebookmark`
ADD CONSTRAINT `ezcontentbrowsebookmark_user_fk`
  FOREIGN KEY (`user_id`)
  REFERENCES `ezuser` (`contentobject_id`)
  ON DELETE CASCADE
  ON UPDATE NO ACTION;

--
-- EZEE-2081: Move NotificationBundle into AdminUI
--

CREATE TABLE IF NOT EXISTS `eznotification` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL DEFAULT 0,
  `is_pending` tinyint(1) NOT NULL DEFAULT '1',
  `type` varchar(128) NOT NULL DEFAULT '',
  `created` int(11) NOT NULL DEFAULT 0,
  `data` blob,
  PRIMARY KEY (`id`),
  KEY `eznotification_owner` (`owner_id`),
  KEY `eznotification_owner_is_pending` (`owner_id`, `is_pending`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

COMMIT;

ALTER TABLE ezcontentobject_trash add  trashed int(11) NOT NULL DEFAULT '0';

DROP TABLE IF EXISTS `ezcontentclass_attribute_ml`;
CREATE TABLE `ezcontentclass_attribute_ml` (
  `contentclass_attribute_id` INT NOT NULL,
  `version` INT NOT NULL,
  `language_id` BIGINT NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  `data_text` TEXT NULL,
  `data_json` TEXT NULL,
  PRIMARY KEY (`contentclass_attribute_id`, `version`, `language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `ezcontentclass_attribute_ml`
ADD CONSTRAINT `ezcontentclass_attribute_ml_lang_fk`
  FOREIGN KEY (`language_id`)
  REFERENCES `ezcontent_language` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `eznotification` MODIFY COLUMN `data` TEXT;

ALTER TABLE `ezcontentobject` ADD COLUMN `is_hidden` tinyint(1) NOT NULL DEFAULT '0';

ALTER TABLE `ezcontentobject_tree` DROP KEY `ezcontentobject_tree_contentobject_id_path_string`;
ALTER TABLE `ezcontentobject_tree` ADD UNIQUE KEY `ezcontentobject_tree_contentobject_id_path_string` (`path_string` (191), `contentobject_id`);

SET default_storage_engine=InnoDB;
BEGIN;
-- Set storage engine schema version number
UPDATE ezsite_data SET value='6.13.4' WHERE name='ezpublish-version';
COMMIT;

SET default_storage_engine=InnoDB;

ALTER TABLE IF EXISTS `ezdfsfile` DROP KEY `ezdfsfile_name`;
ALTER TABLE IF EXISTS`ezdfsfile` ADD KEY `ezdfsfile_name` (`name` (191));

ALTER TABLE IF EXISTS `ezdfsfile` DROP KEY `ezdfsfile_name_trunk`;
ALTER TABLE IF EXISTS `ezdfsfile` ADD KEY `ezdfsfile_name_trunk` (`name_trunk` (191));

ALTER TABLE IF EXISTS `ezdfsfile` DROP KEY `ezdfsfile_expired_name`;
ALTER TABLE IF EXISTS `ezdfsfile` ADD KEY `ezdfsfile_expired_name` (`expired`, `name` (191));

--
-- NB!: If you use DFS and Legacy Bridge, you should also run this script for the `ezdfsfile_cache` table.
--

SET default_storage_engine=InnoDB;
BEGIN;
-- Set storage engine schema version number
UPDATE ezsite_data SET value='7.2.0' WHERE name='ezpublish-version';

--
-- EZP-28950: MySQL UTF8 doesn't support 4-byte chars
-- This shortens indexes so that 4-byte content can fit.
-- After running these, convert the table character set, see doc/upgrade/7.2.md
--

ALTER TABLE `ezbasket` DROP KEY `ezbasket_session_id`;
ALTER TABLE `ezbasket` ADD KEY `ezbasket_session_id` (`session_id` (191));

ALTER TABLE `ezcollab_group` DROP KEY `ezcollab_group_path`;
ALTER TABLE `ezcollab_group` ADD KEY `ezcollab_group_path` (`path_string` (191));

ALTER TABLE `ezcontent_language` DROP KEY `ezcontent_language_name`;
ALTER TABLE `ezcontent_language` ADD KEY `ezcontent_language_name` (`name` (191));

ALTER TABLE `ezcontentobject_attribute` DROP KEY `sort_key_string`;
ALTER TABLE `ezcontentobject_attribute` ADD KEY `sort_key_string` (`sort_key_string` (191));

ALTER TABLE `ezcontentobject_name` DROP KEY `ezcontentobject_name_name`;
ALTER TABLE `ezcontentobject_name` ADD KEY `ezcontentobject_name_name` (`name` (191));

ALTER TABLE `ezcontentobject_trash` DROP KEY `ezcobj_trash_path`;
ALTER TABLE `ezcontentobject_trash` ADD KEY `ezcobj_trash_path` (`path_string` (191));

ALTER TABLE `ezcontentobject_tree` DROP KEY `ezcontentobject_tree_path`;
ALTER TABLE `ezcontentobject_tree` ADD KEY `ezcontentobject_tree_path` (`path_string` (191));

ALTER TABLE `ezimagefile` DROP KEY `ezimagefile_file`;
ALTER TABLE `ezimagefile` ADD KEY `ezimagefile_file` (`filepath` (191));

ALTER TABLE `ezkeyword` DROP KEY `ezkeyword_keyword`;
ALTER TABLE `ezkeyword` ADD KEY `ezkeyword_keyword` (`keyword` (191));

ALTER TABLE `ezorder_status` DROP KEY `ezorder_status_name`;
ALTER TABLE `ezorder_status` ADD KEY `ezorder_status_name` (`name` (191));

ALTER TABLE `ezpolicy_limitation_value` DROP KEY `ezpolicy_limitation_value_val`;
ALTER TABLE `ezpolicy_limitation_value` ADD KEY `ezpolicy_limitation_value_val` (`value` (191));

ALTER TABLE `ezprest_authcode` DROP PRIMARY KEY;
ALTER TABLE `ezprest_authcode` ADD PRIMARY KEY (`id` (191));

ALTER TABLE `ezprest_authcode` DROP KEY `authcode_client_id`;
ALTER TABLE `ezprest_authcode` ADD KEY `authcode_client_id` (`client_id` (191));

ALTER TABLE `ezprest_clients` DROP KEY `client_id_unique`;
ALTER TABLE `ezprest_clients` ADD UNIQUE KEY `client_id_unique` (`client_id` (191),`version`);

ALTER TABLE `ezprest_token` DROP PRIMARY KEY;
ALTER TABLE `ezprest_token` ADD PRIMARY KEY (`id` (191));

ALTER TABLE `ezprest_token` DROP KEY `token_client_id`;
ALTER TABLE `ezprest_token` ADD KEY `token_client_id` (`client_id` (191));

ALTER TABLE `ezsearch_object_word_link` DROP KEY `ezsearch_object_word_link_identifier`;
ALTER TABLE `ezsearch_object_word_link` ADD KEY `ezsearch_object_word_link_identifier` (`identifier` (191));

ALTER TABLE IF EXISTS `ezsearch_search_phrase` DROP KEY `ezsearch_search_phrase_phrase`;
ALTER TABLE `ezsearch_search_phrase` ADD UNIQUE KEY `ezsearch_search_phrase_phrase` (`phrase`);

ALTER TABLE `ezurl` DROP KEY `ezurl_url`;
ALTER TABLE `ezurl` ADD KEY `ezurl_url` (`url` (191));

ALTER TABLE `ezurlalias` DROP KEY `ezurlalias_desturl`;
ALTER TABLE `ezurlalias` ADD KEY `ezurlalias_desturl` (`destination_url` (191));

ALTER TABLE `ezurlalias` DROP KEY `ezurlalias_source_url`;
ALTER TABLE `ezurlalias` ADD KEY `ezurlalias_source_url` (`source_url` (191));


--
-- EZEE-2081: Move NotificationBundle into AdminUI
--

CREATE TABLE IF NOT EXISTS `eznotification` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL DEFAULT 0,
  `is_pending` tinyint(1) NOT NULL DEFAULT '1',
  `type` varchar(128) NOT NULL DEFAULT '',
  `created` int(11) NOT NULL DEFAULT 0,
  `data` blob,
  PRIMARY KEY (`id`),
  KEY `eznotification_owner` (`owner_id`),
  KEY `eznotification_owner_is_pending` (`owner_id`, `is_pending`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

COMMIT;

SET default_storage_engine=InnoDB;
-- Set storage engine schema version number
UPDATE ezsite_data SET value='7.3.0' WHERE name='ezpublish-version';

--
-- EZP-28881: Add a field to support "date object was trashed"
--

ALTER TABLE ezcontentobject_trash ADD IF NOT EXISTS trashed int(11) NOT NULL DEFAULT '0';

SET default_storage_engine=InnoDB;
-- Set storage engine schema version number
UPDATE ezsite_data SET value='7.5.0' WHERE name='ezpublish-version';

--
-- EZP-29990 - Add table for multilingual FieldDefinitions
--

DROP TABLE IF EXISTS `ezcontentclass_attribute_ml`;
CREATE TABLE `ezcontentclass_attribute_ml` (
  `contentclass_attribute_id` INT NOT NULL,
  `version` INT NOT NULL,
  `language_id` BIGINT NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  `data_text` TEXT NULL,
  `data_json` TEXT NULL,
  PRIMARY KEY (`contentclass_attribute_id`, `version`, `language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `ezcontentclass_attribute_ml`
ADD CONSTRAINT `ezcontentclass_attribute_ml_lang_fk`
  FOREIGN KEY (`language_id`)
  REFERENCES `ezcontent_language` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

--
-- EZP-30149: As a Developer I want uniform eznotification DB table definition across all DBMS-es
--

ALTER TABLE `eznotification` MODIFY COLUMN `data` TEXT;

ALTER TABLE `ezcontentobject_tree` DROP KEY `ezcontentobject_tree_contentobject_id_path_string`;
ALTER TABLE `ezcontentobject_tree` ADD UNIQUE KEY `ezcontentobject_tree_contentobject_id_path_string` (`path_string` (191), `contentobject_id`);

SET default_storage_engine=InnoDB;

UPDATE ezsite_data SET value='7.4.5' WHERE name='ezpublish-version';

ALTER TABLE `ezcontentobject_tree` DROP KEY `ezcontentobject_tree_contentobject_id_path_string`;
ALTER TABLE `ezcontentobject_tree` ADD UNIQUE KEY `ezcontentobject_tree_contentobject_id_path_string` (`path_string` (191), `contentobject_id`);

UPDATE ezsite_data SET value='7.5.3' WHERE name='ezpublish-version';

ALTER TABLE ezcontentclass_attribute MODIFY data_text1 VARCHAR(255);

DELETE FROM ezcontentclass
  WHERE NOT EXISTS (
    SELECT 1 FROM ezuser
      WHERE ezuser.contentobject_id = ezcontentclass.modifier_id
  ) AND ezcontentclass.version = 1;

DELETE FROM ezcontentclass_attribute
  WHERE NOT EXISTS (
    SELECT 1 FROM ezcontentclass
      WHERE ezcontentclass.id = ezcontentclass_attribute.contentclass_id
      AND ezcontentclass.version = ezcontentclass_attribute.version
  );

DELETE FROM ezcontentclass_attribute_ml
  WHERE NOT EXISTS (
    SELECT 1 FROM ezcontentclass_attribute
      WHERE ezcontentclass_attribute.id = ezcontentclass_attribute_ml.contentclass_attribute_id
      AND ezcontentclass_attribute.version = ezcontentclass_attribute_ml.version
  );

DELETE FROM ezcontentclass_classgroup
  WHERE NOT EXISTS (
    SELECT 1 FROM ezcontentclass
      WHERE ezcontentclass.id = ezcontentclass_classgroup.contentclass_id
      AND ezcontentclass.version = ezcontentclass_classgroup.contentclass_version
  );

DELETE FROM ezcontentclass_name
  WHERE NOT EXISTS (
    SELECT 1 FROM ezcontentclass
      WHERE ezcontentclass.id = ezcontentclass_name.contentclass_id
      AND ezcontentclass.version = ezcontentclass_name.contentclass_version
  );

UPDATE ezsite_data SET value='7.5.5' WHERE name='ezpublish-version';

ALTER TABLE ezuser ADD COLUMN password_updated_at INT(11) NULL;

UPDATE ezuser SET password_updated_at = UNIX_TIMESTAMP();

UPDATE ezsite_data SET value='7.5.7' WHERE name='ezpublish-version';

ALTER TABLE `ezsearch_object_word_link`
ADD COLUMN `language_mask` BIGINT NOT NULL DEFAULT 0;

UPDATE `ezsearch_object_word_link` SET `language_mask` = 9223372036854775807;

SET default_storage_engine=InnoDB;

UPDATE ezsite_data SET value='6.13.0' WHERE name='ezpublish-version';

DELETE FROM `ezuser` WHERE login = '';

UPDATE ezuser SET login = 'amauryprieto2' WHERE contentobject_id = 5502;
UPDATE ezuser SET login = 'sample2@email.tst' WHERE contentobject_id = 72724;
UPDATE ezuser SET login = 'sample3@email.tst' WHERE contentobject_id = 72725;
UPDATE ezuser SET login = 'sample4@email.tst' WHERE contentobject_id = 72726;
UPDATE ezuser SET login = 'sample5@email.tst' WHERE contentobject_id = 72727;
UPDATE ezuser SET login = 'vgarcia2@phillips.com' WHERE contentobject_id = 69361;
ALTER TABLE `ezuser` DROP KEY ezuser_login, ADD UNIQUE KEY ezuser_login (login);
UPDATE `ezcontentclass_attribute` SET is_searchable = 0 WHERE data_type_string = 'ezuser';

INSERT INTO `ezpolicy` (`module_name`, `function_name`, `original_id`, `role_id`)
  SELECT DISTINCT 'content', 'publish', `original_id`, `role_id`
  FROM `ezpolicy`
  WHERE `module_name` = 'content' AND `function_name` IN ('create', 'edit')
    AND NOT EXISTS (SELECT 1 FROM `ezpolicy` WHERE `module_name` = 'content' AND `function_name` = 'publish');
INSERT INTO `ezpolicy_limitation` (`identifier`, `policy_id`)
  SELECT DISTINCT `l0`.`identifier`, `p1`.`id`
  FROM `ezpolicy_limitation` AS `l0`
  JOIN `ezpolicy` AS `p0` ON `l0`.`policy_id` = `p0`.`id`
  JOIN `ezpolicy` AS `p1` ON `p0`.`role_id` = `p1`.`role_id` AND `p1`.`module_name` = 'content' AND `p1`.`function_name` = 'publish'
  WHERE `p0`.`module_name` = 'content' AND `p0`.`function_name` IN ('create', 'edit');
INSERT INTO `ezpolicy_limitation_value` (`limitation_id`, `value`)
  SELECT `l1`.`id`, `value`
  FROM `ezpolicy_limitation` AS `l0`
  JOIN `ezpolicy_limitation_value` AS `lv0` ON `lv0`.`limitation_id` = `l0`.`id`
  JOIN `ezpolicy` AS `p0` ON `p0`.`id` = `l0`.`policy_id`
  JOIN `ezpolicy_limitation` AS `l1` ON `l0`.`identifier` = `l1`.`identifier`
  JOIN `ezpolicy` AS `p1`
    ON `l1`.`policy_id` = `p1`.`id`
    AND `p1`.`module_name` = 'content'
    AND `p1`.`function_name` = 'publish'
    AND `p1`.`role_id` = `p0`.`role_id`;
