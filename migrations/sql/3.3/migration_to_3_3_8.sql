START TRANSACTION;
DELETE FROM ezsite_data WHERE name IN ('ezpublish-version', 'ezplatform-release');
INSERT INTO ezsite_data (name, value) VALUES ('ezplatform-release', '3.0.0');
COMMIT;

ALTER TABLE ezcontentclass_attribute MODIFY data_text1 VARCHAR(255);

UPDATE `ezsearch_object_word_link` SET `language_mask` = 9223372036854775807;

ALTER TABLE ezcontentclass_attribute ADD COLUMN is_thumbnail TINYINT(1) NOT NULL DEFAULT '0';

ALTER TABLE `ezkeyword_attribute_link`
    ADD COLUMN `version` INT(11),
    ADD KEY `ezkeyword_attr_link_oaid_ver` (`objectattribute_id`, `version`);

UPDATE `ezkeyword_attribute_link`
SET `version` = COALESCE(
        (
            SELECT `current_version`
            FROM `ezcontentobject_attribute` AS `cattr`
                     JOIN `ezcontentobject` AS `contentobj`
                          ON `cattr`.`contentobject_id` = `contentobj`.`id`
                              AND `cattr`.`version` = `contentobj`.`current_version`
            WHERE `cattr`.`id` = `ezkeyword_attribute_link`.`objectattribute_id`
        ), 0
    );

ALTER TABLE `ezkeyword_attribute_link`
    MODIFY `version` INT(11) NOT NULL DEFAULT '0';

UPDATE `ezcontentclass_attribute` SET `data_text2` = '^[^@]+$'
    WHERE `data_type_string` = 'ezuser'
    AND `data_text2` IS NULL;

DROP TABLE IF EXISTS `ezsite_public_access`;
DROP TABLE IF EXISTS `ezsite`;

CREATE TABLE `ezsite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `created` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `ezsite_public_access` (
  `public_access_identifier` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `site_id` int(11) NOT NULL,
  `site_access_group` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `status` int(11) NOT NULL,
  `config` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `site_matcher_host` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`public_access_identifier`),
  KEY `ezsite_public_access_site_id` (`site_id`),
  CONSTRAINT `fk_ezsite_public_access_site_id` FOREIGN KEY (`site_id`) REFERENCES `ezsite` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `ezsite_public_access` ADD COLUMN site_matcher_path VARCHAR(255) DEFAULT NULL;

DROP TABLE IF EXISTS `ibexa_segment_group_map`;
CREATE TABLE `ibexa_segment_group_map` (
  `segment_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`segment_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

DROP TABLE IF EXISTS `ibexa_segment_groups`;
CREATE TABLE `ibexa_segment_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`,`identifier`),
  UNIQUE KEY `ibexa_segment_groups_identifier` (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

DROP TABLE IF EXISTS `ibexa_segment_user_map`;
CREATE TABLE `ibexa_segment_user_map` (
  `segment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`segment_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

DROP TABLE IF EXISTS `ibexa_segments`;
CREATE TABLE `ibexa_segments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`,`identifier`),
  UNIQUE KEY `ibexa_segments_identifier` (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
DROP TABLE IF EXISTS `ibexa_setting`;
CREATE TABLE `ibexa_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `identifier` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ibexa_setting_group_identifier` (`group`, `identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `ibexa_setting` (`group`, `identifier`, `value`)
VALUES ('personalization', 'installation_key', '""');

UPDATE ezuser SET password_updated_at = UNIX_TIMESTAMP();

DROP TABLE IF EXISTS `ezcontentclass_attribute_ml`;
CREATE TABLE `ezcontentclass_attribute_ml` (
  `contentclass_attribute_id` INT NOT NULL,
  `version` INT NOT NULL,
  `language_id` BIGINT NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  `data_text` TEXT NULL,
  `data_json` TEXT NULL,
  PRIMARY KEY (`contentclass_attribute_id`, `version`, `language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE ezuser CHANGE password_hash password_hash VARCHAR(255) default NULL;

CREATE TABLE IF NOT EXISTS `eznotification` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL DEFAULT 0,
  `is_pending` tinyint(1) NOT NULL DEFAULT '1',
  `type` varchar(128) NOT NULL DEFAULT '',
  `created` int(11) NOT NULL DEFAULT 0,
  `data` blob,
  PRIMARY KEY (`id`),
  KEY `eznotification_owner` (`owner_id`),
  KEY `eznotification_owner_is_pending` (`owner_id`, `is_pending`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

COMMIT;

SET FOREIGN_KEY_CHECKS=0; -- to disable them
DROP TABLE IF EXISTS `ezeditorialworkflow_workflows`;
DROP TABLE IF EXISTS `ezeditorialworkflow_markings`;
CREATE TABLE `ezeditorialworkflow_workflows` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `version_no` int(11) NOT NULL,
  `workflow_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `initial_owner_id` int(11) DEFAULT NULL,
  `start_date` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_workflow_id` (`id`),
  KEY `initial_owner_id` (`initial_owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

DROP TABLE IF EXISTS `ezeditorialworkflow_transitions`;
CREATE TABLE `ezeditorialworkflow_transitions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `timestamp` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_workflow_id_transitions` (`workflow_id`),
  CONSTRAINT `fk_ezeditorialworkflow_transitions_workflow_id` FOREIGN KEY (`workflow_id`) REFERENCES `ezeditorialworkflow_workflows` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

CREATE TABLE `ezeditorialworkflow_markings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `fk_workflow_id_markings` (`workflow_id`),
  CONSTRAINT `fk_ezeditorialworkflow_markings_workflow_id` FOREIGN KEY (`workflow_id`) REFERENCES `ezeditorialworkflow_workflows` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
SET FOREIGN_KEY_CHECKS=1; -- to re-enable them

-- EZEE-2880: Added support for stage and transition actions --
ALTER TABLE `ezeditorialworkflow_markings`
    ADD COLUMN `message` TEXT NOT NULL,
    ADD COLUMN `reviewer_id` INT(11),
    ADD COLUMN `result` TEXT;
--

CREATE TABLE IF NOT EXISTS `ezdatebasedpublisher_scheduled_version` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `version_id` int(11) NOT NULL,
  `version_number` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `publication_date` int(11) NOT NULL,
  `url_root` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index4` (`content_id`,`version_number`),
  KEY `index2` (`content_id`),
  KEY `index3` (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ezdatebasedpublisher_scheduled_version`;
CREATE TABLE IF NOT EXISTS `ezdatebasedpublisher_scheduled_version` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `version_id` int(11) NOT NULL,
  `version_number` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `publication_date` int(11) NOT NULL,
  `url_root` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index4` (`content_id`,`version_number`),
  KEY `index2` (`content_id`),
  KEY `index3` (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

START TRANSACTION;
ALTER TABLE  `ezdatebasedpublisher_scheduled_version`
CHANGE COLUMN `publication_date` `action_timestamp` INT(11) NOT NULL;
ALTER TABLE  `ezdatebasedpublisher_scheduled_version`
ADD COLUMN `action` VARCHAR(32);
UPDATE `ezdatebasedpublisher_scheduled_version` SET `action` = 'publish';
ALTER TABLE  `ezdatebasedpublisher_scheduled_version` CHANGE COLUMN `action` `action` VARCHAR(32) NOT NULL;
COMMIT;

DROP TABLE IF EXISTS `ezdatebasedpublisher_scheduled_entries`;
ALTER TABLE `ezdatebasedpublisher_scheduled_version`
CHANGE COLUMN `version_id` `version_id` INT NULL ,
CHANGE COLUMN `version_number` `version_number` INT NULL ,
RENAME TO  `ezdatebasedpublisher_scheduled_entries`;

-- Product name migration
START TRANSACTION;
DELETE FROM ezsite_data WHERE name IN ('ezpublish-version', 'ezplatform-release');
INSERT INTO ezsite_data (name, value) VALUES ('ezplatform-release', '3.0.0');
COMMIT;

ALTER TABLE ezcontentclass_attribute MODIFY data_text1 VARCHAR(255);

DELETE FROM `ezcontentclass_classgroup`
WHERE ((`contentclass_id` = '62' AND `contentclass_version` = '0' AND `group_id` = '7'));

-- INSERT INTO `ibexa_setting` (`group`, `identifier`, `value`)
-- VALUES ('personalization', 'installation_key', '""');

CREATE TABLE IF NOT EXISTS `ibexa_workflow_version_lock` (
    `id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `content_id` INT(11) NOT NULL,
    `version` INT(11) NOT NULL,
    `user_id` INT(11) NOT NULL,
    `created` INT(11) NOT NULL DEFAULT 0,
    `modified` INT(11) NOT NULL DEFAULT 0,
    `is_locked` BOOLEAN NOT NULL DEFAULT true,
    KEY `ibexa_workflow_version_lock_content_id_index` (`content_id`) USING BTREE,
    KEY `ibexa_workflow_version_lock_user_id_index` (`user_id`) USING BTREE,
    UNIQUE KEY `ibexa_workflow_version_lock_content_id_version_uindex` (`content_id`,`version`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE = utf8mb4_unicode_520_ci;

--
-- EZP-28881: Add a field to support "date object was trashed"
--
CREATE TABLE IF NOT EXISTS `ezdfsfile` (
  `name_hash` varchar(34) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name_trunk` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `datatype` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'application/octet-stream',
  `scope` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `size` bigint(20) unsigned NOT NULL DEFAULT 0,
  `mtime` int(11) NOT NULL DEFAULT 0,
  `expired` tinyint(1) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`name_hash`),
  KEY `ezdfsfile_name_trunk` (`name_trunk`(191)),
  KEY `ezdfsfile_expired_name` (`expired`,`name`(191)),
  KEY `ezdfsfile_name` (`name`(191)),
  KEY `ezdfsfile_mtime` (`mtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- Migración customTags

UPDATE ezcontentobject_attribute SET data_text = replace(data_text, '<ezcontent/>','<ezcontent><para></para></ezcontent>');

UPDATE ezcontentobject_attribute SET data_text = replace(data_text,'<eztemplate name="infopage-banner"','<eztemplate name="infopage_banner"') where ezcontentobject_attribute.data_type_string = 'ezrichtext';
UPDATE ezcontentobject_attribute SET data_text = replace(data_text,'<eztemplate name="environmental-impact"','<eztemplate name="environmental_impact"') where ezcontentobject_attribute.data_type_string = 'ezrichtext';
UPDATE ezcontentobject_attribute SET data_text = replace(data_text,'<eztemplate name="environmental-impact-stretches"','<eztemplate name="environmental_impact_stretches"') where ezcontentobject_attribute.data_type_string = 'ezrichtext';
UPDATE ezcontentobject_attribute SET data_text = replace(data_text,'<eztemplate name="environmental-impact-item"','<eztemplate name="environmental_impact_item"') where ezcontentobject_attribute.data_type_string = 'ezrichtext';
UPDATE ezcontentobject_attribute SET data_text = replace(data_text,'<eztemplate name="transmision-accordion"','<eztemplate name="transmision_accordion"') where ezcontentobject_attribute.data_type_string = 'ezrichtext';
UPDATE ezcontentobject_attribute SET data_text = replace(data_text,'<eztemplate name="transmision-accordion-item"','<eztemplate name="transmision_accordion_item"') where ezcontentobject_attribute.data_type_string = 'ezrichtext';
UPDATE ezcontentobject_attribute SET data_text = replace(data_text,'<eztemplateinline name="transmision-accordion-item"','<eztemplateinline name="transmision_accordion_item"') where ezcontentobject_attribute.data_type_string = 'ezrichtext';
UPDATE ezcontentobject_attribute SET data_text = replace(data_text,'<ezvalue key="identifier">transmision-accordion','<ezvalue key="identifier">transmision_accordion') where ezcontentobject_attribute.data_type_string = 'ezrichtext';

-- Ez Tags
DROP TABLE IF EXISTS `eztags`;
CREATE TABLE `eztags` (
  `id` int(11) NOT NULL auto_increment,
  `parent_id` int(11) NOT NULL default '0',
  `main_tag_id` int(11) NOT NULL default '0',
  `keyword` varchar(255) NOT NULL default '',
  `depth` int(11) NOT NULL default '1',
  `path_string` varchar(255) NOT NULL default '',
  `modified` int(11) NOT NULL default '0',
  `remote_id` varchar(100) NOT NULL default '',
  `main_language_id` int(11) NOT NULL default '0',
  `language_mask` int(11) NOT NULL default '0',
  PRIMARY KEY ( `id` ),
  KEY `idx_eztags_keyword` ( `keyword`(191) ),
  KEY `idx_eztags_keyword_id` ( `keyword`(191), `id` ),
  UNIQUE KEY `idx_eztags_remote_id` ( `remote_id` )
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `eztags_attribute_link`;
CREATE TABLE `eztags_attribute_link` (
  `id` int(11) NOT NULL auto_increment,
  `keyword_id` int(11) NOT NULL default '0',
  `objectattribute_id` int(11) NOT NULL default '0',
  `objectattribute_version` int(11) NOT NULL default '0',
  `object_id` int(11) NOT NULL default '0',
  `priority` int(11) NOT NULL default '0',
  PRIMARY KEY ( `id` ),
  KEY `idx_eztags_attr_link_keyword_id` ( `keyword_id` ),
  KEY `idx_eztags_attr_link_kid_oaid_oav` ( `keyword_id`, `objectattribute_id`, `objectattribute_version` ),
  KEY `idx_eztags_attr_link_kid_oid` ( `keyword_id`, `object_id` ),
  KEY `idx_eztags_attr_link_oaid_oav` ( `objectattribute_id`, `objectattribute_version` )
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `eztags_keyword`;
CREATE TABLE `eztags_keyword` (
  `keyword_id` int(11) NOT NULL default '0',
  `language_id` int(11) NOT NULL default '0',
  `keyword` varchar(255) NOT NULL default '',
  `locale` varchar(20) NOT NULL default '',
  `status` int(11) NOT NULL default '0',
  PRIMARY KEY ( `keyword_id`, `locale` )
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `ezform_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) DEFAULT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `identifier` varchar(128) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


CREATE TABLE IF NOT EXISTS `ezform_field_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) DEFAULT NULL,
  `identifier` varchar(128) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `value` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


CREATE TABLE IF NOT EXISTS `ezform_field_validators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) DEFAULT NULL,
  `identifier` varchar(128) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `value` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


CREATE TABLE IF NOT EXISTS `ezform_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_id` int(11) DEFAULT NULL,
  `version_no` int(11) DEFAULT NULL,
  `content_field_id` int(11) DEFAULT NULL,
  `language_code` varchar(16) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


CREATE TABLE IF NOT EXISTS `ezform_form_submissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `language_code` varchar(6) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


CREATE TABLE IF NOT EXISTS `ezform_form_submission_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_submission_id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `identifier` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

CREATE INDEX ezpage_map_zones_pages_zone_id ON ezpage_map_zones_pages(zone_id);
CREATE INDEX ezpage_map_zones_pages_page_id ON ezpage_map_zones_pages(page_id);
CREATE INDEX ezpage_map_blocks_zones_block_id ON ezpage_map_blocks_zones(block_id);
CREATE INDEX ezpage_map_blocks_zones_zone_id ON ezpage_map_blocks_zones(zone_id);
CREATE INDEX ezpage_map_attributes_blocks_attribute_id ON ezpage_map_attributes_blocks(attribute_id);
CREATE INDEX ezpage_map_attributes_blocks_block_id ON ezpage_map_attributes_blocks(block_id);
CREATE INDEX ezpage_blocks_design_block_id ON ezpage_blocks_design(block_id);
CREATE INDEX ezpage_blocks_visibility_block_id ON ezpage_blocks_visibility(block_id);
CREATE INDEX ezpage_pages_content_id_version_no ON ezpage_pages(content_id, version_no);

CREATE INDEX idx_workflow_co_id_ver ON ezeditorialworkflow_workflows(content_id, version_no);
CREATE INDEX idx_workflow_name ON ezeditorialworkflow_workflows(workflow_name);