#!/bin/ash

php bin/console cache:clear && php bin/console cache:pool:clear cache.redis && chmod 777 -R /app/var/cache

php -d memory_limit=-1 bin/console ibexa:reindex --no-purge --no-interaction --iteration-count=${1:-50} -vvv

php bin/console cache:clear && php bin/console cache:pool:clear cache.redis && chmod 777 -R /app/var/cache

